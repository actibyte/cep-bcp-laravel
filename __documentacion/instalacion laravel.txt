1. Instalador composer para windows, no utilizar proxy e indicar la ruta de la carpeta php durante la instalación:
	https://getcomposer.org/Composer-Setup.exe

2. Configurar paquetes para sublime text 2
	Instalacion de Package Control
	-------------------------------
	- instalar package control, en Sublime text abrir consola (View>Console) y pegar la siguiente linea:
		import urllib2,os,hashlib; h = 'df21e130d211cfc94d9b0905775a7c0f' + '1e3d39e33b79698005270310898eea76'; pf = 'Package Control.sublime-package'; ipp = sublime.installed_packages_path(); os.makedirs( ipp ) if not os.path.exists(ipp) else None; urllib2.install_opener( urllib2.build_opener( urllib2.ProxyHandler()) ); by = urllib2.urlopen( 'http://packagecontrol.io/' + pf.replace(' ', '%20')).read(); dh = hashlib.sha256(by).hexdigest(); open( os.path.join( ipp, pf), 'wb' ).write(by) if dh == h else None; print('Error validating download (got %s instead of %s), please try manual install' % (dh, h) if dh != h else 'Please restart Sublime Text to finish installation')
	
	[Opcional] Instalacion de Emmet (Autocompletado para html5 con tecla tabulacion)
	------------------------------------------------------------------
	- En sublime text abrir "command pallete" (Tools>Command pallete)
	- ingresar el siguiente comando "Package Control List Packages" y presionar enter
	- ingresar "Emmet", seleccionarlo de la lista y presionar enter para instalar (esperar a que termine la instalacion y reiniciar)

	[Opcional] Instalacion de laravel blade highlighter (Resaltado de etiquetas blade de laravel)
	----------------------------------------------------------------------------------
	- En sublime text abrir "command pallete" (Tools>Command pallete)
	- ingresar el siguiente comando "Package Control List Packages" y presionar enter
	- ingresar "Laravel Blade Highlighter", seleccionarlo de la lista y presionar enter para instalar (esperar a que termine la instalacion y reiniciar)
	
3. Clonar repositorio
	nombre del proyecto: 	laravel_bcp.git
	repositorio url:	miusuario@192.168.0.200:~/repositorio/laravel_bcp.git

4. Configurar repositorio
	- Crear y configurar bd del proyecto (usuario, claves y permisos de ser necesario)
	- Crear archivo de configuracion en la raiz del proyecto, copiar ".env.example" a ".env"
	- Editar los datos de configuracion de DB en los campos indicados y guardar.
		DB_DATABASE=mibd
		DB_USERNAME=miusuario
		DB_PASSWORD=miclave

5. Instalacion de DB y librerias laravel
	- Abrir consola de sistema (CMD) e ingresar al directorio del proyecto laravel.
	- Ejecutar "composer dump-autoload"
	- Ejecutar "composer install"
	- Ejecutar "php artisan key:generate"
	- Ejecutar "php artisan optimize"
	- Ejecutar "php artisan migrate:refresh --seed"

5. Ingresar al sitio web renombrar el subdirectorio del proyecto [miproyecto] por [laravel_bcp] o el nombre de la carpeta del proyecto:
	URL index: localhost/[miproyecto]/public
	usuario/clave default:	admin/admin