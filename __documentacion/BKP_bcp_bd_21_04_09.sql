-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-04-2019 a las 15:53:08
-- Versión del servidor: 5.7.9
-- Versión de PHP: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bcp_bd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adjuntos`
--

DROP TABLE IF EXISTS `adjuntos`;
CREATE TABLE IF NOT EXISTS `adjuntos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_respuesta` int(10) UNSIGNED NOT NULL,
  `tipo` int(11) NOT NULL,
  `ruta_archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `adjuntos`
--

INSERT INTO `adjuntos` (`id`, `id_respuesta`, `tipo`, `ruta_archivo`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'Et accusantium aut vel nulla recusandae ad aut.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(2, 1, 3, 'Molestias et quis quis optio sunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(3, 5, 3, 'At atque blanditiis vero dolor.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(4, 8, 2, 'Est quisquam ut officiis et vero.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(5, 18, 2, 'Accusamus consequatur et ea autem.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(6, 11, 1, 'Autem ut alias sunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(7, 11, 1, 'Neque culpa nobis possimus error amet labore.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(8, 14, 1, 'Voluptas dolorum assumenda qui deserunt animi.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(9, 4, 1, 'A omnis eos sint voluptates.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(10, 10, 3, 'Eos quae et esse vel.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(11, 20, 1, 'Eveniet ad totam perferendis.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(12, 15, 3, 'Praesentium fugit voluptatibus deleniti at.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(13, 12, 3, 'Sunt omnis aliquam vitae rerum.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(14, 15, 1, 'Veniam quae quia atque rerum et qui.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(15, 16, 2, 'Maiores dolor alias voluptates velit velit.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(16, 1, 2, 'Ab consequatur tempora beatae esse.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(17, 19, 1, 'Eos rem quo non occaecati nostrum.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(18, 20, 1, 'Enim quas vero dolor qui voluptate dolorem.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(19, 12, 2, 'Tempora et nam animi sed.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(20, 19, 1, 'Adipisci quia cupiditate quae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apuntes`
--

DROP TABLE IF EXISTS `apuntes`;
CREATE TABLE IF NOT EXISTS `apuntes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_miembro` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ruta_archivo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `apuntes`
--

INSERT INTO `apuntes` (`id`, `id_miembro`, `titulo`, `comentario`, `ruta_archivo`, `created_at`, `updated_at`) VALUES
(1, 4, 'Quia dignissimos autem vel in quod.', 'Optio et sit placeat.', 'Rerum id hic sint illum qui id.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(2, 18, 'Nobis aut suscipit dolor quaerat voluptatem est.', 'Maxime et eligendi ut consectetur ea rerum.', 'Facere modi eius molestiae architecto ipsa eos.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(3, 14, 'Earum esse architecto ab velit in nemo.', 'Molestiae eius quo harum odio enim quibusdam.', 'Voluptas vero dolorum reiciendis debitis iure.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(4, 4, 'Sit sit et nihil et ea molestiae nihil.', 'Et sunt et consequatur unde minus laborum est.', 'Sint ea corrupti sit quia consequatur distinctio.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(5, 19, 'In eveniet quaerat quasi eius.', 'Enim fugit et cupiditate aliquid ipsa.', 'Ea ut enim non est. Fugit quo assumenda quasi.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(6, 20, 'Dolorem fuga debitis incidunt odit.', 'Sunt placeat voluptatem aut ut tempora quaerat.', 'Qui officiis sit iure et sunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(7, 18, 'Fugit earum aut occaecati sed ut quidem.', 'Veniam doloribus voluptate excepturi.', 'Vel eum impedit modi amet dolores possimus ea.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(8, 7, 'Similique doloribus eveniet ullam recusandae.', 'Natus expedita eos dicta qui omnis.', 'Quia iure enim et. Voluptas quo vero eaque ipsa.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(9, 5, 'Iure minus perferendis corporis quia.', 'Quia quam dolorem atque.', 'Dolores minus quasi excepturi magnam.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(10, 13, 'Totam earum autem magnam a voluptatem.', 'Dicta qui temporibus qui minus dolor cupiditate.', 'Dolore totam quo sed facere culpa.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(11, 13, 'Aliquid aut rerum ut in aut tenetur.', 'Iusto hic rerum earum sapiente saepe.', 'Qui enim id ipsam perferendis.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(12, 9, 'Et est non sunt et ipsum blanditiis.', 'Suscipit eligendi et neque.', 'Dolor qui vel ea et est.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(13, 4, 'Cupiditate voluptas molestiae unde et sit sit.', 'Ut nam eum et.', 'Similique asperiores corrupti facere enim.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(14, 18, 'Qui nesciunt aut nihil maxime ipsam incidunt est.', 'Ut est voluptatem vitae illum sunt.', 'Debitis eos fuga aperiam sint facere nisi.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(15, 1, 'Ab sunt est aperiam saepe.', 'Sunt nihil voluptatum sed harum rerum velit.', 'Eos commodi maxime vel perferendis aperiam aut.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(16, 20, 'Quae quia placeat tempore.', 'Ex totam quo non laboriosam est.', 'Nostrum et sit omnis et commodi officia maxime.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(17, 5, 'Incidunt et ab qui deserunt adipisci odio.', 'Quidem quis et totam quibusdam accusamus.', 'Mollitia vel quo et mollitia.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(18, 1, 'Eos similique voluptatum atque ut vitae nostrum.', 'A accusantium provident quia dolor accusamus et.', 'Excepturi ipsa odit nihil at explicabo neque.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(19, 16, 'Qui architecto ea nemo cupiditate rerum quia.', 'Accusamus assumenda dolor sapiente.', 'Nesciunt aut sint dignissimos.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(20, 9, 'Accusamus ut cumque et adipisci nesciunt.', 'Ipsam provident laboriosam provident omnis.', 'Temporibus rerum vero est.', '2016-10-26 18:50:05', '2016-10-26 18:50:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos`
--

DROP TABLE IF EXISTS `cargos`;
CREATE TABLE IF NOT EXISTS `cargos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `cargos`
--

INSERT INTO `cargos` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Lucile', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 'Selmer', '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

DROP TABLE IF EXISTS `equipos`;
CREATE TABLE IF NOT EXISTS `equipos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(10) UNSIGNED NOT NULL,
  `id_sucursal` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id`, `id_proyecto`, `id_sucursal`, `created_at`, `updated_at`) VALUES
(1, 11, 4, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 5, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 18, 19, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 7, 5, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 12, 10, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 1, 18, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 1, 15, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 8, 4, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 2, 5, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 20, 12, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 5, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 8, 11, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 6, 9, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 7, 3, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 14, 4, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 18, 16, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 13, 8, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 18, 7, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 10, 15, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 4, 18, '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foro_respuestas`
--

DROP TABLE IF EXISTS `foro_respuestas`;
CREATE TABLE IF NOT EXISTS `foro_respuestas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_miembro` int(10) UNSIGNED NOT NULL,
  `id_tema` int(10) UNSIGNED NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `foro_respuestas`
--

INSERT INTO `foro_respuestas` (`id`, `id_miembro`, `id_tema`, `asunto`, `comentario`, `estado`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 'Aut esse omnis quis consectetur consequatur.', 'Minima sapiente ut magni reiciendis.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(2, 1, 19, 'Labore aut nostrum reiciendis culpa et.', 'Voluptas nobis hic cumque autem.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(3, 15, 17, 'Nulla ea inventore in. Culpa et in aut illum est.', 'Qui nihil nihil sunt dolorum eum.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(4, 1, 18, 'Voluptates ipsum delectus sint accusantium sunt.', 'In dolorem quidem reiciendis maxime.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(5, 12, 3, 'Placeat quas minus voluptatem.', 'Dolorem id asperiores architecto itaque.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(6, 5, 18, 'Qui a nobis ad officiis.', 'Mollitia quidem alias aut quibusdam maiores.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(7, 2, 14, 'Quisquam deleniti qui excepturi qui.', 'Ipsum qui et libero ratione facere iste facere.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(8, 5, 16, 'Ut exercitationem est quo dignissimos.', 'Unde ad tenetur provident labore rem.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(9, 7, 15, 'Nostrum laborum qui aliquid et ut similique.', 'Quasi rem explicabo sit dolores.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(10, 16, 11, 'Molestiae autem nobis qui a.', 'Sit est et tempore id.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(11, 19, 15, 'Delectus ut dolor quia nemo.', 'Quidem possimus quia sunt tempora.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(12, 15, 1, 'Occaecati consectetur qui dolore.', 'Officiis est exercitationem ea voluptas eius ut.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(13, 2, 15, 'Temporibus nulla soluta ipsam rem.', 'Iste aut ut officia porro.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(14, 17, 12, 'Aspernatur excepturi nam aut itaque omnis aut.', 'Qui consectetur est ratione dicta.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(15, 18, 1, 'In nihil ipsum quasi similique corrupti alias.', 'Esse est ducimus commodi reiciendis.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(16, 3, 11, 'Assumenda sit et ex itaque sed magni.', 'Nisi nemo dolorem excepturi ratione.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(17, 3, 20, 'Aut voluptas animi accusamus adipisci ullam.', 'Amet est quis sapiente ut odit.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(18, 2, 1, 'Fugiat cumque doloremque quos libero.', 'Temporibus fugit totam natus iste.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(19, 14, 10, 'Numquam aspernatur dolores est perferendis.', 'Ut qui quia quis dolore numquam.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(20, 16, 14, 'Voluptates earum vero ut voluptatem eum incidunt.', 'Iusto ea ea dicta velit fugiat aut nulla.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foro_temas`
--

DROP TABLE IF EXISTS `foro_temas`;
CREATE TABLE IF NOT EXISTS `foro_temas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_equipo` int(10) UNSIGNED NOT NULL,
  `id_miembro` int(10) UNSIGNED NOT NULL,
  `asunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `foro_temas`
--

INSERT INTO `foro_temas` (`id`, `id_equipo`, `id_miembro`, `asunto`, `comentario`, `estado`, `created_at`, `updated_at`) VALUES
(1, 18, 7, 'Cumque voluptas officia aut autem.', 'Vel ut cum laborum atque quo qui.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(2, 20, 12, 'Aut nemo impedit quia.', 'Natus id repellendus maiores voluptate harum qui.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(3, 16, 11, 'Perferendis atque quod magni sed.', 'Neque amet sunt tenetur repudiandae.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(4, 8, 13, 'Autem velit ad aut ipsam.', 'Omnis et voluptatem nulla natus veniam et.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(5, 19, 9, 'Rerum eius aut at facere.', 'Dolor illo provident voluptatem qui commodi quis.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(6, 8, 16, 'Eligendi in et incidunt.', 'Culpa et cupiditate velit qui officia.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(7, 20, 3, 'Eum aut amet eveniet soluta et minus qui hic.', 'Dicta et sit laborum sed ad non sunt.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(8, 6, 18, 'Facilis neque ea omnis molestiae provident et.', 'Sed quia consequatur quos totam.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(9, 13, 8, 'Quo eum molestias ducimus.', 'Et placeat modi qui voluptatem iure illo dolores.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(10, 7, 12, 'Laborum voluptas illo ab.', 'Quibusdam optio et accusamus.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(11, 11, 10, 'Non iure ut hic perspiciatis odio illum.', 'Et earum dolorum eum eos itaque.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(12, 16, 3, 'Libero numquam ea velit unde.', 'Et distinctio blanditiis quas.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(13, 17, 4, 'Consequuntur architecto non earum dolorem in.', 'Dolor placeat ullam fugiat ad.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(14, 3, 4, 'Exercitationem qui qui nemo omnis.', 'Qui et ut saepe incidunt qui.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(15, 14, 9, 'Sit iste temporibus ducimus sunt magnam.', 'Et sit qui quis.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(16, 1, 16, 'At voluptate omnis nisi quis iste pariatur.', 'Sapiente amet hic exercitationem.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(17, 2, 9, 'Eos accusamus quod voluptate ea aliquid.', 'Voluptas vel est eius qui debitis.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(18, 7, 20, 'In ea molestiae fuga fuga aperiam.', 'Est quidem tenetur in quos dolorem labore.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(19, 19, 4, 'Quis ipsam sit ut aperiam quas sunt.', 'Modi atque recusandae sint repellendus vel.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(20, 15, 11, 'Qui accusamus aut ut voluptatem.', 'Ut dolor numquam quos nulla numquam occaecati in.', '', '2016-10-26 18:50:05', '2016-10-26 18:50:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `miembros`
--

DROP TABLE IF EXISTS `miembros`;
CREATE TABLE IF NOT EXISTS `miembros` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_equipo` int(10) UNSIGNED NOT NULL,
  `id_usuario` int(10) UNSIGNED NOT NULL,
  `id_cargo` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `miembros`
--

INSERT INTO `miembros` (`id`, `id_equipo`, `id_usuario`, `id_cargo`, `created_at`, `updated_at`) VALUES
(1, 8, 1, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 11, 16, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 9, 20, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 3, 12, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 15, 8, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 8, 15, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 3, 13, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 18, 20, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 16, 16, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 15, 3, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 11, 14, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 12, 9, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 1, 11, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 3, 9, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 17, 10, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 14, 3, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 2, 11, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 9, 11, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 20, 8, 1, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 20, 11, 2, '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_07_144220_create_usuarios_table', 1),
('2016_10_10_130952_create_sucursales_table', 1),
('2016_10_10_170730_create_proyectos_table', 1),
('2016_10_10_170815_create_preguntas_table', 1),
('2016_10_13_153135_create_equipos_table', 1),
('2016_10_13_153141_create_miembros_table', 1),
('2016_10_13_155332_create_cargos_table', 1),
('2016_10_13_210534_create_respuestas_table', 1),
('2016_10_13_210544_create_adjuntos_table', 1),
('2016_10_25_132918_create_foro_temas_table', 1),
('2016_10_25_132919_create_foro_respuestas_table', 1),
('2016_10_25_133402_create_apuntes_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
CREATE TABLE IF NOT EXISTS `preguntas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(10) UNSIGNED NOT NULL,
  `numero` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `id_proyecto`, `numero`, `titulo`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 6, 1, 'Maxime sunt nulla odio.', 'Aut porro maxime ut recusandae cum ullam consequatur. Rerum sit illum fugit aut. Animi repellendus quasi eligendi voluptatem natus omnis quia minus.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 12, 1, 'Molestiae repellat neque quibusdam praesentium velit quidem velit amet.', 'Assumenda quaerat nisi nihil sint explicabo. Dolorum eaque omnis quasi aut vel cupiditate magni dolorem. In ut voluptatem inventore officiis quo.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 17, 1, 'Voluptatum dolorem est cum deserunt.', 'Et voluptatem voluptate qui quae quia rerum eum nemo. In incidunt vel voluptatum modi ipsa molestiae. Aspernatur autem explicabo inventore quidem sed.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 5, 1, 'Eveniet id laudantium aut ipsam dolor voluptatem officia.', 'Quae ad ut fuga consequuntur nisi ut. Omnis ipsum consequatur rerum sapiente. Quas rerum incidunt et vel quas doloremque omnis. Suscipit quod id itaque consequatur et impedit impedit.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 16, 1, 'Repellat a quia ut neque vel deleniti velit.', 'Aperiam et et explicabo fugit voluptatum omnis omnis. Sit quasi quia aut. Pariatur vitae ex eaque et ad.\nSapiente ea fugiat quo. Dignissimos magni ut at molestiae enim qui.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 11, 1, 'Deserunt mollitia aut accusantium quod.', 'Eveniet sint omnis laborum recusandae quia eius. Quia iste ipsa ex quia. Iste illum atque dignissimos id.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 10, 1, 'Architecto quaerat non voluptatem aperiam.', 'Asperiores voluptatem laboriosam qui facilis. Et veniam sunt et ipsa. Deserunt itaque rerum magnam et. Qui autem id numquam maiores unde.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 11, 1, 'Omnis eius repellendus architecto recusandae nihil rem.', 'Tempora nesciunt atque vel assumenda. Aut blanditiis quisquam qui rerum labore nostrum. Voluptatem amet qui deserunt est vero dolorem ex. Debitis eum veritatis magni aut in magni.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 9, 1, 'Doloremque ipsum in quaerat consequuntur quo aperiam placeat.', 'Aut id hic omnis necessitatibus. Rerum deserunt ut iusto. Ipsa aut est accusamus consectetur eligendi occaecati at.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 14, 1, 'Nisi sed officia possimus quas quam sit.', 'Fuga unde odit natus ut. Voluptatum enim illo aut expedita.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 12, 1, 'Consequatur eveniet inventore id sequi autem.', 'Aut sapiente voluptas harum impedit minima eligendi culpa. Ea vel adipisci ducimus voluptas aut. Sed ducimus dolore dolore sint.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 17, 1, 'Id rem placeat illo voluptas.', 'Beatae odit tenetur omnis est quo voluptas error. Veniam repellat dolor qui sit. Sapiente ratione architecto magni ad.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 6, 1, 'Ut maiores autem tenetur rerum dolor.', 'Impedit iure rerum culpa aut. Natus fugit qui totam. Voluptatibus quo minus nam velit corporis placeat ipsa delectus.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 14, 1, 'Non nemo illo eos voluptates cum.', 'Inventore totam maxime vitae hic officia est provident. Reprehenderit illo et iste et quis adipisci. Quidem sequi omnis asperiores velit veritatis minus magnam.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 17, 1, 'Ad ut velit et consequatur eum dolor natus.', 'Tempora repellat deleniti fugiat eos rerum nostrum. Eum incidunt laboriosam adipisci quibusdam deleniti architecto laborum. Veniam debitis quod ab possimus.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 12, 1, 'Eveniet adipisci hic non provident dicta consequatur.', 'Qui illo fuga fuga. Porro quia sed dignissimos cum aut eum et corrupti. Expedita officiis minima ab illo rerum voluptatem.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 4, 1, 'Voluptas labore unde reprehenderit tenetur dignissimos.', 'Quo adipisci id asperiores ea sunt sed perferendis. Impedit quo accusantium reprehenderit rerum. Quia dolor sapiente non ut provident.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 15, 1, 'Dolor officia consequatur ab maiores voluptatem.', 'Necessitatibus qui esse earum maiores dolores quia. Ducimus distinctio debitis aspernatur et ut praesentium deleniti et. A et id repellendus. Non ratione impedit voluptatem mollitia aut.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 13, 1, 'Quos omnis dolorem blanditiis necessitatibus.', 'Illum error quod fugiat a sunt ipsum consequatur et. Sunt quos eos sit maiores earum aliquid numquam quam. Ipsa ipsa occaecati quae tenetur accusamus. Dolorum ut quia sit molestiae nam.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 18, 1, 'Quisquam quia aut non dolores voluptas repellat.', 'Quia blanditiis et illum nam quam. Numquam odit aspernatur qui pariatur.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

DROP TABLE IF EXISTS `proyectos`;
CREATE TABLE IF NOT EXISTS `proyectos` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_termino` date NOT NULL,
  `descripcion` text COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`id`, `titulo`, `fecha_inicio`, `fecha_termino`, `descripcion`, `estado`, `created_at`, `updated_at`) VALUES
(1, 'Annamae', '1985-10-25', '2007-06-04', 'Fuga minima similique a. Dignissimos quod incidunt eum nisi. Qui eius aspernatur iure. Alias omnis corrupti amet inventore dolore ut ea.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 'Earlene', '1975-08-28', '2005-05-09', 'Qui molestiae unde sint est. Et autem ex omnis minima sint. Tempora sunt corrupti ut animi est reprehenderit sit. Libero omnis doloremque culpa eius est earum dolorem.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 'Hoyt', '1986-02-12', '2011-01-31', 'Unde illo quis dolores quam unde aliquam. Et ipsum nihil blanditiis atque. Deleniti eum cupiditate cupiditate eos qui rerum aut. Est veniam nesciunt illum.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 'Kaci', '2007-08-16', '1979-04-06', 'Voluptates debitis ea distinctio vitae quisquam. Quod saepe voluptatem minima aut consequatur sed adipisci.\nRerum nostrum iure consectetur et quis a adipisci. Dolore nulla adipisci quia ducimus.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 'Glennie', '2008-04-19', '1990-12-15', 'Ut quia iste facilis rerum. Iusto et quas in animi sapiente adipisci. Id non accusamus consequatur et aut. Aperiam pariatur dolores maiores.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 'Rosalee', '2012-06-24', '1995-12-16', 'Autem vero vel fugiat at delectus voluptatum ipsum. Exercitationem adipisci quod nihil modi sequi delectus. A non perspiciatis quia dolorum odit.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 'Marietta', '2002-09-17', '1977-09-30', 'Voluptate impedit est nulla commodi quod. Optio culpa veniam architecto quo minus aut. Nobis qui unde porro ea veniam.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 'Alexander', '1981-01-18', '1980-10-23', 'Sunt dicta in excepturi in corporis nostrum voluptate. Maiores rerum illum quibusdam natus dignissimos. Ducimus sed enim repellendus aut dolores et atque veniam.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 'Maxwell', '1970-02-26', '2009-12-25', 'Unde adipisci corrupti quibusdam sint. Non doloribus necessitatibus aut velit voluptates. Qui accusamus quia nihil sed sed.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 'Nakia', '2010-07-16', '2005-06-14', 'Dolores enim non et maiores libero. Modi vel quia porro quasi rerum earum. Et et quos veniam fugit sint qui. Optio debitis asperiores sed in.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 'Rosalinda', '2012-01-12', '2010-12-18', 'Aut harum architecto sed sed iste qui consequuntur. Et excepturi nisi ipsum excepturi molestiae nam ex. Eum molestiae voluptate aut et.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 'Kendall', '1988-10-11', '2005-05-10', 'Fuga et eum velit maxime excepturi. Reiciendis magnam illum veniam aut eius. Natus temporibus rem sunt assumenda voluptates modi ducimus.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 'Blanche', '2016-08-14', '1995-01-17', 'Eos at qui eum voluptatem quae qui. Voluptatem est sit ea quo. Quisquam ipsa amet non.', 'IN', '2016-10-26 18:50:04', '2016-10-27 23:03:34'),
(14, 'Furman', '1973-02-19', '1979-08-07', 'Nulla facere dolorem illo. Iusto harum numquam consequatur enim autem voluptates. Eius tempore maxime quia excepturi architecto laudantium. Consequatur recusandae eius est.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 'Jaime', '2013-08-10', '1986-01-31', 'Totam vero repudiandae totam aut qui repellendus aperiam veniam. Asperiores dolores voluptatibus iste iste quibusdam. Possimus impedit ut repudiandae quia.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 'D''angelo', '1971-09-30', '2016-04-11', 'Non dolore veritatis quo aut dolorum tempora qui commodi. Odio incidunt ut quod ut. Laudantium quis qui voluptatem expedita perferendis debitis. Consequatur officiis enim ipsum impedit optio.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 'Mia', '1980-09-04', '1994-08-17', 'Ut fugit non ducimus ea. Voluptate et voluptatibus maiores repellendus. A et omnis optio deleniti reiciendis nihil ducimus.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 'Melody', '2015-05-18', '1988-11-22', 'Aperiam qui corporis est rem modi fuga esse. Quia nihil reiciendis eum. Doloremque hic fuga distinctio possimus et maxime.', 'AC', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 'Shana', '2002-12-30', '1979-09-24', 'Quis veniam enim ut occaecati non debitis. In officiis aut rerum corrupti enim voluptas ipsum. Aliquam nihil at in qui modi debitis facilis harum. Necessitatibus autem eos ut beatae.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 'Ernie', '1993-02-17', '1990-11-20', 'Magni asperiores repellat asperiores blanditiis voluptatem dicta. Eum aut dolorem nisi eum et. Enim a sit harum odit. Voluptatem sed itaque tempore at voluptatem mollitia omnis.', 'IN', '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_miembro` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `comentario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id`, `id_miembro`, `id_pregunta`, `comentario`, `created_at`, `updated_at`) VALUES
(1, 7, 3, 'Rerum itaque laboriosam deserunt laudantium placeat. Ullam nihil et qui nam et nobis. Praesentium molestiae iure quo expedita.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 12, 7, 'Dolorem dicta veniam suscipit ut facere est a. Repudiandae veniam perferendis labore vel itaque reprehenderit omnis. Alias similique est qui.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 1, 20, 'Autem est qui ut. Quis voluptatem iusto nam nihil harum deleniti asperiores. Expedita repellendus dolorem cupiditate sunt recusandae repellendus ea et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 1, 16, 'Tempora omnis tenetur vel eaque animi magnam. Sapiente eos ea deleniti occaecati voluptates sit in laborum. Ut suscipit cumque dolorem nesciunt. Modi et nemo cum velit aut dolorum vitae adipisci.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 19, 11, 'Ipsa alias natus repudiandae quasi quos repellendus. Cumque dignissimos atque non aspernatur at sit sed. Ut eaque dolorum eveniet. Maiores iure enim corporis molestiae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 2, 10, 'Et nesciunt qui delectus veniam voluptatem distinctio. In quia at et veniam ea dicta. Voluptatem sunt saepe in velit nihil illo nulla. Voluptatibus et a quia numquam vero labore.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 17, 12, 'Commodi temporibus ut aut odio et. Tempora non delectus ea non provident sed. Temporibus fugit iusto molestiae id eius voluptates qui.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 15, 3, 'Minima nihil ut voluptatem ut qui cumque. Suscipit ab alias explicabo eum. Dolor ea non occaecati praesentium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 4, 20, 'Omnis voluptatem magni voluptatibus culpa. Animi animi laboriosam deserunt. Facilis id alias id sed maxime suscipit voluptatum qui.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 10, 5, 'Harum molestiae iure molestiae ut aut quisquam dolor. Odit nam facilis architecto ex in non. Qui quasi suscipit et qui. Ipsum eligendi quia est explicabo quia alias dolor.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 1, 17, 'Labore perferendis facilis illo similique voluptatibus. Magni quibusdam molestias tenetur consequatur. Animi iure alias et molestias.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 4, 7, 'Sit ipsam eos et aut corporis delectus. Non optio dicta suscipit id. Sed consequatur laborum ex. Quia voluptatem perferendis qui et esse corporis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 5, 13, 'Fugiat aliquid autem minus quis dolore enim vel. Unde maxime porro rem voluptatem qui. Expedita voluptatem neque doloremque consequatur.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 13, 20, 'Voluptates odio vero nam distinctio nobis. Consequuntur nobis magnam quibusdam adipisci cum. Dignissimos magnam nihil sunt. Et nisi aut quo nobis autem facilis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 2, 6, 'Perferendis saepe sequi dolores dolorum et in sunt. Qui quis veniam et quibusdam sed earum. Eaque eos voluptatibus voluptates libero recusandae. Quae voluptatum explicabo rerum et aliquid occaecati.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 8, 4, 'Maiores natus voluptatem et et aliquid provident tempore. Blanditiis consequuntur quos inventore enim. Quam quis in natus beatae qui suscipit officiis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 1, 13, 'Sed aperiam quibusdam quibusdam in. Tempora doloremque molestiae et rerum. Eum suscipit sed alias corrupti et mollitia doloremque aliquid. Dolores quam ipsum optio est.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 2, 7, 'Rerum labore occaecati commodi doloremque odit molestiae. Et tempora eum eius aut corrupti quia. Corrupti explicabo minima nihil assumenda tenetur saepe. Eos minima eaque ad alias molestiae quam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 7, 9, 'Non a sapiente quo beatae occaecati eaque. Omnis possimus et ab iste voluptatibus nulla.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 6, 12, 'Rerum repudiandae natus cumque vel expedita. Quia vel et qui vero quae assumenda. Nam maiores hic sint magni laudantium. Placeat ullam dolorum facilis dolorem sint assumenda.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(21, 6, 10, 'Et saepe delectus occaecati et. Eum non nemo vitae provident facilis molestias.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(22, 5, 16, 'Est modi unde quidem. Magni quidem ut quos et. Quisquam dicta officia deleniti vitae sit aut accusantium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(23, 11, 13, 'Atque illum modi voluptas. Nihil dolor non natus quisquam voluptas voluptatem deleniti. Porro eos in nostrum illum corrupti vitae ex.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(24, 17, 19, 'Nisi tempora eos numquam qui consequatur et dignissimos. Adipisci sint rem eos. Eveniet libero incidunt dolores aliquid quia rerum laudantium. Alias id voluptates excepturi accusamus.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(25, 2, 4, 'Ipsam sed sit ipsam rerum reprehenderit illo ut id. Et hic enim consequuntur ex. Impedit id ut ea ut velit aspernatur magnam. Et odit assumenda asperiores. Aut aut aut nihil unde.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(26, 20, 15, 'Cupiditate a optio quibusdam aliquam nulla voluptas. Fugit eveniet consectetur expedita. Velit eius eos praesentium illum consequatur cumque nihil sit.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(27, 14, 20, 'Quisquam et nihil totam quasi illum architecto quisquam. Molestiae odio aut animi assumenda consequuntur. Dolorem est non qui. Est occaecati rerum qui ipsa rem minima.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(28, 16, 15, 'Minima nulla et eligendi magnam repudiandae. Eum et unde ipsum sunt.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(29, 16, 16, 'Et sit delectus sunt voluptates. Omnis harum quisquam error dolor. Quis temporibus inventore qui quos vel et rem. Qui quia aliquid voluptate laborum autem perferendis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(30, 18, 8, 'Magnam occaecati beatae cupiditate nam et cum. Maiores quia sint quos et. Et suscipit perspiciatis vitae eum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(31, 5, 10, 'Quia illum id magni qui. Quia nisi et iste est repudiandae ratione qui. Laborum aut nihil ut fuga aut nihil. Corrupti enim nobis quibusdam commodi optio molestiae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(32, 16, 16, 'Libero repellat numquam omnis harum unde. Est temporibus rerum illum placeat voluptatibus. Natus nemo aut sit ex. Omnis similique rerum assumenda mollitia.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(33, 10, 1, 'Dolorum porro dolore nisi maxime perspiciatis. Alias et ipsa eos aut ut. Error impedit odit ut suscipit rerum explicabo. Quidem et sint non ducimus temporibus excepturi sed.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(34, 13, 15, 'Omnis non fugit blanditiis quaerat. Rerum nihil officia aliquid vitae ipsa repudiandae. Sit natus architecto a quia. Maxime voluptates ea quia rerum omnis fugiat tempore.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(35, 8, 7, 'Et nesciunt itaque vel et distinctio alias. Quia aut et voluptatem temporibus veniam id voluptas ea. Voluptas quo nam at omnis dolorum aut. Veritatis voluptas sapiente culpa non quos sint.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(36, 3, 16, 'Est quam sit quo. Distinctio consequatur sit et nulla culpa quaerat. Voluptatem odio a quia ea et sit ab.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(37, 5, 8, 'Voluptatem numquam modi tempore culpa optio ut voluptas molestiae. Dolore praesentium aut id aut. Nihil et dicta ad tenetur recusandae impedit est alias. Ut ut hic aliquam et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(38, 18, 18, 'Omnis dignissimos recusandae quam aut amet ipsam sed. Fugiat sunt pariatur sunt omnis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(39, 9, 18, 'Eos inventore consequuntur sed voluptatum dolor vitae architecto. Magni et laborum autem voluptas quasi ipsa sunt. Iusto quaerat sit sed voluptatem veniam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(40, 12, 6, 'Quam cum nesciunt non soluta qui ad similique. Et modi incidunt est. Sapiente quia nam dolores suscipit sunt sit vel.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(41, 12, 13, 'Perferendis temporibus atque cumque illum amet non repellat. Earum quibusdam cumque qui sit et. Rem ut earum animi ipsam labore. Enim cupiditate incidunt suscipit neque autem consequuntur qui.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(42, 16, 20, 'Laborum provident facere eos. Et et id doloribus maxime doloremque. Ducimus sint dicta voluptatum qui dignissimos ducimus. Dicta veritatis mollitia qui. Totam quod odit ut ut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(43, 9, 19, 'Eos accusantium consequatur blanditiis ipsum. Ullam culpa voluptate incidunt et. Sed iure officia ut dolores officiis quam. Rerum eveniet perferendis sit eum est ut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(44, 8, 8, 'Et illum maiores voluptatem vel. Et asperiores quasi quo architecto quo.\nNihil eius reprehenderit eligendi et aut vel. Quia non laudantium autem veniam minima. Debitis vel molestiae sapiente ut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(45, 13, 15, 'Non numquam omnis amet nam facilis. Vel qui atque nihil. Consequatur cum minima sint. Magni eum incidunt repellendus aut a.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(46, 4, 5, 'Neque molestiae dolor voluptatem. Voluptatem enim minima ut molestiae et totam. Omnis esse doloribus cupiditate minima officia rerum aspernatur. Exercitationem repellat ipsam unde impedit eum ea.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(47, 6, 16, 'Totam ut perspiciatis ut omnis. Sit et ratione eos quia officia hic neque et. Itaque modi atque aperiam ab veritatis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(48, 2, 20, 'Et enim aliquam quo. Sunt nihil nulla sint odio tempora enim. Officia fugit deserunt magnam alias omnis voluptate voluptatum et. Sed quod voluptates velit nisi.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(49, 18, 12, 'Dolores adipisci ut esse repellat modi hic. Occaecati ut consectetur ratione deserunt aut. Reprehenderit distinctio distinctio et reiciendis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(50, 17, 15, 'Voluptatibus doloribus culpa esse. Aut odit consequatur quis qui officiis et. Cum odit voluptate nisi unde excepturi id suscipit quis. Deleniti et in et hic dicta.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(51, 17, 8, 'Nisi optio maxime saepe incidunt et. Doloribus esse facere esse rerum. Omnis vel distinctio doloribus voluptas totam occaecati magnam. Illum rerum saepe earum omnis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(52, 14, 13, 'Similique dolor vitae beatae iure earum. Perspiciatis fugiat repellat neque sequi inventore laudantium consequuntur. Id dicta iure quo ea. Quia non recusandae non eos.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(53, 2, 6, 'Magnam deleniti qui nulla animi repellat totam. Totam voluptas aut voluptatem quam ratione. Harum ea non fugit dolor. Et omnis dicta totam possimus voluptas ipsa sunt.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(54, 20, 17, 'Ex aut et voluptatem minima quis debitis. Laborum rerum consequatur doloribus eaque adipisci magnam saepe qui. Enim totam debitis velit vel aut. Ea quis dolorem aliquam cumque at.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(55, 6, 19, 'Et omnis ipsum hic. Commodi et voluptatem reiciendis omnis. Autem recusandae voluptas et magnam. Quibusdam ad explicabo aut aliquam ex repellendus culpa.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(56, 13, 6, 'Iure et officiis non nobis aut et aut. Et dolorem necessitatibus ad aut. Ut iusto quod doloribus nesciunt sit et. Fugit fugiat atque architecto incidunt molestiae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(57, 15, 11, 'Sint laudantium tenetur occaecati sed numquam molestias. Illum architecto fugiat cum dolor ut. Autem id velit est et quae distinctio. Tenetur qui repellat sit dignissimos repellendus id sed.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(58, 2, 8, 'Natus illum vel et et. Est enim consequuntur aut quam. Nemo incidunt quidem illo repellat nam aperiam. Recusandae dignissimos est non id omnis reiciendis. Magnam sed et et autem nobis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(59, 14, 6, 'Aspernatur culpa officia omnis. Et inventore libero molestiae ad at natus qui consequuntur. Expedita magnam alias non cupiditate assumenda dolor. Esse est autem sint accusamus sapiente.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(60, 4, 3, 'Nam atque consequatur odit perferendis est et modi. Occaecati dolor est autem quasi architecto. Repellat assumenda eligendi inventore et eligendi. Ad qui aliquam est aut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(61, 5, 5, 'Veniam et et est repellat explicabo quo. Delectus corporis non sapiente voluptatem repudiandae nihil officia. Impedit quia voluptatem est consequatur illum aut deleniti.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(62, 4, 14, 'Est totam est suscipit error. Accusamus dicta culpa sint et et. Quaerat consequatur voluptate nihil nesciunt sit voluptatum. Qui at est tenetur vel et numquam et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(63, 11, 13, 'Illo quia qui dignissimos. Totam molestias voluptatibus ipsa et ipsam. Asperiores aperiam voluptas temporibus odit dolores velit quam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(64, 16, 3, 'Ullam placeat perferendis consequatur omnis reprehenderit minus. Voluptatum minima quia magnam quis illum. Fuga et reiciendis facilis ut asperiores fugit cumque.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(65, 18, 20, 'In reprehenderit ex inventore. Eum possimus sint eaque neque iste animi assumenda. Laborum sed alias id. Qui neque occaecati nemo consequuntur praesentium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(66, 15, 14, 'Architecto a suscipit eius et ut voluptates et. Quo asperiores sint numquam assumenda beatae consequatur sequi. Quos libero et eos provident vel voluptas.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(67, 14, 17, 'Ducimus fuga modi quis quis itaque suscipit. Sit aspernatur sint quia nulla cum at. Modi doloribus occaecati temporibus voluptatem ipsa voluptas.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(68, 15, 3, 'Esse enim deserunt rerum ex molestiae nisi. Aut et voluptas repellendus molestiae ullam consectetur assumenda. Rerum perferendis unde eos et quisquam quae quas.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(69, 15, 2, 'Incidunt officiis itaque ea sed. Quisquam et impedit eum maiores vero labore. Nemo molestias id quis vero error consequatur quos.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(70, 11, 1, 'Qui architecto ipsam sequi laborum. Rem possimus dolorem eos esse et. Quas sed dolores et dignissimos. Tempore a sunt saepe quas quo magni vitae nesciunt.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(71, 16, 19, 'Nihil voluptatum assumenda rem. Autem delectus nisi id et quia. Consequatur illo fuga excepturi. Libero eaque necessitatibus rerum enim est.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(72, 11, 18, 'Quidem placeat architecto rerum voluptas quam. Sit ad velit voluptatum dicta ipsum enim sint. Qui quam a odio nulla assumenda. Voluptates deserunt sapiente accusantium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(73, 11, 6, 'Est expedita dolorem aut ut esse incidunt quis fugiat. Adipisci nesciunt veniam eaque omnis error sunt. Alias nihil aut sunt perferendis labore aut itaque delectus.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(74, 20, 13, 'Voluptatum rerum quia maiores ducimus provident. Nisi voluptas nesciunt doloremque aperiam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(75, 16, 2, 'Nisi earum nesciunt dolor sint accusantium. Qui ut sunt placeat earum officiis. Facere est quo quasi laborum. Voluptas officiis et et explicabo.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(76, 14, 3, 'Velit sed et maxime. Fugiat fugit sapiente consequuntur quo vel ad. Ea molestiae aut dolores quis ut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(77, 3, 11, 'Repellendus dolore ab quas corporis dolores fuga enim. Nihil molestiae asperiores numquam ut. Maiores recusandae et est aut quisquam maiores aut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(78, 10, 7, 'Ut quia quam id aut. Et amet et dolores ea repudiandae cupiditate et. Et voluptatem dignissimos natus sunt quae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(79, 19, 8, 'Ex esse repudiandae provident molestiae nobis quia ex. Sunt est deserunt distinctio voluptas. Sit voluptas dolorem molestiae sunt repellendus.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(80, 16, 16, 'Impedit et et voluptates voluptatem dolores labore. Ducimus non omnis deleniti vitae et ut porro eum. Debitis non rem amet.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(81, 8, 5, 'Aut fugiat autem expedita sit sit hic. Dolor dolores totam et iure et quos. Minus unde nihil architecto qui. Quos quis recusandae nobis reprehenderit maiores dolorem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(82, 19, 3, 'Non molestiae aperiam animi quis. Ea quaerat non repellendus omnis voluptatem dolore. Voluptatum aut quo et. Aperiam dolorem animi similique esse dolor nulla tenetur.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(83, 13, 5, 'Dolorem id corporis ipsa veniam quod asperiores. Quia numquam dolorem et dolorem nobis eveniet.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(84, 20, 10, 'Quas voluptatum eos omnis et corporis voluptatem. Quam et ad modi tempore voluptatem. Molestias voluptas sunt velit fuga. Eos autem eligendi aperiam magnam minima omnis minima incidunt.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(85, 5, 16, 'Tempora est minus officiis ipsam atque. Repellat doloremque voluptatem omnis voluptate. Maiores aperiam deleniti aperiam necessitatibus ut nulla.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(86, 1, 19, 'Qui laborum placeat ut quisquam. Eum nesciunt odit voluptatem quo quae iusto corporis. Aspernatur assumenda id qui exercitationem inventore qui illum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(87, 10, 18, 'Deserunt impedit nihil beatae blanditiis voluptas at dolorum. Sed amet quia rerum in. Ipsa tempora itaque ducimus eveniet qui est ipsum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(88, 9, 15, 'Doloribus sit dicta officiis itaque. Quam aut adipisci consequatur ut. Magnam officia mollitia nam. Mollitia minima dolor adipisci quis. Deserunt harum dignissimos rerum eaque.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(89, 17, 3, 'Temporibus ipsum ea beatae tenetur sunt. Fugiat eligendi dolorem pariatur. Quia magni id nisi ipsum et vitae. Omnis sit reiciendis ea quis natus. Deleniti exercitationem at occaecati sint fugit aut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(90, 5, 4, 'Dolore maiores eius quos necessitatibus consectetur perferendis autem. Accusantium et voluptatem a perferendis veniam. Reprehenderit labore ullam laboriosam et et unde.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(91, 3, 7, 'Autem dolores tempora suscipit non dolor. Qui amet laboriosam et quis autem. Totam ut et quo suscipit voluptatem. Officiis possimus doloribus porro fuga dolores labore quibusdam quibusdam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(92, 1, 15, 'Nesciunt nostrum eum officia. Ut laudantium ut vitae quia non et officiis. Reprehenderit enim vero porro nulla quo temporibus.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(93, 20, 13, 'Et nostrum aut labore est quo. In et qui porro amet dolor nulla architecto. Ad in culpa ex aut nam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(94, 6, 2, 'Non et soluta minima doloribus consequuntur natus. Debitis repellendus atque nam aspernatur. Ipsa dolorem nam ut. Dolore accusantium odit sed sed aspernatur reprehenderit.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(95, 15, 5, 'Temporibus vel qui distinctio porro explicabo. Nemo eligendi nihil neque quos reiciendis est. Unde voluptatem quae optio consequatur quasi.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(96, 20, 2, 'Sunt maiores commodi ullam cumque. Doloribus quia excepturi eos.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(97, 3, 2, 'Explicabo et fuga praesentium sit. Rem qui nam nostrum vel hic dolor quo possimus. Dolorum aperiam aut animi pariatur. Iste ut consequatur omnis provident ab.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(98, 14, 19, 'Voluptatibus et pariatur maiores et modi cupiditate vel veniam. Tempora corrupti ratione quam exercitationem molestias ullam. Hic deserunt ab id id et maiores et. Quis id quis iure qui ratione rerum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(99, 12, 3, 'Qui voluptas praesentium sit ullam ipsum. Odio id et itaque eveniet dolorem quo. Minus molestias dolores odit rerum dolor. Aut ut ut modi.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(100, 12, 14, 'Quasi voluptatem ullam sapiente quasi esse et. Tempora accusantium fugiat deserunt. Eos quia reprehenderit rerum nobis at qui. Architecto atque et sit sit et occaecati.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(101, 19, 12, 'Odio dolorem ut consequuntur nam. Omnis voluptas at accusamus maiores non qui maiores ratione. Nemo sit laudantium blanditiis magnam est illum unde.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(102, 13, 8, 'Sint et veniam ut est dignissimos dolorem voluptates. Placeat est dolores numquam. Esse vel qui non sed rerum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(103, 14, 7, 'Autem amet dolor enim sint ratione. Occaecati ex quam fuga nihil qui. Nostrum ducimus fuga similique sint. Unde et molestiae in tenetur nihil et. Qui provident suscipit non officiis dolor error.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(104, 8, 7, 'Cupiditate provident facere nam aut. Eum neque quia dolores odit consequatur mollitia velit perspiciatis. Vel odit non mollitia voluptatem amet optio.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(105, 4, 11, 'Odit aspernatur consequatur molestias. Quis et aspernatur quisquam ab perferendis veritatis placeat. Qui tempore rerum qui harum sint maxime.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(106, 16, 3, 'Voluptatem sed autem qui mollitia assumenda debitis. Sit iusto veniam ut. Consequatur dolores laboriosam vel perferendis. Corporis assumenda natus omnis et molestiae autem nihil.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(107, 13, 20, 'Qui reiciendis et corporis officia ducimus minima rerum consectetur. Autem delectus ut velit temporibus sint. Aut alias voluptates id omnis. Reprehenderit quos cumque voluptatem ea fuga voluptatem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(108, 17, 15, 'Consectetur praesentium et laboriosam voluptatem. Est voluptatibus suscipit tenetur illo quis eius. Distinctio sed non in voluptas quia iste quia. At debitis repellendus inventore facere dolorem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(109, 12, 8, 'Quod veniam ad nam ad velit soluta dolor. Ratione labore numquam dolore facilis officiis neque id rem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(110, 20, 1, 'Voluptate distinctio molestiae impedit quia temporibus non. Qui pariatur et nesciunt placeat ea. Ullam omnis autem adipisci quis mollitia praesentium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(111, 12, 12, 'Enim voluptas quasi et sed adipisci doloribus. Porro exercitationem numquam autem non natus aliquid veritatis. Dolores explicabo qui quis quae distinctio.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(112, 5, 2, 'Nemo aliquam nulla tempora molestiae porro voluptatem. Assumenda in beatae ratione ut. Autem sint voluptas quo alias. Eos rerum et quae qui delectus id totam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(113, 18, 2, 'Est consequatur consequatur facilis voluptatem qui modi illo. Voluptatem eos odio asperiores nesciunt est. Enim ut aut corrupti voluptas.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(114, 10, 1, 'Voluptatem eius maiores eos adipisci rerum facilis ab. Quasi alias qui et ut aliquam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(115, 17, 12, 'Nisi ipsam tempore asperiores quae est sit. Velit quod mollitia sit autem. Assumenda veritatis accusamus sed est. Qui consectetur alias quia molestiae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(116, 19, 9, 'Eaque minima dolor nihil odio. Consequatur ipsam veniam aut enim qui porro laudantium. Nesciunt non impedit ut quisquam reiciendis voluptate voluptates. Quasi quo excepturi deleniti sit.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(117, 11, 4, 'Iure laboriosam quaerat cupiditate non perspiciatis sit. Voluptatem amet officia distinctio similique. Nisi deleniti est porro laboriosam dolorem. Ut autem odio ipsa accusantium eum occaecati.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(118, 14, 17, 'Eligendi et aut et earum non recusandae sapiente. Dolorem qui impedit tenetur ut commodi temporibus deserunt. Rem minus unde ratione non labore.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(119, 11, 13, 'Nesciunt id inventore ipsum enim nesciunt aut similique aut. Autem consectetur error ut optio architecto eum. Molestias nostrum sed veniam exercitationem laborum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(120, 9, 17, 'Quasi et beatae exercitationem sapiente ipsum. Sit laboriosam earum harum ut vel cumque ea. Et incidunt at nemo quod ratione. Aut pariatur quia a rerum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(121, 2, 7, 'Ducimus voluptatibus ut sequi exercitationem. Debitis officia et quibusdam aut ea ad. Veniam iusto velit aperiam praesentium.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(122, 7, 17, 'Et sit ipsum quisquam voluptatem cupiditate. Mollitia aspernatur assumenda minima dolor vitae est sint. Accusamus inventore omnis nihil reiciendis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(123, 1, 20, 'Facere asperiores animi nisi ducimus accusamus molestiae illo. Dolorum id omnis iusto quibusdam.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(124, 13, 7, 'Quas nobis tempora debitis sit est maiores atque. Non nam eos aspernatur. Et ducimus ut omnis hic et consequatur. Sed nemo quia quo rerum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(125, 3, 1, 'Dolorem magnam eveniet sunt. Quia suscipit consequatur et dolores qui ut nemo.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(126, 14, 1, 'Ipsum nulla magni aperiam error. Dolor placeat molestiae natus iure consequatur esse. Assumenda est nostrum repudiandae alias et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(127, 15, 18, 'Molestiae cumque qui eum doloribus animi. Facilis quo sint praesentium sed cupiditate. Fuga porro dolores aut rerum. Odio error commodi dolore officia laudantium ad.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(128, 11, 11, 'Magni rem porro nisi unde. Culpa et est sed nobis voluptas nulla nostrum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(129, 13, 19, 'Rem a similique unde expedita saepe esse tempora. Consequatur est alias consequatur ad voluptate. Quis quas quibusdam delectus. Animi vel ut soluta magnam fugiat libero.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(130, 9, 17, 'Esse magni exercitationem dolorem perferendis natus. Aut ullam fugit laborum. Numquam amet esse et nisi. Consequatur eos deleniti ab voluptatem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(131, 11, 7, 'Saepe rerum labore omnis inventore. Sint exercitationem beatae et. Commodi praesentium similique numquam officia commodi qui est.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(132, 6, 7, 'Repellendus modi fugit delectus natus natus aperiam. Est quaerat quasi non facere ut optio. Hic et voluptate omnis hic non qui. Illo velit illo nam est quae amet beatae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(133, 4, 12, 'Necessitatibus ea sint impedit qui quos at omnis. Et est voluptatem ipsa et neque asperiores et. Adipisci et quas voluptas quia animi provident.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(134, 9, 5, 'Odit eum nam ad beatae sint quis provident. Vel culpa aut rerum in porro ut. Laborum vel suscipit sit occaecati excepturi illum quis. Tempora et cumque rerum et ea reiciendis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(135, 6, 3, 'Sed esse odio veritatis accusamus asperiores commodi debitis et. Provident culpa nam quae quia nisi reprehenderit. Culpa inventore dolor id dignissimos repellat quos. Sed rerum non quae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(136, 4, 3, 'Consectetur omnis ut provident odit. Enim quos nemo laboriosam voluptate qui magni. Culpa et similique qui non. Placeat esse dolorum recusandae.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(137, 4, 10, 'Asperiores qui nulla quo aut quia assumenda id. Ipsum itaque culpa quia et et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(138, 10, 8, 'Odio ut ut error et molestiae ullam fugiat rerum. Expedita quidem qui et maxime sapiente autem aut. Quod atque inventore eos voluptas maxime. Cumque iure neque non non ipsa sint et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(139, 15, 18, 'A ea praesentium sapiente sit. Itaque qui optio vel pariatur sapiente molestiae et non. Provident corporis quidem tenetur quos et ea.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(140, 13, 8, 'Ut itaque voluptatem quia cum omnis consequatur velit. Qui fuga et voluptatem architecto sit. Officiis molestiae ad harum quo perspiciatis at. Est et consequuntur quaerat dolor.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(141, 7, 8, 'Omnis recusandae dolorem rerum fuga. Ut quia voluptatem sit explicabo. Et dolores officia illum et quisquam optio et. Sed incidunt a iste. Provident et quo impedit voluptatem quod dolorem sunt aut.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(142, 20, 8, 'Illum soluta et voluptatem deserunt quae dolores tempore. Quia at eos autem dolores nulla facilis. Accusantium autem nesciunt quod quia iure sit consequatur.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(143, 20, 15, 'Ratione quaerat consequatur ad eum. Et enim sint consequatur sapiente quod porro est. Temporibus voluptate corporis accusantium quibusdam explicabo amet est voluptatibus.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(144, 20, 8, 'Ut aut recusandae perferendis quia voluptatibus sapiente ut. Autem sequi velit saepe qui modi aut. Accusantium quibusdam repellendus necessitatibus iste sed.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(145, 2, 12, 'Similique fugit aliquid minima enim. Consequatur debitis eum sint nihil non est quam qui. Repellendus repellat expedita non sequi. Perspiciatis nulla reprehenderit sunt facere et deserunt voluptatem.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(146, 20, 16, 'Laborum velit est provident nobis et. Ratione aliquam quod alias totam voluptates accusamus. Magni eaque aspernatur voluptatum vel voluptates. Dolores dolor error dolorem fuga rerum.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(147, 12, 12, 'Itaque perferendis voluptatibus magnam. Minima nostrum ut numquam molestiae aliquid est delectus eligendi. Iure et tenetur et recusandae blanditiis.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(148, 20, 13, 'Deserunt error libero aut nulla. Praesentium amet consequatur harum veniam voluptates eius. Temporibus doloremque mollitia enim. Rerum totam ipsum non doloremque et itaque.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(149, 12, 12, 'Saepe officiis aut voluptatibus itaque aut culpa. Dolorem quia eveniet et qui placeat. Ipsum laudantium voluptas non quia aperiam facilis et et.', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(150, 7, 9, 'Neque molestiae minima ut ipsa. Aut ab dolores expedita beatae ducimus sed. Assumenda quam cum error et minima aut possimus.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(151, 6, 18, 'Totam harum assumenda dignissimos cum consequatur. Quo deleniti quas eligendi optio provident voluptatem. Qui voluptates dolores dolor architecto rerum molestiae recusandae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(152, 5, 18, 'Enim non in enim quia rerum animi. Iusto animi sapiente incidunt atque non voluptatem. Aut ipsa at est delectus.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(153, 2, 9, 'Beatae nihil est itaque. Exercitationem numquam odit iste ipsum delectus ipsam. Error mollitia et ut voluptatem.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(154, 2, 10, 'Labore minus quod animi similique suscipit vitae iusto. Rerum sunt harum quis. Provident praesentium voluptatem omnis vitae fuga eum praesentium.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(155, 10, 8, 'Quia tempora facilis omnis similique adipisci sed. Quo cum qui iure aut accusantium dolores corrupti a.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(156, 13, 15, 'Et dolores minus quis. Consequuntur ex est necessitatibus amet. Dolores id omnis rerum dolore quia cum eaque quia.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(157, 1, 2, 'Ex dolor optio ducimus ipsum. Iste ut id at omnis ullam qui consequatur natus. Nihil distinctio voluptas natus a reprehenderit. Beatae aut quae ex rem id.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(158, 5, 10, 'Mollitia nihil saepe fuga. Quia totam quae et enim ullam ea. Excepturi sed et omnis earum eveniet nam.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(159, 14, 5, 'Ducimus vero sunt laudantium sit. Eligendi eos quia veniam placeat aspernatur. Pariatur et quia quod natus officia eos.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(160, 5, 16, 'Officia aut perspiciatis eum dolor at. Est culpa consequuntur est veniam sit amet.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(161, 5, 9, 'Asperiores dignissimos est molestiae. Tenetur quos rerum aperiam asperiores aliquid nihil quod. Odit nemo earum molestiae in minus error.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(162, 2, 20, 'In eaque omnis asperiores excepturi et quos laborum. Porro modi consectetur et tempora dolor numquam aut. Tempore dolores soluta vel veniam itaque est non. Modi dolor mollitia beatae in vitae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(163, 10, 14, 'Dolorem fugit officia occaecati minima. Aut vitae autem aliquam voluptatem voluptate expedita vitae. Rem et aut doloremque qui qui est quia. Quas est qui qui tenetur tempore omnis repudiandae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(164, 2, 8, 'Laboriosam assumenda ut sit dolores. Voluptatem enim voluptas temporibus est id quidem rerum. Ut eos a non beatae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(165, 7, 2, 'Fugit animi et delectus dolor minima asperiores cum. At aut nemo est reprehenderit occaecati. Id ipsam eos est accusamus sit harum. Quis voluptas molestiae nobis et et est sunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(166, 1, 20, 'Voluptates exercitationem qui doloribus sint quasi. Repellat pariatur exercitationem ipsam aliquid accusamus accusamus. Eveniet earum eos sit repudiandae ratione veritatis.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(167, 13, 20, 'Animi et minus quisquam quibusdam a. Nihil ex tenetur magnam dolor aut omnis sit cum. Qui veritatis mollitia temporibus at minima qui. Labore est ut sunt possimus delectus.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(168, 7, 13, 'Quam est qui officiis quia atque vel. Possimus corrupti eos alias ab. Eos quidem est hic quisquam corrupti.\nSint odit odio aspernatur aliquam est. Repellat culpa dolore cupiditate quos optio eaque.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(169, 2, 10, 'Porro est facere rerum dolores. Qui iste doloribus velit nobis quia beatae a.\nEx ea neque non facere architecto. Dicta et dolorem aut expedita qui velit. Nemo tempora ratione ipsum deleniti.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(170, 12, 19, 'Similique odio iste cumque officia. Quidem quos quisquam cupiditate autem ex accusamus omnis. Deserunt autem at dolores labore.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(171, 17, 5, 'Veritatis eligendi fugit laboriosam et repellendus. Numquam qui vel ea porro consequuntur explicabo consequatur. Voluptatum facere sequi molestias harum.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(172, 12, 6, 'Velit aut laborum voluptatum aut accusantium. Labore accusamus officia ex mollitia. Excepturi iure deleniti rerum fugit perspiciatis dolores.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(173, 13, 1, 'Laudantium et voluptatibus qui dolores illum sint et. Molestias quos consequatur iste aliquam qui qui quo. Incidunt distinctio nobis voluptatem magni.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(174, 6, 10, 'Consectetur nulla ipsa distinctio voluptatem sit et. Cum ullam est voluptatem consequatur. Corrupti quos vel voluptatem voluptatem iure excepturi eos ut. Et est quis sed aspernatur ex.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(175, 9, 2, 'Sequi eveniet ut autem soluta. Doloremque enim et tenetur non voluptatem laborum quasi. Qui magnam sit in voluptas.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(176, 5, 14, 'Fuga corporis voluptates nemo. Similique atque et dignissimos ipsa dolores. Nostrum laboriosam facere consequatur quia nihil voluptatem. Est fugiat hic aperiam et veniam molestiae.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(177, 7, 3, 'Voluptatem occaecati eum enim provident consequatur. Est sed dolores nihil. Repellat cumque fuga expedita omnis. Sit mollitia est numquam veniam.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(178, 3, 14, 'Saepe incidunt doloribus ipsum consequuntur. Id quis mollitia ducimus incidunt delectus sint eos.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(179, 2, 8, 'Dignissimos impedit aut laboriosam facere et necessitatibus quia. Amet sit accusantium rem soluta nisi dolore est. Similique delectus provident minus dolorem dolores quas.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(180, 5, 19, 'Aut quaerat porro et quia. Cupiditate est quia qui blanditiis ducimus. Deleniti dolores qui eaque et sit dicta. Corporis nulla aut natus pariatur.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(181, 11, 16, 'Ipsa nisi omnis molestiae. Sit doloremque est voluptatem. Veritatis quasi aut architecto earum voluptatem ut. Sint molestiae optio error aut.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(182, 20, 6, 'Incidunt iste qui accusamus vitae magni. Ipsam voluptatem harum ut cupiditate eveniet in. Repellat aut et tenetur vel.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(183, 2, 2, 'Dolores at et magni ipsam molestiae laboriosam. Commodi sed minima nihil libero iste nihil. Blanditiis recusandae quia ut fugit adipisci.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(184, 14, 13, 'Nihil reiciendis neque perspiciatis facilis error nostrum. Enim autem assumenda voluptates et est et suscipit. Iusto voluptas aperiam sint veritatis sunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(185, 13, 3, 'Quae et commodi dignissimos omnis. Natus totam nisi quos. Qui debitis nihil ut optio nihil sapiente.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(186, 6, 4, 'Sint voluptas provident ut architecto quae. Rerum voluptatem animi voluptatem totam animi maiores earum. Et eum doloremque in maxime quis tempore reprehenderit.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(187, 20, 5, 'Aut qui commodi omnis enim. Assumenda temporibus iure sit rem accusamus. Excepturi consequatur consequuntur cum exercitationem nostrum itaque non.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(188, 1, 14, 'Est sed pariatur rerum dolore pariatur consequuntur. Sit ex dolore blanditiis corrupti quia laboriosam. Saepe esse numquam ratione nihil ex expedita. Assumenda occaecati similique velit a rerum et.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(189, 12, 12, 'Dolor architecto nihil laboriosam consequatur minus excepturi nulla neque. Facere ut rerum culpa accusantium doloribus. Rerum veritatis cum laborum sapiente.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(190, 11, 5, 'Labore quia sit ab saepe porro delectus commodi. Aut qui perspiciatis nobis commodi nesciunt hic. Nihil ipsum ipsa consequatur voluptatum.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(191, 17, 6, 'Exercitationem consectetur et ut blanditiis reprehenderit repudiandae sequi. Cupiditate animi rem aliquam ea itaque. Iure commodi et porro sint et ea.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(192, 18, 19, 'Fugiat voluptates non nemo laborum repellendus. Veniam doloribus optio labore voluptates. Error soluta qui ducimus nobis nobis.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(193, 18, 19, 'Dolores voluptates ipsa laboriosam excepturi. Occaecati aspernatur excepturi laudantium. Consectetur dolorem provident temporibus nesciunt.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(194, 15, 6, 'Quibusdam dolores laboriosam harum ad vero consequuntur. Repellendus eveniet iusto cupiditate nulla minima accusamus. Sint commodi a sapiente eum reiciendis accusamus consequatur.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(195, 7, 10, 'Fugiat et aliquam non. Natus impedit sunt sit quia explicabo. Eius dignissimos facere qui.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(196, 20, 4, 'Ut unde autem praesentium explicabo. Expedita aut earum debitis. Natus dolor quidem voluptatem sunt totam quisquam. Molestias qui voluptatum a.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(197, 16, 3, 'Culpa quos consequatur voluptas. Vitae totam corrupti facilis omnis. Ut ratione quia magni doloribus officia illo omnis quo. Qui architecto quaerat sunt qui in.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(198, 5, 16, 'Repudiandae consequuntur nihil qui aut tempora est dignissimos. Eum ut illo ducimus. Doloribus omnis culpa cumque.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(199, 3, 4, 'Sit est ea fugiat rerum officiis saepe. Voluptate voluptates quaerat magnam totam. Dolore quia labore suscipit quisquam qui. Nesciunt soluta dolorum placeat vero.', '2016-10-26 18:50:05', '2016-10-26 18:50:05'),
(200, 1, 16, 'Temporibus corporis iste pariatur laudantium qui et excepturi. Minus nostrum eos esse in. In libero voluptatem autem vero. Est aut voluptate distinctio.', '2016-10-26 18:50:05', '2016-10-26 18:50:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
CREATE TABLE IF NOT EXISTS `sucursales` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ciudad` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `nombre`, `descripcion`, `direccion`, `telefono`, `ciudad`, `created_at`, `updated_at`) VALUES
(1, 'Aftonshire', 'Qui est quo quia sapiente nemo possimus. Consequatur ipsa enim illo doloribus ut cum consequatur saepe. Voluptatem fuga quae voluptatem consectetur.', '7808 Antonio Skyway\nLake Mossiebury, OK 87187-0824', '1-968-886-5539 x59344', 'Terrytown', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(2, 'East River', 'Expedita doloribus iure qui minus dolor et eos maiores. Rerum occaecati veniam laborum reiciendis. Quis sed ea velit modi temporibus iure. Dolor assumenda numquam voluptatem ipsa.', '778 Joseph Forge\nGenesisburgh, RI 85470-9662', '349.312.0945 x7855', 'West Cotyhaven', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(3, 'Marionland', 'Mollitia ullam laboriosam ullam. Nihil eveniet nobis aliquid est aliquam ipsam quia.', '1748 Hoeger Knolls Suite 419\nSouth Brandon, NM 00111', '869.603.9560 x17534', 'Lake Joshuah', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 'New Brionna', 'Et alias assumenda molestiae id nemo unde exercitationem. Corporis vel molestiae suscipit dolor. Voluptas laborum tempore et error. Quos molestiae exercitationem inventore eum placeat vel.', '321 Carlos Hollow\nNorth Evelyn, DE 20010-4559', '1-375-851-6112 x9663', 'East Kylee', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 'West Miguel', 'Ipsa nihil sed in atque odio nostrum repudiandae aut. Aut exercitationem quia distinctio molestias. Ad velit voluptatum inventore velit.', '57368 Efren Harbor\nWest Dana, CT 69935', '1-519-394-1504', 'Ernestomouth', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 'North Maximillia', 'Similique ullam id aspernatur. Ad sed saepe error doloremque dolore. Maxime et provident cumque. Accusantium est dolor at est qui hic.', '3168 Murphy Wall Apt. 654\nClaudineside, AK 02542-8216', '+12943474616', 'South Jarred', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 'Medhursttown', 'Quo non autem quos commodi. Laboriosam ut natus blanditiis iste qui molestiae. Necessitatibus omnis debitis asperiores quaerat sit. Ex est id sequi dolores accusamus sint.', '448 Amelia Road Apt. 132\nEast Evanburgh, MS 93284-5904', '1-360-792-0136 x451', 'McGlynnberg', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 'West Ned', 'Dignissimos sed distinctio voluptates laborum omnis optio quod. Doloremque velit dicta aliquid dolores. Enim saepe ut fugiat. Et ab quasi et temporibus culpa.', '40429 Thompson Motorway\nNew Edgarborough, DC 44422-6021', '(709) 401-8406 x8991', 'Kaelynborough', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 'Lake Michaela', 'Quis explicabo sit velit doloremque. Doloribus similique voluptatem consequatur autem quia. Iure quae aut expedita et aspernatur enim.', '2459 Sylvester Coves\nLockmantown, NJ 66290', '+17373434080', 'South Goldenbury', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 'Jackelinestad', 'Consequatur dolores illum ullam odit. Quas maxime qui tempora et. Qui mollitia temporibus placeat et.', '3488 Yundt Harbor Apt. 106\nLake Merle, AZ 86242-8447', '995.331.9134', 'South Viviantown', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(11, 'Tristianport', 'Accusantium sit numquam dolores reiciendis illum voluptas autem. Doloremque deleniti aliquid sed voluptas est voluptates dolor eum. Nihil qui et at quod.', '2951 Lisa Shore Suite 066\nPort Jeanne, HI 29382-2180', '1-342-347-4470 x10429', 'West Thomas', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 'Welchburgh', 'Voluptatibus et id molestiae vero reprehenderit incidunt. Ipsa quae aliquam quia sit et ut harum ipsum. Iure suscipit odio voluptatem quos rerum a.', '73957 Macejkovic Port\nSouth Caleighberg, SD 17512', '1-590-842-0439 x6697', 'New Loyce', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 'Armstrongfort', 'Quia necessitatibus iure architecto mollitia. Id est eveniet velit. Omnis doloremque enim fuga facere veritatis quia numquam. Cumque quo quas eum est.', '86060 Jamarcus Junction Suite 221\nSelinaland, AR 61250', '(782) 314-5142 x444', 'Gloverland', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 'Colleenhaven', 'Eum id consequatur doloremque excepturi veritatis est. Repudiandae deleniti perferendis ut pariatur. Nobis voluptatibus minus porro eos sint.', '349 Feil Path Suite 613\nStanview, SD 63000-3259', '(203) 914-6753', 'New Cora', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 'West Estellamouth', 'Corporis hic nisi dignissimos aut eveniet fugit et. Possimus esse aut dolore consequatur dicta et fugit voluptate. Omnis esse tempore accusamus. Nemo et laborum maiores optio voluptate fugiat.', '7014 Kub Corners Suite 473\nEast Ona, SC 57170-8993', '772-897-0709', 'Thoraburgh', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 'East Jazmynshire', 'Amet recusandae ab exercitationem nam. Quas vel est fugiat. Voluptas recusandae vel quia et. Alias qui inventore cumque aspernatur.', '270 Zakary Freeway Suite 593\nLadariusshire, HI 19771-9205', '+1-679-832-6172', 'East Marcellusmouth', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 'Nathenstad', 'Minus et voluptatem non ut est laudantium sunt. Ducimus aut sunt sunt vitae occaecati. Neque fugiat et id rerum.', '606 Armstrong Branch\nGrantberg, TX 91561-7360', '+1 (838) 736-5188', 'North Eldoramouth', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 'Lake Taliaborough', 'Ipsum libero excepturi et nam. Amet aut tempora est sed. Atque est mollitia reprehenderit quia ducimus qui.', '32487 Reichel Village\nSouth Jamalchester, FL 72867', '360.247.0329', 'South Sterling', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 'North Ellenmouth', 'Sed quae dolore ut aliquid vitae. Et dolorem ea repellat amet distinctio sit. Sit quibusdam et praesentium sequi.', '36881 Yoshiko Junction\nNew Ezra, ND 36430', '748-302-8556 x8991', 'Lafayettemouth', '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 'Nolachester', 'Optio sed rerum repudiandae adipisci qui perferendis et non. Dolor aspernatur esse neque sit. Rerum amet nobis error enim. Blanditiis qui consequatur cumque qui.', '561 Joanny Hollow\nSouth Helene, MO 04503-9120', '279.570.9517 x1792', 'Kadenview', '2016-10-26 18:50:04', '2016-10-26 18:50:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefono_movil` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `correo_personal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruta_foto` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_sucursal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `clave_actualizada` tinyint(1) NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `apellido`, `telefono_movil`, `correo_personal`, `ruta_foto`, `id_sucursal`, `codigo`, `usuario`, `password`, `clave_actualizada`, `estado`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'charly', 'gastelo', '978772143', 'alangb01@gmail.com', NULL, NULL, '', 'admin', '$2y$10$5pd850afj8eZQzpwGpxf0OzdJrBtjFD9hlbclRLdzrv6SYw3NviBi', 1, '', 'WKazkPDBAPOWg1bgJglyxqTw6jibVPlm3tBDodswbMxHDf5TwQUNezsIRG3c', '2016-10-26 18:50:01', '2016-10-27 23:08:45'),
(2, 'Esteban', 'Konopelski', '1-968-734-7429 x23904', 'joelle.bednar@bednar.com', '', NULL, '953727d8-38a7-39e5-bf26-53ee5e60a2fb', 'rogahn.marion', '$2y$10$MdvjBWiQDkZVodUHnzvL6ehKqj82ENP3KN1Q7EkxDk14/wneBFdiy', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-27 22:43:28'),
(3, 'Yazmin', 'Christiansen', '(898) 365-0806 x8677', 'corine30@yahoo.com', '', NULL, '50d6a497-e6d4-3325-8787-472e39b0cd36', 'schuyler.price', '$2y$10$jPIJItVQPt7KLr7P5DcG0.HkF4ZFgNLHRbtBlBORJcmVYrWsDKXBq', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(4, 'Kelton', 'Fay', '1-331-864-9877 x0172', 'carroll.lind@hotmail.com', '', NULL, 'cddd01e4-f1e1-3995-9bce-3f04d2fc48a0', 'owen.collier', '$2y$10$YhyBIi9kwDL3zMo4f1stXeflsNaFhfeQYdLP5n6rY7QLdYUS915u2', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(5, 'Roxane', 'Abshire', '+1 (964) 393-4747', 'rippin.dell@yahoo.com', '', NULL, '28b91bc7-f07d-361a-a846-47ba932e63e1', 'jarrell.walter', '$2y$10$YUyvaxQetBSlpvxKSkGE1e219PnIUEfJRjnJLDS/Hlx4wKpRmu.mW', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(6, 'Nat', 'Haley', '(603) 489-5689 x34335', 'melyna09@gibson.com', '', NULL, '6b54f83d-e25b-377a-8d80-94233c31000a', 'wturcotte', '$2y$10$XJ2TwwKi5KwIGaX22pv5KOPTadIUVzRAzhbdoQcMbgdsQIglM9Po6', 0, 'AC', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(7, 'Garrison', 'Will', '770-888-4715', 'abbott.freeda@hotmail.com', '', NULL, 'e453806c-1f9e-375e-b23b-8f1e5c61b339', 'xeffertz', '$2y$10$u0KF.BVbYeyHwvNO1.jWwOLp6WbugCA166Gl9tWIXx7DswlQl51fe', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(8, 'Garnet', 'Schmeler', '+1 (719) 787-1394', 'wyman.angeline@yahoo.com', '', NULL, '3bde3d99-117e-323f-8723-e21494c8a366', 'tremblay.alexane', '$2y$10$jKvKAhcw1lMm3mFFFct.g.pv.M5Hwirw7K9IhbU0unMs9K0ZC4Wrq', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(9, 'Lela', 'Russel', '1-414-322-3332 x563', 'cade35@hotmail.com', '', NULL, '097f316d-86e7-399a-833d-68e71eafd71a', 'kautzer.estelle', '$2y$10$dsJ0N0qvLgDwcmLDYz4cvu7UOpu39nc.3g/4CUxV1QFgnU0J2yVQi', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(10, 'Earline', 'Hodkiewicz', '(845) 775-4295', 'jleannon@leuschke.com', '', NULL, 'ccb39b56-fdf2-39bd-b863-b1c7c5758762', 'rau.etha', '$2y$10$2MHXNp9zcMO9zYpcthwJG.LyT0ZeR6h1V62gVCaa3yy755Q3Qd0fi', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-27 21:51:06'),
(11, 'Elena', 'Jakubowski', '1-434-692-9988', 'adrian37@mclaughlin.info', '', NULL, 'cf63f0d4-a357-3187-bdf0-6df211140a7e', 'qstoltenberg', '$2y$10$fr7E2H8Qj636O9nzAiHrluTudXFSbOudoXO7dwyuarDE49Q.2KU8a', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(12, 'Kiel', 'Weissnat', '(405) 476-3241 x9693', 'qwillms@gmail.com', '', NULL, '2654cbda-255e-3cfe-a5ce-525498fa38f4', 'ograham', '$2y$10$sIMI17C41vb62vOSVOsqDOxag3j2C7SdlfQfv/VObf4qKDW16eSMS', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(13, 'Ward', 'Block', '218-589-6844 x226', 'qdamore@hotmail.com', '', NULL, '89f4e88a-f2c7-3ebe-80eb-fe4e922825a9', 'alexandrine41', '$2y$10$nIbOk3Dk4AO11mJ6t4S4FuaTCg6cfSSviNO0/kDqzzS5JPqR.ERVK', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(14, 'Brook', 'Russel', '+1.940.927.0674', 'zboncak.paul@pagac.org', '', NULL, '63cad69b-5a7d-3fbe-a51c-5c391c9b5026', 'trevor94', '$2y$10$lbdW3IobmPD6NhsEqOxh7eH35uDOTPsPzrM7RsNFZtyQQ5wMW2tbe', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(15, 'Maybelle', 'Rodriguez', '+1-927-703-4975', 'ihills@wisozk.biz', '', NULL, 'b94f6789-b9a9-3872-b205-280a3d228be3', 'thuel', '$2y$10$jGJsq99EjZXDkmb/PbgyyO72agK9E5QcIoEz35.DXN0/6tscarcQ6', 0, 'AC', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(16, 'Adrienne', 'Waelchi', '+1.641.242.1162', 'hmorissette@jast.info', '', NULL, '3760c6a7-04e4-3421-ae70-81b59082b0a7', 'tmertz', '$2y$10$nFUqXEuC2bViLTHQWQzRUeEZ2PyxSgwoBPvlRXcoUdMJ480vBtc16', 0, 'AC', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(17, 'Genevieve', 'Skiles', '817-731-5544 x67142', 'francis31@yahoo.com', '', NULL, '657f347e-876c-3ac8-8e62-71130a2bcba9', 'ondricka.vidal', '$2y$10$g2hVp9c0d8ezKT8gowgtFOZfotbtL5p4m0mD.jpYTA3/TRuPsl8uK', 0, 'AC', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(18, 'Jordy', 'Veum', '+1-502-849-6651', 'gilda.krajcik@gmail.com', '', NULL, '68ab99b7-2895-3731-988e-77f340e3425b', 'ukihn', '$2y$10$EmtvXRqxWLx1JSdiLb1.I.DgiKUPXpoLxZX60Qyf.R37Q9XJqhZ0G', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(19, 'Urban', 'Watsica', '+1 (392) 228-6399', 'flossie62@becker.com', '', NULL, '81a24c85-711d-3899-b349-6c06744bb3be', 'haley.hallie', '$2y$10$jJvW/RPTFChM9rec2frxEeY8vp2ab7Mmrzni/dYtWueyahwfKhIMy', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(20, 'Rowland', 'Lubowitz', '(879) 424-5031', 'schuppe.heath@hotmail.com', '', NULL, 'fd8f9651-0733-3b8c-82d1-b57a031c9290', 'kcronin', '$2y$10$GDWPA7GeAHQUWR31I3pcde5UhkPdrNWUwPXwO6bTKbK288xJCjEim', 0, 'IN', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04'),
(21, 'Jerome', 'Rodriguez', '+1.862.682.9454', 'keebler.beau@yahoo.com', '', NULL, '2217ce9d-774f-3a22-a6d9-59e4e20d9690', 'trevion35', '$2y$10$YpCBP.VDMy5Ga1iaiBF2J./EPMAODJSk7ULmqgKgGYaCnZFx7atgO', 0, 'AC', NULL, '2016-10-26 18:50:04', '2016-10-26 18:50:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
