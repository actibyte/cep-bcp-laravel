<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActualizarSoftdeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('adjuntos',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('admins',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('apuntes',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('cargos',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('chats',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('equipos',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('miembros',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('preguntas',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('proyectos',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('respuestas',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('sucursales',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('tipos_sucursal',function(Blueprint $table){
            $table->softDeletes();
        });

        Schema::table('users',function(Blueprint $table){
            $table->softDeletes();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('adjuntos',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('admins',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('apuntes',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('cargos',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('chats',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('equipos',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('miembros',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('preguntas',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('proyectos',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('respuestas',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('sucursales',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('tipos_sucursal',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });

        Schema::table('users',function(Blueprint $table){
            $table->dropColumn('deleted_at');
        });
    }
}
