<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono_movil')->nullable();
            $table->string('correo_personal')->unique();
            
            $table->string('ruta_foto')->nullable();
            $table->string('id_sucursal')->nullable();
            
            $table->string('usuario');
            // $table->string('password');
            $table->boolean('clave_actualizada');
            $table->string('estado');
            
            $table->string('nivel_acceso')->default(0);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('admins');
    }
}
