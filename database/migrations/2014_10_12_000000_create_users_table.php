<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo')->unique();
            $table->string('password');
            
            $table->string('id_sucursal')->nullable();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('telefono_movil')->nullable();
            $table->string('correo_personal')->unique();
            
            $table->string('ruta_foto')->nullable();
            
            $table->boolean('clave_actualizada');
            $table->string('estado');
            
            $table->string('api_token', 60)->unique();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
