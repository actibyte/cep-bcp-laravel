<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateStringMax extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('apuntes',function(Blueprint $table){
            $table->text('comentario')->change();
        });
        Schema::table('respuestas',function(Blueprint $table){
            $table->text('comentario')->change();
        });

        Schema::table('chats',function(Blueprint $table){
            $table->text('mensaje')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('apuntes',function(Blueprint $table){
            $table->string('comentario')->change();
        });

        Schema::table('respuestas',function(Blueprint $table){
            $table->string('comentario')->change();
        });

        Schema::table('chats',function(Blueprint $table){
            $table->string('mensaje')->change();
        });
    }
}
