<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjuntosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuntos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('id_miembro')->unsigned();
            $table->integer('tipo');
            $table->string('ruta_archivo');
            $table->timestamps();

            // $table->foreign('id_respuesta')->references('id')->on('respuestas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('adjuntos', function ($table) {
        //     $table->dropForeign(['id_respuesta']); // Drops index 'geo_state_index'
        // });
        Schema::drop('adjuntos');
    }
}
