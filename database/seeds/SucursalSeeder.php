<?php

use Illuminate\Database\Seeder;
use App\Admin\Sucursal;
use App\Admin\TipoSucursal;

class SucursalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tipos_sucursal')->truncate();
        TipoSucursal::create(['nombre'=>'Tienda']);
        TipoSucursal::create(['nombre'=>'Agente']);

        
        DB::table('sucursales')->truncate();
        factory(Sucursal::class,4)->create();
    }
}
