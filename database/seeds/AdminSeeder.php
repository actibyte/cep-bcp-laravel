<?php

use Illuminate\Database\Seeder;

use App\Admin\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->truncate();
        Admin::create([
            // 'codigo'=>'MM0001',
            'nombre' => 'charly',
        	'apellido' => 'gastelo',
            'correo_personal' => 'alangb01@gmail.com',
            'telefono_movil' => '978772143',
            'usuario' => 'admin01',
            'password' => bcrypt('admin01'),
            'clave_actualizada' => 1,
            'nivel_acceso'=>1,
            // 'api_token'=>bcrypt(str_random(10)),
            'estado'=>'AC'
        ]);
    }
}
