<?php

use Illuminate\Database\Seeder;

use App\Admin\Sucursal;
use App\Admin\Proyecto;
use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\User as Usuario;

class EquipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('equipos')->truncate();
        Sucursal::all()->each(function($sucursal,$ids){
            Proyecto::all()->each(function($proyecto,$idp) use ($sucursal) {
                $e=new Equipo();
                $e->id_proyecto=$proyecto->id;
                $e->id_sucursal=$sucursal->id;
                $e->save();
            });
        });

        DB::table('miembros')->truncate();
        Equipo::all()->each(function($equipo,$id){
            factory(Miembro::class,rand(3,8))->create()->each(function($miembro,$pos) use ($equipo) {
                $miembro->id_usuario=Usuario::all()->random()->id;
                $miembro->id_equipo=$equipo->id;
                // $pregunta->numero=$pos+1;
                $miembro->save();
            });
        });

    }
}
