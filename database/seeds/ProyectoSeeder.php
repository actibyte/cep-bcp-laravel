<?php

use Illuminate\Database\Seeder;

use App\Admin\Proyecto;
use App\Admin\Pregunta;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//GENERA PROYECTOS
        DB::table('proyectos')->truncate();
        factory(Proyecto::class,5)->create();

        //GENERA LAS PREGUNTAS POR CADA PROYECTO
        DB::table('preguntas')->truncate();
        Proyecto::all()->each(function($proyecto,$id){
            factory(Pregunta::class,rand(3,8))->create()->each(function($pregunta,$pos) use ($proyecto) {
                $pregunta->id_proyecto=$proyecto->id;
                $pregunta->numero=$pos+1;
                $pregunta->save();
            });
        });


    }
}
