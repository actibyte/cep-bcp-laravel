<?php

use Illuminate\Database\Seeder;




// use App\Admin\Asignacion;

use App\Admin\Cargo;
use App\Admin\Equipo;
use App\Admin\Miembro;

use App\Admin\Respuesta;
use App\Admin\Adjunto;

use App\Admin\Apunte;
use App\Admin\Chat;

use App\Admin\Foro\Respuesta as ForoRespuesta;
use App\Admin\Foro\Tema as ForoTema;



class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsuarioSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(SucursalSeeder::class);
        $this->call(ProyectoSeeder::class);
        $this->call(EquipoSeeder::class);

        // $this->call(RespuestasTableSeeder::class);
        // $this->call(AdjuntosTableSeeder::class);
        // $this->call(ApuntesTableSeeder::class);
        // $this->call(ChatsTableSeeder::class);
    }
}


class ChatsTableSeeder extends Seeder {
    public function run(){
        DB::table('chats')->truncate();
        

        Proyecto::all()->each(function($proyecto,$id){
            $miembros=Miembro::select('miembros.*')->leftJoin('equipos','equipos.id','=','miembros.id_equipo')->where('id_proyecto',$proyecto->id);
            factory(Chat::class,rand(2,10))->create()->each(function($chat,$pos) use($proyecto,$miembros){

                $chat->id_miembro=$miembros->get()->random(rand(2,7))->random(1)->id;
                $chat->id_proyecto=$proyecto->id;
                $chat->save();
            });
        });
    }
}

class RespuestasTableSeeder extends Seeder {
    public function run(){
        DB::table('respuestas')->truncate();
        
        Equipo::all()->each(function($equipo,$id){
            $equipo->proyecto->preguntas->each(function($pregunta,$idpr) use($equipo) {
                $equipo->miembros->each(function($miembro,$idm) use($pregunta){
                    if (rand(0,1)==1) {
                        $respuesta=factory(Respuesta::class,1)->create();
                        // dd($respuesta);
                        $respuesta->id_miembro=$miembro->id;
                        $respuesta->id_pregunta=$pregunta->id;
                        $respuesta->save();
                    }
                    
                });
            });
        });
    }
}

class AdjuntosTableSeeder extends Seeder {
    public function run(){
        DB::table('adjuntos')->truncate();

        factory(Adjunto::class,20)->create();
    }
}

class ApuntesTableSeeder extends Seeder {
    public function run(){
        DB::table('apuntes')->truncate();

        factory(Apunte::class,20)->create();
    }
}
