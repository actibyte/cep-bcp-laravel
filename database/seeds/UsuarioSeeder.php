<?php

use Illuminate\Database\Seeder;

use App\Admin\User as Usuario;
use App\Admin\Cargo;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cargos')->truncate();
        factory(Cargo::class,2)->create();


        DB::table('users')->truncate();
        Usuario::create([
            'codigo'=>'MM0001',
            'password' => bcrypt('MM0001'),

            'nombre' => 'charly',
        	'apellido' => 'gastelo',
            'correo_personal' => 'alangb01@gmail.com',
            'telefono_movil' => '978772143',
            'clave_actualizada' => 1,
            'api_token'=>bcrypt(str_random(10)),
            'estado'=>'AC'
        ]);
        factory(Usuario::class,11)->create();
    }
}
