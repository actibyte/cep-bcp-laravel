<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// $factory->define(App\User::class, function (Faker\Generator $faker) {
//     return [
//         'name' => $faker->name,
//         'email' => $faker->safeEmail,
//         'password' => bcrypt(str_random(10)),
//         'remember_token' => str_random(10),
//     ];
// });

$factory->define(App\Admin\User::class, function (Faker\Generator $faker) {
    return [
        'codigo' => str_random(10),
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName,
        'ruta_foto' => '',
        'telefono_movil' => $faker->phoneNumber,
        'clave_actualizada'=>1,
        'correo_personal' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'api_token' => bcrypt(str_random(10)),
        'estado' => $faker->randomElement($array = array('AC','IN')), // 'b'
    ];
});

$factory->define(App\Admin\Sucursal::class, function (Faker\Generator $faker) {
    return [
        'id_tipo' => $faker->numberBetween($min = 1, $max = 2),
        'codigo' => str_random(10),
        'nombre' => $faker->city,
        'direccion' => $faker->address,
        'telefono' => $faker->phoneNumber,
        'ciudad' => $faker->city,
        'descripcion' => $faker->text($maxNbChars = 200),
    ];
});

$factory->define(App\Admin\Proyecto::class, function (Faker\Generator $faker) {
    return [
        'codigo' => str_random(10),
        'titulo' => $faker->firstName,
        'fecha_inicio' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'fecha_termino' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'descripcion' => $faker->text($maxNbChars = 200),
        'estado' => $faker->randomElement($array = array('AC','IN')), // 'b'
    ];
});


$factory->define(App\Admin\Chat::class, function (Faker\Generator $faker) {
    return [
        'id_miembro' => $faker->numberBetween($min = 1, $max = 20),
        'id_proyecto' => $faker->numberBetween($min = 1, $max = 20),
        'mensaje' => $faker->text($maxNbChars = 50),
    ];
});

$factory->define(App\Admin\Pregunta::class, function (Faker\Generator $faker) {
    return [
        'id_proyecto' => $faker->numberBetween($min = 1, $max = 20), // 8567
        'titulo' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'descripcion' => $faker->text($maxNbChars = 200),
        'estado' => $faker->randomElement($array = array('AC','IN')), // 'b'
        // 'telefono' => $faker->phoneNumber,
        // 'ciudad' => $faker->city,
    ];
});

$factory->define(App\Admin\Cargo::class, function (Faker\Generator $faker) {
    return [
        // 'id_proyecto' => $faker->numberBetween($min = 1, $max = 20),
        'nombre' => $faker->firstName, 
        // 'id_usuario' => $faker->numberBetween($min = 1, $max = 20), 
        // 'id_cargo' => 0, //$faker->numberBetween($min = 1, $max = 20) 
    ];
});


$factory->define(App\Admin\Equipo::class, function (Faker\Generator $faker) {
    return [
        'id_proyecto' => $faker->numberBetween($min = 1, $max = 20),
        'id_sucursal' => $faker->numberBetween($min = 1, $max = 20), 
        // 'id_usuario' => $faker->numberBetween($min = 1, $max = 20), 
        // 'id_cargo' => 0, //$faker->numberBetween($min = 1, $max = 20) 
    ];
});

$factory->define(App\Admin\Miembro::class, function (Faker\Generator $faker) {
    return [
        'id_equipo' => $faker->numberBetween($min = 1, $max = 20),
        'id_usuario' => $faker->numberBetween($min = 1, $max = 20), 
        'id_cargo' => $faker->numberBetween($min = 1, $max = 2),
        // 'id_usuario' => $faker->numberBetween($min = 1, $max = 20), 
        // 'id_cargo' => 0, //$faker->numberBetween($min = 1, $max = 20) 
    ];
});



$factory->define(App\Admin\Respuesta::class, function (Faker\Generator $faker) {
    return [
        'id_miembro' => $faker->numberBetween($min = 1, $max = 20),
        'id_pregunta' => $faker->numberBetween($min = 1, $max = 20), 
        'comentario' => $faker->text($maxNbChars = 200),
    ];
});

$factory->define(App\Admin\Foro\Respuesta::class, function (Faker\Generator $faker) {
    return [
        'id_tema'=>$faker->numberBetween($min = 1, $max = 20),
        'id_miembro' => $faker->numberBetween($min = 1, $max = 20),
        'asunto' => $faker->text($maxNbChars = 50),
        'comentario' => $faker->text($maxNbChars = 50),
    ];
});



$factory->define(App\Admin\Adjunto::class, function (Faker\Generator $faker) {
    return [
        'id_miembro' => $faker->numberBetween($min = 1, $max = 20),
        'tipo'=>$faker->numberBetween($min = 1, $max = 3), // Audio(1),Imagen(2),Video(3)
        'ruta_archivo' => $faker->text($maxNbChars = 50),
    ];
});


$factory->define(App\Admin\Apunte::class, function (Faker\Generator $faker) {
    return [
        'id_miembro' => $faker->numberBetween($min = 1, $max = 20),
        'titulo' => $faker->text($maxNbChars = 50),
        'comentario' => $faker->text($maxNbChars = 50),
        'ruta_archivo' => $faker->text($maxNbChars = 50),
    ];
});
