// $(document).on('click',"form .alerta-eliminar",function(e){
// 	e.preventDefault();
// 	if (confirm('¿Desea eliminar el item clickeado?')) {
// 		$(this).parents("form").submit();
// 	};
// })

$(document).on('click',"form [data-confirmar]",function(e){
	e.preventDefault();

	$("#myModal").find('#modal_titulo').html('Confirmación');
	$("#myModal").find('#modal_contenido').html($(this).attr("data-confirmar"));

	var form=$(this).parents("form")
	$("#myModal").modal('show').one('click', '#btn_accion', function() {
       form.submit();
    });;
})

$(document).on('ready',function(){
	$('.datepicker').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	});
	$('.datepicker_fecha_nacimiento').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD',
		viewMode: 'years',
	});

	$('.timepicker').datetimepicker({
		format: 'HH:mm:ss',
	});


	$('.datepicker-periodo').datetimepicker({
		locale: 'es',
		format: 'YYYY-MM-DD'
	});

})
