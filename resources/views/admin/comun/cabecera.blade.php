<nav class="navbar navbar-default" role="navigation">
  <!-- El logotipo y el icono que despliega el menú se agrupan
       para mostrarlos mejor en los dispositivos móviles -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse"
            data-target=".navbar-ex1-collapse">
      <span class="sr-only">Desplegar navegación</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#"><img src="{{ asset('img/logo-bcp.png') }}" alt="Banco de Crédito del Perú - BCP" class="img-responsive"></a>
  </div>
 
  <!-- Agrupar los enlaces de navegación, los formularios y cualquier
       otro elemento que se pueda ocultar al minimizar la barra -->
  @if(Auth::user()!=null)
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">
      @if(Auth::user()->tienePermiso('usuario'))
      <li class="dropdown">
        <a href="{{ route('usuario.listado') }}" class="dropdown-toggle" data-toggle="{{-- dropdown --}}">
          Usuarios {{-- <b class="caret"></b> --}}
        </a>
        <ul class="dropdown-menu">
          
          {{-- <li class="divider"></li> --}}
          <li><a href="{{ route('usuario.listado') }}">Usuarios</a></li>
        </ul>
      </li>
      @endif
       @if(Auth::user()->tienePermiso('personal'))
      <li class="dropdown">
        <a href="{{ route('sucursal.listado') }}" class="dropdown-toggle" data-toggle="{{-- dropdown --}}">
          Sucursales {{-- <b class="caret"></b> --}}
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('sucursal.listado') }}">Sucursales</a></li>
          {{-- <li class="divider"></li> --}}
          
        </ul>
      </li>
      @endif
      @if(Auth::user()->tienePermiso('proyectos'))
      <li class="dropdown">
        <a href="{{ route('proyecto.listado') }}" class="dropdown-toggle" data-toggle="{{-- dropdown --}}">
          Proyectos {{-- <b class="caret"></b> --}}
        </a>
        <ul class="dropdown-menu">
          <li><a href="{{ route('proyecto.listado') }}">Proyecto</a></li>
         {{--  <li class="divider"></li>
          <li><a href="{{ route('usuario.listado') }}">Usuarios</a></li> --}}
        </ul>
      </li>
      @endif
        @if(Auth::user()->tienePermiso('reporte'))
      <li class="dropdown">
        <a href="{{ route('reporte.proyectos') }}" class="dropdown-toggle" data-toggle="{{-- dropdown --}}">
          Reporte {{-- <b class="caret"></b> --}}
        </a>
        {{-- <ul class="dropdown-menu">
          <li><a href="{{ '' }}">Reporte</a></li>
          <li class="divider"></li>
          <li><a href="{{ route('usuario.listado') }}">Usuarios</a></li>
        </ul> --}}
      </li>
      @endif
    </ul>
 
    <!-- <form class="navbar-form navbar-left" role="search">
      <div class="form-group">
        <input type="text" class="form-control" placeholder="Buscar">
      </div>
      <button type="submit" class="btn btn-default">Enviar</button>
    </form>
      -->
    <ul class="nav navbar-nav navbar-right">
     
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          Hola, {{ Auth::user() }} <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
       
          {{-- <li class="divider"></li> --}}
          <li><a href="{{ route('admin.logout') }}"
                  onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                  Logout
              </a>

              <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                  {{ csrf_field() }}
              </form>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  @else
  
  @endif
</nav>
