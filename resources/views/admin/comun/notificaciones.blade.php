@if (session('notificacion'))
<div class="alert alert-{{ session('notificacion')['tipo'] }}">
  	@if(isset(session('notificacion')['url']))
  		<a href="{{ session('notificacion')['url'] }}" class="alert-link">{{ session('notificacion')['mensaje']}}</a>
  	@else
		<span class="">{{ session('notificacion')['mensaje']}}</span>
  	@endif
</div>
@endif
<script>
	$(".alert").fadeTo(500, 500).delay(5000).slideUp(500);
</script>