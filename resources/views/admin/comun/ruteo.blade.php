@if(isset($ruteo) && $ruteo->count()>1)
<ol class="breadcrumb">
	<?php foreach ($ruteo as $id => $seccion): ?>
	@if($id!=count($ruteo)-1)
	<li class="breadcrumb-item"><a href="{{ $seccion->url }}">{{ $seccion->nombre }}</a></li>		
	@else
	<li class="breadcrumb-item active">{{ $seccion->nombre }}</li>		
	@endif
	<?php endforeach ?>
</ol>
@endif