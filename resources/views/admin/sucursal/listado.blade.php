@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de sucursales')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar sucursales">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
				<a href="{{ route('sucursal.importar.formulario') }}" class="btn btn-success pull-right">Importar</a>
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="col-md-1 text-center">tipo {!! $campos_orden->get('tipo') !!}</th>
				<th class="col-md-1 text-center">código{!! $campos_orden->get('codigo') !!}</th>
	   			<th class="col-md-2 text-center">nombre {!! $campos_orden->get('nombre') !!}</th>
	   			<th class="col-md-1 text-center">ciudad {!! $campos_orden->get('ciudad') !!}</th>
	   			<th class="col-md-4 text-center">direccion {!! $campos_orden->get('direccion') !!}</th>
	   			<th class="col-md-2 text-center">telefono {!! $campos_orden->get('telefono') !!}</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_sucursales) && count($listado_sucursales)>0)
			@foreach($listado_sucursales as $id=>$sucursal)
				<tr class="">
					{{-- <td class="col-md-1">{{ $sucursal->id }}</td> --}}
					<td >{{ $sucursal->tipo }}</td>
					<td >{{ $sucursal->codigo }}</td>
					<td >{{ $sucursal->nombre }}</td>
					<td >{{ $sucursal->ciudad }}</td>
					<td >{{ $sucursal->direccion }}</td>
					<td >{{ $sucursal->telefono }}</td>
					<td class=" text-center">
						<form action="{{ $sucursal->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $sucursal->url_editar }}" class="btn btn-warning">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar"></span> Editar
								</a>
								<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar el usuario '{{ $sucursal }}'?">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>	


					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay sucursales registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_sucursales->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection