@extends ("admin.comun.plantilla")
@section('titulo_pagina',$sucursal->id?'Editar sucursal':'Nueva sucursal')
@section('contenido_pagina')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $sucursal->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($sucursal->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="tipo">Tipo</label>
			    	<select class="form-control" id="id_tipo" name="id_tipo">
			    		@if(isset($tipos_disponibles) && count($tipos_disponibles)>0)
						<option value="">Seleccione un tipo</option>
			    		<?php foreach ($tipos_disponibles as $id => $tipo): ?>
			    		<option value="{{ $tipo->id }}" {{ old('id_tipo',$sucursal->id_tipo)==$tipo->id?'selected':'' }}>{{ $tipo->nombre }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay tipo</option>
						@endif
					</select>
					@if($errors->has('id_tipo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('id_tipo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="codigo" class="obligatorio">Código</label>
			    	<input type="text" class="form-control " id="codigo" name="codigo" value="{{ old('codigo',$sucursal->codigo) }}" placeholder="ingrese el codigo de la sucursal">
			    	
			    	@if($errors->has('codigo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('codigo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="nombre" class="obligatorio">Nombre</label>
			    	<input type="text" class="form-control " id="nombre" name="nombre" value="{{ old('nombre',$sucursal->nombre) }}" placeholder="ingrese el nombre de la sucursal">
			    	
			    	@if($errors->has('nombre')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('nombre') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="ciudad"  class="">Ciudad</label>
			    	<input type="text" class="form-control" id="ciudad" name="ciudad" value="{{ old('ciudad',$sucursal->ciudad) }}" placeholder="ingrese la ciudad de la sucursal">
			    	
			    	@if($errors->has('ciudad')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('ciudad') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="direccion" class="obligatorio">Dirección</label>
			    	<input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion',$sucursal->direccion) }}" placeholder="ingrese la dirección de la sucursal">
			    	
			    	@if($errors->has('direccion')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('direccion') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="telefono" class="obligatorio">Telefono </label>
			    	<input type="text" class="form-control" id="telefono" name="telefono" value="{{ old('telefono',$sucursal->telefono) }}" placeholder="ingrese el teléfono de la sucursal">
			    	
			    	@if($errors->has('telefono')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('telefono') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="descripcion" class="">Descripción </label>
			    	<textarea name="descripcion" id="descripcion" rows="5" class="form-control" placeholder="ingrese la descripcion de la sucursal">{{ old('descripcion',$sucursal->descripcion) }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div>
			  	<!-- <div class="form-group">
			  				    	<label for="estado">Estado</label>
			  				    	<select class="form-control" id="estado" name="estado">
			  				    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
			  							<option value="">Seleccione un estado</option>
			  				    		<?php foreach ($estados_disponibles as $id => $estado): ?>
			  				    		<option value="{{ $estado->id }}" {{ old('estado',$sucursal->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
			  				    		<?php endforeach ?>
			  				    		@else
			  				    		<option value="">No hay estados</option>
			  							@endif
			  						</select>
			  						@if($errors->has('estado')!=null)
			  				    	<span class="help-inline alerta-campo">{{ $errors->first('estado') }}</span>
			  				    	@endif
			  	</div> -->
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection