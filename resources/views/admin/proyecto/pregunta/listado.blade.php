@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de preguntas')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-3">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		<div class="col-md-4">

	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar preguntas">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">

				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
			</div>
	  	</div>
	  </div>
	 	@include('admin.proyecto.pregunta.informacion')
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th>Número {!! $campos_orden->get('numero') !!}</th>
	   			<th>Titulo {!! $campos_orden->get('titulo') !!}</th>
	   			<th>Descripcion {!! $campos_orden->get('descripcion') !!}</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_preguntas) && count($listado_preguntas)>0)
			@foreach($listado_preguntas as $id=>$pregunta)
				<tr class="">
					{{-- <td class="col-md-1">{{ $pregunta->id }}</td> --}}
					<td class="col-md-1">{{ $pregunta->numero }}</td>
					<td class="col-md-4">{{ $pregunta->titulo }}</td>
					<td class="col-md-5">{{ $pregunta->descripcion }}</td>
					

					<td class="col-md-2 text-center">
						<form action="{{ $pregunta->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $pregunta->url_editar }}" class="btn btn-warning">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar"></span> Editar
								</a>
								<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar el proyecto '{{ $proyecto }}'?">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay preguntas registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_preguntas->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection