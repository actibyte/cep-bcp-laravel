@extends ("admin.comun.plantilla")
@section('titulo_pagina',$pregunta->id?'Editar pregunta':'Nuevo pregunta')
@section('contenido_pagina')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $pregunta->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($pregunta->id)
				{!!  method_field('PUT') !!}
	   			@endif
   				<div class="form-group col-md-12">
			    	<label for="numero" class="obligatorio">Numero de pregunta</label>
			    	<input type="text" class="form-control " id="numero" name="numero" value="{{ old('numero',$pregunta->numero) }}" placeholder="ingrese el numero del pregunta">
			    	
			    	@if($errors->has('numero')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('numero') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="titulo" class="obligatorio">Titulo</label>
			    	<input type="text" class="form-control " id="titulo" name="titulo" value="{{ old('titulo',$pregunta->titulo) }}" placeholder="ingrese el titulo del pregunta">
			    	
			    	@if($errors->has('titulo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('titulo') }}</span>
			    	@endif
			  	</div>
			  	{{-- <div class="form-group col-md-12">
			    	<label for="descripcion" class="">Descripción</label>
			    	<textarea name="descripcion" id="descripcion" class="form-control" placeholder="ingrese la descripcion de la pregunta">{{ $pregunta->descripcion }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div> --}}
			  	<!-- 
			  	<div class="form-group">
			    	<label for="estado">Estado</label>
			    	<select class="form-control" id="estado" name="estado">
			    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
						<option value="">Seleccione un estado</option>
			    		<?php foreach ($estados_disponibles as $id => $estado): ?>
			    		<option value="{{ $estado->id }}" {{ old('estado',$pregunta->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay estados</option>
						@endif
					</select>
					@if($errors->has('estado')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('estado') }}</span>
			    	@endif
			  	</div> 
			  	-->
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection