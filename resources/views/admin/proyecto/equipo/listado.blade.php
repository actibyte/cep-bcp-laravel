@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de equipos')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-3"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="post" class="">
	  				
					{{ csrf_field() }}
	  				<div class="input-group">
				    	@if(isset($sucursales_disponibles) && count($sucursales_disponibles)>0)
				    	<select class="form-control" id="id_sucursal" name="id_sucursal">
							<option value="">Seleccione una sucursal </option>
				    		<?php foreach ($sucursales_disponibles as $id => $sucursal): ?>
				    		<option value="{{ $sucursal->id }}">{{ $sucursal->nombre }}</option>
				    		<?php endforeach ?>
						</select>
						 <span class="input-group-btn">
					        <button type="submit" class="btn btn-primary">Agregar</button>
					      </span>
						@endif
				  	</div> 
	  			</form>
				
	  		</div>
			<div class="col-md-4">
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th>Tipo {!! $campos_orden->get('tipo') !!}</th>
	   			<th>Sucursal {!! $campos_orden->get('sucursal') !!}</th>
	   			<th>Miembros {!! $campos_orden->get('miembros') !!}</th>
	   			
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_equipos) && count($listado_equipos)>0)
			@foreach($listado_equipos as $id=>$equipo)
				<tr class="">
					{{-- <td class="col-md-1">{{ $equipo->id }}</td> --}}
					<td >{{ $equipo->sucursal!=null?$equipo->sucursal->tipo:'La sucursal fue eliminada' }}</td>
					<td >{{ $equipo->sucursal!=null?$equipo->sucursal:'La sucursal fue eliminada' }}</td>
					<td >
						<div class="btn-group btn-group-sm">
							<a href="{{ $equipo->url_miembros }}"  class="btn {{ $equipo->miembros->count()>0?'btn-default':'btn-danger' }}">
								{{ $equipo->miembros->count() }}
							</a>
							<a href="{{ $equipo->url_miembros }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-user" aria-hidden="true" title="Miembros"></span> Miembro{{ $equipo->miembros->count()!=1?'s':''}}
							</a>
						</div>
					</td>
					<td class="col-md-2 text-center">
						<form action="{{ $equipo->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar la sucursal '{{ $equipo->sucursal }}'?">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay equipos registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_equipos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection