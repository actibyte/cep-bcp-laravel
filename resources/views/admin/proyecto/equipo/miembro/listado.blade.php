@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de miembros')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-3"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="post" class="">
	  				{{ csrf_field() }}
	  				<div class="input-group">
				    	@if(isset($usuarios_disponibles) && count($usuarios_disponibles)>0)
				    	<select class="form-control" id="id_usuario" name="id_usuario">
							<option value="">Seleccione un miembro </option>
				    		<?php foreach ($usuarios_disponibles as $id => $usuario): ?>
				    		<option value="{{ $usuario->id }}">{{ $usuario }}</option>
				    		<?php endforeach ?>
						</select>
						 <span class="input-group-btn">
					        <button type="submit" class="btn btn-primary">Agregar</button>
					      </span>
						@endif
				  	</div> 
	  			</form>
				
	  		</div>
			<div class="col-md-4">
				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th>Apellido {!! $campos_orden->get('apellido') !!}</th>
	   			<th>Nombre {!! $campos_orden->get('nombre') !!}</th>
	   			{{-- <th>Cargo {!! $campos_orden->get('cargo') !!}</th> --}}
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_miembros) && count($listado_miembros)>0)
			@foreach($listado_miembros as $id=>$miembro)
				<tr class="">
					{{-- <td class="col-md-1">{{ $miembro->id }}</td> --}}
					<td >{{ $miembro->usuario!=null?$miembro->usuario->apellido:'El usuario fue eliminado' }}</td>
					<td >{{ $miembro->usuario!=null?$miembro->usuario->nombre:'El usuario fue eliminado' }}</td>
					{{-- <td >{{ $miembro->cargo }}</td> --}}
					<td class="col-md-2 text-center">
						<form action="{{ $miembro->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<button class="btn btn-danger" type="submit" data-confirmarx="¿Desea remover el usuario de la sucursal? ">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay miembros registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_miembros->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection