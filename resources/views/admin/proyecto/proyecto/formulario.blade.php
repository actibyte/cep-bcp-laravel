@extends ("admin.comun.plantilla")
@section('titulo_pagina',$proyecto->id?'Editar proyecto':'Nuevo proyecto')
@section('contenido_pagina')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $proyecto->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($proyecto->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="codigo" class="">Codigo</label>
			    	<input type="text" class="form-control " id="codigo" name="codigo" value="{{ old('codigo',$proyecto->codigo) }}" placeholder="ingrese el codigo del proyecto">
			    	
			    	@if($errors->has('codigo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('codigo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="titulo" class="obligatorio">Nombre</label>
			    	<input type="text" class="form-control " id="titulo" name="titulo" value="{{ old('titulo',$proyecto->titulo) }}" placeholder="ingrese el titulo del proyecto">
			    	
			    	@if($errors->has('titulo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('titulo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="fecha_inicio"  class="obligatorio">Fecha inicio</label>
			    	<input type="text" class="form-control datepicker" id="fecha_inicio" name="fecha_inicio" value="{{ old('fecha_inicio',$proyecto->fecha_inicio) }}" placeholder="ingrese la fecha de inicio del proyecto">
			    	
			    	@if($errors->has('fecha_inicio')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('fecha_inicio') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="fecha_termino"  class="obligatorio">Fecha termino</label>
			    	<input type="text" class="form-control datepicker" id="fecha_termino" name="fecha_termino" value="{{ old('fecha_termino',$proyecto->fecha_termino) }}" placeholder="ingrese la fecha de termino del proyecto">
			    	
			    	@if($errors->has('fecha_termino')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('fecha_termino') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="descripcion"  class="">Descripción</label>
			    	<textarea name="descripcion" id="descripcion"  rows="4" class="form-control">{{ $proyecto->descripcion }}</textarea>
			    	@if($errors->has('descripcion')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('descripcion') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="estado" class="obligatorio">Estado</label>
			    	<select class="form-control" id="estado" name="estado">
			    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
						{{-- <option value="">Seleccione un estado</option> --}}
			    		<?php foreach ($estados_disponibles as $id => $estado): ?>
			    		<option value="{{ $estado->id }}" {{ old('estado',$proyecto->estado)==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay estados</option>
						@endif
					</select>
					@if($errors->has('estado')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('estado') }}</span>
			    	@endif
			  	</div> 
			  	
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection