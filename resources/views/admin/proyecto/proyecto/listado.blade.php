@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de proyectos')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar proyectos">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1 text-center">Codigo {!! $campo=$campos_orden->get('codigo') !!}</th>
	   			<th class="col-md-2 text-center">Título {!! $campo=$campos_orden->get('titulo') !!}</th>
	   			<th class="col-md-1 text-center">Inicio {!! $campo=$campos_orden->get('fecha_inicio') !!}</th>
	   			<th class="col-md-1 text-center">Término {!! $campo=$campos_orden->get('fecha_termino') !!}</th>
	   			<th class="col-md-2 text-center">Preguntas {!! $campo=$campos_orden->get('preguntas') !!}</th>
	   			<th class="col-md-2 text-center">Asignados </th>
	   			<th class="col-md-1 text-center">Importación</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_proyectos) && count($listado_proyectos)>0)
			@foreach($listado_proyectos as $id=>$proyecto)
				<tr class="">
					<td class="col-md-1">{{ $proyecto->codigo }}</td>
					<td >{{ $proyecto->titulo }}</td>
					<td >{{ $proyecto->fecha_inicio->format('Y-m-d') }}</td>
					<td >{{ $proyecto->fecha_termino->format('Y-m-d') }}</td>
					<td >
						<div class="btn-group btn-group-sm">
							<a href="{{ $proyecto->url_preguntas }}"  class="btn {{ $proyecto->preguntas->count()>0?'btn-default':'btn-danger' }}">
								{{ $proyecto->preguntas->count() }}
							</a>
							<a href="{{ $proyecto->url_preguntas }}" class="btn btn-success ">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Asignar"></span> Pregunta{{ $proyecto->preguntas->count()!=1?'s':''}}
							</a>
						</div>
					</td>
					
					<td >
						<div class="btn-group btn-group-sm">
							<a href="{{ $proyecto->url_equipos }}" class="btn {{ $proyecto->equipos->count()>0?'btn-default':'btn-danger' }}">
								{{ $proyecto->equipos->count() }}
							</a>
							<a href="{{ $proyecto->url_equipos }}" class="btn btn-success ">
								<span class="glyphicon glyphicon-user" aria-hidden="true" title="Asignar"></span> Sucursal{{ $proyecto->preguntas->count()!=1?'es':''}}
							</a>
						</div>
					</td>
					<td>
						<a href="{{ $proyecto->url_importar }}" class="btn btn-warning">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Importar"></span> Importar
								</a>
					</td>
					<td class="col-md-2 text-center">
						<form action="{{ $proyecto->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $proyecto->url_editar }}" class="btn btn-warning">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar"></span> Editar
								</a>
								<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar el proyecto '{{ $proyecto }}'?">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay proyectos registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_proyectos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection