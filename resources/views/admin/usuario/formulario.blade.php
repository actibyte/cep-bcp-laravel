@extends ("admin.comun.plantilla")
@section('titulo_pagina',$usuario->id?'Editar usuario':'Nuevo usuario')
@section('contenido_pagina')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver}}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $usuario->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			@if ($usuario->id)
				{!!  method_field('PUT') !!}
	   			@endif
	   			<div class="form-group col-md-12">
			    	<label for="codigo" class="obligatorio">Codigo</label>
			    	<input type="text" class="form-control " id="codigo" name="codigo" value="{{ old('codigo',$usuario->codigo) }}" placeholder="ingrese el codigo del usuario">
			    	
			    	@if($errors->has('codigo')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('codigo') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="nombre" class="obligatorio">Nombre</label>
			    	<input type="text" class="form-control " id="nombre" name="nombre" value="{{ old('nombre',$usuario->nombre) }}" placeholder="ingrese el nombre del usuario">
			    	
			    	@if($errors->has('nombre')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('nombre') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="apellido"  class="obligatorio">Apellido</label>
			    	<input type="text" class="form-control" id="apellido" name="apellido" value="{{ old('apellido',$usuario->apellido) }}" placeholder="ingrese el apellido del usuario">
			    	
			    	@if($errors->has('apellido')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('apellido') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="correo_personal" class="obligatorio">Correo personal</label>
			    	<input type="text" class="form-control" id="correo_personal" name="correo_personal" value="{{ old('correo_personal',$usuario->correo_personal) }}" placeholder="ingrese el correo personal del usuario">
			    	
			    	@if($errors->has('correo_personal')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('correo_personal') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12">
			    	<label for="telefono_movil" class="">Telefono movil</label>
			    	<input type="text" class="form-control" id="telefono_movil" name="telefono_movil" value="{{ old('telefono_movil',$usuario->telefono_movil) }}" placeholder="ingrese el telefono movil del usuario">
			    	
			    	@if($errors->has('telefono_movil')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('telefono_movil') }}</span>
			    	@endif
			  	</div>

			  {{-- 	<div class="form-group col-md-12">
			    	<label for="usuario" class="obligatorio">Usuario</label>
			    	<input type="text" class="form-control" id="usuario" name="usuario" value="{{ old('usuario',$usuario->usuario ) }}" placeholder="ingrese el usuario del usuario">
			    	
			    	@if($errors->has('usuario')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('usuario') }}</span>
			    	@endif
			  	</div>
		  		 --}}
			  	<div class="form-group col-md-12">
			    	<label for="estado" class="obligatorio">Estado</label>
			    	<select class="form-control" id="estado" name="estado">
			    		@if(isset($estados_disponibles) && count($estados_disponibles)>0)
						<option value="">Seleccione un estado</option>
			    		<?php foreach ($estados_disponibles as $id => $estado): ?>
			    		<option value="{{ $estado->id }}" {{ old('estado',$usuario->estado) ==$estado->id?'selected':'' }}>{{ $estado->nombre }}</option>
			    		<?php endforeach ?>
			    		@else
			    		<option value="">No hay estados</option>
						@endif
					</select>
					@if($errors->has('estado')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('estado') }}</span>
			    	@endif
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection