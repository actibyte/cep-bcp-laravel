@extends ("admin.comun.plantilla")
@section('titulo_pagina','Listado de usuarios')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar usuarios">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">
				<a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a>
				<a href="{{ route('usuario.importar.formulario') }}" class="btn btn-success pull-right">Importar</a>
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="col-md-1">Código {!! $campos_orden->get('codigo') !!}</th>
	   			<th class="col-md-2">Apellido {!! $campos_orden->get('apellido') !!}</th>
	   			<th class="col-md-2">Nombre {!! $campos_orden->get('nombre') !!}</th>
	   			<th class="col-md-2">
	   				Clave por defecto
	   			</th>
	   			<th class="col-md-1">Estado</th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_usuarios) && count($listado_usuarios)>0)
			@foreach($listado_usuarios as $id=>$usuario)
				<tr class="">
					{{-- <td class="col-md-1">{{ $usuario->id }}</td> --}}
					<td >{{ $usuario->codigo }}</td>
					<td >{{ $usuario->apellido }}</td>
					<td >{{ $usuario->nombre }}</td>
					<td>
						<form action="{{ $usuario->url_resetear }}" method="post" class="">
   							{{ csrf_field() }}
							<div class="btn-group btn-group-sm">
								@if($usuario->clave_actualizada)
								{{-- <button class="btn btn-success" type="button">
								Modificada
								</button> --}}
								@else
								{{-- <button class="btn btn-warning" type="button">
								Pendiente
								</button> --}}
								@endif
								<button class="btn btn-primary " type="submit" data-confirmar="¿Desea generar una nueva la clave para el usuario {{ $usuario }}?">
									<span class="glyphicon glyphicon-refresh" aria-hidden="true" title="Generar y enviar nueva clave"></span> Generar y enviar clave	
								</button>
							</div>
						</form>	
					</td>
					<td >
						@if($usuario->estado==$usuario::ESTADO_ACTIVO)
						activo
						@elseif($usuario->estado==$usuario::ESTADO_INACTIVO)
						inactivo
						@else
						no especificado
						@endif
					</td>
					<td class="col-md-2 text-center">

						<form action="{{ $usuario->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $usuario->url_editar }}" class="btn btn-warning">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true" title="Editar"></span> Editar
								</a>
						
								<button class="btn btn-danger" type="submit" data-confirmar="¿Desea eliminar el usuario '{{ $usuario->usuario }}' de '{{ $usuario }}'?">
									<span class="glyphicon glyphicon-remove" aria-hidden="true" title="Eliminar"></span> Eliminar
								</button>
							</div>
						</form>	
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay usuarios registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_usuarios->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection