@extends ("admin.comun.plantilla")
@section('titulo_pagina',"Cambio de clave de usuario")
@section('contenido_pagina')
@include('admin.comun.notificaciones')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" class="col-md-12" action="{{ $usuario->action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
	   			{{-- @if ($usuario->id) --}}
				{!!  method_field('PUT') !!}
	   			{{-- @endif --}}
			  	
			  	<div class="form-group">
			    	<label for="apellido">Trabajador:</label>
			    	<input type="text" class="form-control" readonly value="{{ $usuario!=null ? $usuario:'no asociado' }}">
			    	
			  	</div>
			  	<div class="form-group">
  			    	<label for="password">Clave:</label>
  			    	<input type="password" class="form-control" id="password" name="password"  value="" placeholder="Ingrese su clave" autocomplete="off">
  			    	@if($errors->has('password')!=null)
  			    	<span class="help-inline">{{ $errors->first('password') }}</span>
  			    	@endif
			  </div>
			  
			  <div class="form-group">
  			    	<label for="password_confirmation">Confirmar clave:</label>
  			    	<input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  value="" placeholder="Verifique su clave " autocomplete="off">
  			    	@if($errors->has('password_confirmation')!=null)
  			    	<span class="help-inline">{{ $errors->first('password_confirmation') }}</span>
  			    	@endif
			  </div>
			  	
			  	<!-- <div class="checkbox">
			  				    	<label><input type="checkbox"> Remember me</label>
			  	</div> -->
			  	<div class="">
			  		<button type="submit" class="btn btn-success pull-right">Registrar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
    function previsualizar(input_file_id,img_preview_id) {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById(input_file_id).files[0]);

        oFReader.onload = function (oFREvent) {
        	if (!$("#"+img_preview_id).length) {
        		$("#"+input_file_id).parent().append($("<img id=\""+img_preview_id+"\" class=\"preview_foto\">"));
        	}

        	$("#"+img_preview_id).attr('src',oFREvent.target.result);	
        };
    };

    $(document).on('change','#ruta_foto',function(){
    	previsualizar("ruta_foto","preview_foto");
    })
</script>

<style>
	.preview_foto{
		margin:auto;
		width:100%;height:100%;
		max-height: 200px;
		max-width: 200px;
	}
</style>
@endsection