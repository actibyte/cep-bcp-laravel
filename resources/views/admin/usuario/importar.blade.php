@extends ("admin.comun.plantilla")
@section('titulo_pagina','Importar usuarios')
@section('contenido_pagina')
<div class="container-fluid">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<div class="row text-center">
				<div class="col-md-3"><a href="{{ $url_volver }}" class="btn btn-primary pull-left">Volver</a></div>
				<div class="col-md-6">
					<h4>@yield('titulo_pagina')</h4>
				</div>
				<div class="col-md-3"></div>
			</div>
		</div>
		<div class="panel-body">
	   		<form role="form" action="{{ $action_form }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
	   			{{ csrf_field() }}
			  	<div class="form-group col-md-12">
			    	<label for="excel">Archivo Excel</label>
			    	<input type="file" name="excel" >


					@if($errors->has('excel')!=null)
			    	<span class="help-inline alerta-campo">{{ $errors->first('excel') }}</span>
			    	@endif
			  	</div>
			  	<div class="form-group col-md-12 alert-warning">
			  		El archivo debe contener una sola hoja o una de las hojas debe llamarse "USUARIOS".<br>
			  		El archivo debe considerar los campos en la primera fila <strong>[codigo,nombre,apellido,correo]</strong>.<br>
			  		(Considerar mayusculas y minusculas).
			  	</div>
			  	<div class="from-group col-md-12">
			  		<button type="submit" class="btn btn-success pull-right">Guardar</button>
			  	</div>
			</form>
		</div>
	</div>
</div>
@endsection