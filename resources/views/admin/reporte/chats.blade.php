@extends ("admin.comun.plantilla")
@section('titulo_pagina','Chat de proyecto')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-7">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		{{-- <div class="col-md-4"> --}}

	  			{{-- <form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar respuestas">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form> --}}

	  		{{-- </div> --}}
			<div class="col-md-4">

				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	  <table class="table">
	   	
	   	<tbody>
	   		@if(isset($listado_chats) && count($listado_chats)>0)

			@foreach($listado_chats as $id=>$chat)


				<tr class="">
					 <td class="col-md-1"></td>
					<td>
						<div class="list-group">
						  <div href="#" class="list-group-item ">
						    <h4 class="list-group-item-heading">{{ $chat->asunto }}</h4>
							 @if($chat->miembro!=null)
								  <p><small>Por <b>{{ $chat->miembro->usuario }}</b> asignado a <b>{{ $chat->miembro->equipo->sucursal }}</b> a las <b>{{ $chat->created_at->format('Y-m-d h:i a') }}</b></small></p>
							  @else
								 Usuario eliminado
							 @endif


						    <p class="list-group-item-text">{{ $chat->mensaje}}</p>
						  </div>
						</div>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay mensajes registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_chats->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection