@extends ("admin.comun.plantilla")
@section('titulo_pagina','Sucursales')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-6">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		<div class="col-md-5">

	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar sucursales">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">

				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	 	
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr class="">
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="col-md-2 ">Tipo {!! $campos_orden->get('tipo') !!}</th>
	   			<th class="col-md-7 ">Sucursal {!! $campos_orden->get('sucursal') !!}</th>
	   			{{-- <th class="col-md-3 text-center">Temas (Foro) {!! $campos_orden->get('tema') !!} --}}
	   			<th class="col-md-3 text-center">Asignados</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_equipos) && count($listado_equipos)>0)
			@foreach($listado_equipos as $id=>$equipo)
				<tr class="">
					{{-- <td class="col-md-1">{{ $equipo->id }}</td> --}}
					<td>{{ $equipo->sucursal!=null?$equipo->sucursal->tipo:'La sucursal fue eliminada' }}</td>
					<td>{{ $equipo->sucursal!=null?$equipo->sucursal:'La sucursal fue eliminada' }}</td>
					{{-- <td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $equipo->url_temas }}"  class="btn {{ $equipo->temas->count()>0?'btn-default':'btn-danger' }}">
								{{ $equipo->temas->count() }}
							</a>
							<a href="{{ $equipo->url_temas }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Visualizar temas">
							</a>
						</div>
					</td> --}}
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $equipo->url_miembros }}"  class="btn {{ $equipo->miembros->count()>0?'btn-default':'btn-danger' }}">
								{{ $equipo->miembros->count() }}
							</a>
							<a href="{{ $equipo->url_miembros }}" class="btn btn-primary">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Visualizar respuestas"></span> Miembro{{ $equipo->miembros->count()!=1?'s':'' }}
							</a>
						</div>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay sucursales registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_equipos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection