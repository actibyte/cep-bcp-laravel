@extends ("admin.comun.plantilla")
@section('titulo_pagina','Adjuntos')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-6">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		<div class="col-md-5">

	  		{{-- 	<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar sucursales">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>
 --}}
	  		</div>
			<div class="col-md-4">

				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	 	
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr class="">
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="col-md-3 text-center">Ruta</th>
	   			<th class="col-md-3 text-center">Adjunto {!! $campos_orden->get('nombre') !!}</th>	   			
	   			<th class="col-md-3 text-center">Fecha hora</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_adjuntos) && count($listado_adjuntos)>0)
			@foreach($listado_adjuntos as $id=>$adjunto)
				<tr class="">
					<td class="col-md-1">{{ $adjunto->ruta_archivo }}</td>
					{{-- <td>{{ $adjunto->comentario }}</td> --}}
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $adjunto->url_archivo }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-download" aria-hidden="true" title="descargar archivo">
							</a>
						</div>
					</td>
					<td class="text-center">
						{{ $adjunto->created_at->format("Y-m-d h:i a") }}
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay adjuntos registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_adjuntos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection