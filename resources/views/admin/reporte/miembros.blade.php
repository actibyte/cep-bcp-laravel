@extends ("admin.comun.plantilla")
@section('titulo_pagina','Miembros')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-6">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		<div class="col-md-5">

	  			<form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar miembros">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">

				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	 	
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr class="">
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="col-md-2">Apellidos {!! $campos_orden->get('apellido') !!}</th>	 
	   			<th class="col-md-2">Nombres {!! $campos_orden->get('nombre') !!}</th>
	   			<th class="col-md-2 text-center">Respuestas</th>
	   			<th class="col-md-2 text-center">Adjuntos</th>	  
	   			<th class="col-md-2 text-center">Apuntes</th>	   			
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_miembros) && count($listado_miembros)>0)
			@foreach($listado_miembros as $id=>$miembro)
				<tr class="">
					{{-- <td class="col-md-1">{{ $miembro->id }}</td> --}}
					<td>{{ $miembro->usuario!=null?$miembro->usuario->apellido:'El usuario fue eliminado' }}</td>
					<td>{{ $miembro->usuario!=null?$miembro->usuario->nombre:'El usuario fue eliminado' }}</td>
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $miembro->url_respuestas }}"  class="btn btn-default">
								{{ $miembro->respuestas->count() }}
							</a>
							<a href="{{ $miembro->url_respuestas }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Visualizar respuestas">
							</a>
						</div>
					</td>
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $miembro->url_adjuntos }}"  class="btn btn-default">
								{{ $miembro->adjuntos->count() }}
							</a>
							<a href="{{ $miembro->url_adjuntos }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Visualizar adjuntos">
							</a>
						</div>
					</td>
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="{{ $miembro->url_apuntes }}"  class="btn btn-default">
								{{ $miembro->apuntes->count() }}
							</a>
							<a href="{{ $miembro->url_apuntes }}" class="btn btn-primary ">
								<span class="glyphicon glyphicon-paperclip" aria-hidden="true" title="Visualizar apuntes">
							</a>
						</div>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay miembros registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_miembros->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection