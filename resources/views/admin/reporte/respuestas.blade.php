@extends ("admin.comun.plantilla")
@section('titulo_pagina','Respuestas')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
	  		<div class="col-md-1">
	  			<a href="{{ $url_volver }}" class="btn btn-primary "> Volver</a>
	  		</div>
		  	<div class="col-md-7">
		  		<h4>
		  			
		  			@yield('titulo_pagina')
					
		  		</h4>
		  	</div>
	  		{{-- <div class="col-md-4"> --}}

	  			{{-- <form action="" method="get" class="">
	  				<div class="input-group">
				      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar respuestas">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
				      	<span class="input-group-btn">
				        	<button class="btn btn-primary" type="submit">Filtrar</button>
				     	</span>
				    </div>
	  			</form>
 --}}
	  		{{-- </div> --}}
			<div class="col-md-4">

				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	 	{{-- @include('admin.proyecto.pregunta.informacion') --}}
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			<th class="col-md-1 text-center">nro </th>
	   			<th class="col-md-3 text-center">Pregunta {!! $campos_orden->get('pregunta') !!}</th>
	   			<th class="col-md-6 text-center">Comentario {!! $campos_orden->get('fecha_limite') !!}</th>
	   			<th class="col-md-2 text-center">Fecha hora</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_respuestas) && count($listado_respuestas)>0)
	   	
			@foreach($listado_respuestas as $id=>$respuesta)
				<tr class="">
					{{-- <td>{{ $respuesta->pregunta->proyecto->id }}</td> --}}
					<td>{{ $respuesta->numero }}</td>
					<td>{{ $respuesta->pregunta }}</td>
					<td >{{ $respuesta->comentario }}</td>
					<td >{{ $respuesta->created_at->format("Y-m-d h:i:s a") }}</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="5">
						No hay respuestas registradas
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="5">
	   				{{ $listado_respuestas->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection