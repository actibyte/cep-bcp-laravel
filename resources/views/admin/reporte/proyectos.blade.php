@extends ("admin.comun.plantilla")
@section('titulo_pagina','Reportes de proyecto')
@section('contenido_pagina')
<div class="container-fluid">
	@include('admin.comun.ruteo')
	@include('admin.comun.notificaciones')
	@include('admin.comun.modal')
	<div class="panel panel-default">
	  <!-- Default panel contents -->
	  <div class="panel-heading">
	  	<div class="row ">
		  	<div class="col-md-4"><h4>@yield('titulo_pagina')</h4></div>
	  		<div class="col-md-4">
	  			<form action="" method="get" class="">
	  				<div class="input-group">
	  							      	<input type="text" class="form-control text-center" name="buscar" value="{{ app('request')->get('buscar') }}" placeholder="Filtrar reportes">
	  					<input type="hidden" name="ordenar" value="{{ app('request')->get('ordenar') }}">
	  							      	<span class="input-group-btn">
	  							        	<button class="btn btn-primary" type="submit">Filtrar</button>
	  							     	</span>
	  							    </div>
	  			</form>

	  		</div>
			<div class="col-md-4">
				{{-- <a href="{{ $url_nuevo }}" class="btn btn-success pull-right">Nuevo</a> --}}
			</div>
	  	</div>
	  </div>
	  <table class="table table-default table-condensed table-hover ">
	   	<thead>
	   		<tr>
	   			{{-- <th class="col-md-1">Id</th> --}}
	   			<th class="text-center">código {!! $campos_orden->get('codigo') !!}</th>
	   			<th class="text-center">titulo {!! $campos_orden->get('titulo') !!}</th>
	   			<th class="text-center">Inicio {!! $campos_orden->get('fecha_inicio') !!}</th>
	   			<th class="text-center">Termino {!! $campos_orden->get('fecha_termino') !!}</th>
	   			<th class="text-center">Chat </th>
	   			<th class="text-center">Equipos </th>
	   			<th class="col-md-2 text-center">Opciones</th>
	   		</tr>
	   	</thead>
	   	<tbody>
	   		@if(isset($listado_proyectos) && count($listado_proyectos)>0)
			@foreach($listado_proyectos as $id=>$proyecto)
				<tr class="">
					{{-- <td class="col-md-1">{{ $proyecto->id }}</td> --}}
					<td class="col-md-1">{{ $proyecto->codigo }}</td>
					<td class="col-md-3">{{ $proyecto->titulo }}</td>
					<td class="col-md-1">{{ $proyecto->fecha_inicio->format('d/m/Y') }}</td>
					<td class="col-md-1">{{ $proyecto->fecha_termino->format('d/m/Y') }}</td>
					<td class="col-md-2">
						<div class="btn-group btn-group-sm">
							<a href="{{ $proyecto->url_chats }}" class="btn {{ $proyecto->chats->count()>0?'btn-default':'btn-danger' }}">
								{{ $proyecto->chats->count() }}
							</a>
							<a href="{{ $proyecto->url_chats }}" class="btn btn-success ">
								<span class="glyphicon glyphicon-user" aria-hidden="true" title="Chats"></span> Mensaje{{ $proyecto->chats->count()!=1 ?'s':'' }}
							</a>
						</div>
					</td>
					<td class="col-md-2">
						<div class="btn-group btn-group-sm">
							<a href="{{ $proyecto->url_equipos }}" class="btn {{ $proyecto->equipos->count()>0?'btn-default':'btn-danger' }}">
								{{ $proyecto->equipos->count() }}
							</a>
							<a href="{{ $proyecto->url_equipos }}" class="btn btn-success ">
								<span class="glyphicon glyphicon-user" aria-hidden="true" title="Asignar"></span> Sucursal{{ $proyecto->equipos->count()!=1 ?'es':'' }}
							</a>
						</div>
					</td>
					<td class="col-md-2 text-center">
						<form action="{{ $proyecto->url_eliminar }}" method="post" class=" inline">
   							{{ csrf_field() }}
							{!! method_field('DELETE') !!}
							<div class="btn-group btn-group-sm">
								<a href="{{ $proyecto->url_exportar }}" class="btn btn-success btn-editar" target="_blank">
									<span class="glyphicon glyphicon-save" aria-hidden="true" title="Descargar"></span> Descargar
								</a>
							</div>
						</form>
					</td>
				</tr>
			@endforeach
			@else
				<tr>
					<td colspan="3">
						No hay proyectos registrados
					</td>
				</tr>
			@endif
	   	</tbody>
	   	<tfoot>
	   		<tr>
	   			<td colspan="10">
	   				{{ $listado_proyectos->links() }}
	   			</td>
	   		</tr>
	   	</tfoot>
	  </table>
	</div>
	
</div>
@endsection