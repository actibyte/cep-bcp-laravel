@extends('admin.comun.plantilla')
@section('contenido_pagina')
    <div class="container text-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 >Politica de uso y privacidad de la aplicación</h2>
                </div>
                <div class="panel-body">

                    <p>
                        La aplicación “CEP BCP” tiene como objetivo ser una herramienta digital para el registro de información de estudios etnográficos en los cuales participan trabajadores del BCP y personas bancarizadas. Para ello, es necesario contar con el registro de estos estudios etnográficos a través de fotos, videos y audios que nos sirvan de evidencia e insumo para llevar acabo workshops (talleres plenarios).
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('links')
<style>
    footer{
        position: absolute;
        bottom: 0px;
    }
</style>
@endpush