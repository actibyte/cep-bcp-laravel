<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Tu contraseña ha sido reseteada!',
    'sent' => 'Te hemos enviado un enlace para resetear tu clave!',
    'token' => 'Este codigo de reseteo de clave no es válido.',
    'user' => "No pudimos encontrar el usuario con la dirección de correo ingresado.",

];
