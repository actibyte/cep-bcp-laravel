<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sucursal extends Model
{
    //
    use SoftDeletes;
    protected $table="sucursales";

    public function __toString(){
    	return $this->nombre;
    }

    public function equipos(){
        return $this->hasMany(Equipo::class,'id_sucursal','id');
    }

    public function scopeCargarListado($query,$params=array()){
        $query->select("sucursales.*")->leftJoin('tipos_sucursal','tipos_sucursal.id','=','sucursales.id_tipo');
    	if (isset($params['buscar']) && $params['buscar']!="") {
    		$query->where(function($subquery) use($params){
    			$subquery->where('sucursales.nombre','like','%'.$params['buscar'].'%')
    				->orWhere('direccion','like','%'.$params['buscar'].'%')
    				->orWhere('telefono','like','%'.$params['buscar'].'%')
                    ->orWhere('tipos_sucursal.nombre','like','%'.$params['buscar'].'%')
    				->orWhere('ciudad','like','%'.$params['buscar'].'%');
    		});
    	}

    	if (isset($params['ordenar']) && $params['ordenar']!="") {
            
    		$parte=explode("-",$params['ordenar']);
            if (count($parte)==2) {
                $ajuste_campos=array('tipo'=>'tipos_sucursal.nombre');
                $parte[0]=str_replace(array_keys($ajuste_campos), array_values($ajuste_campos), $parte[0]);
              $query->orderBy($parte[0],$parte[1]);
            }
    	}

    	return $query;
    }

    public function scopeCargarDisponiblesParaProyecto($query,$params){
        /* SUCURSALES QUE NO ESTAN ASISNADAS AL PROYECTO*/
        $consulta="select * from equipos where id_proyecto=".$params['id_proyecto'];

        $query->select('sucursales.*');
        $query->leftjoin(\DB::raw("(".$consulta.") as equipos"),'equipos.id_sucursal','=','sucursales.id')->whereNull('equipos.id_proyecto');


        return $query;
    }

    public function tipo(){
        return $this->hasOne(TipoSucursal::class,'id','id_tipo');
    }
}
