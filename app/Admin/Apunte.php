<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apunte extends Model
{
    //
    use SoftDeletes;
    protected $table = "apuntes";
    
    public function miembro(){
    	return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function scopeCargarListado($query,$params=array()){
     	if (isset($params['id_miembro']) && $params['id_miembro']!="") {
     		$query->where('id_miembro',$params['id_miembro']);
     	}

    	return $query;
    }
}
