<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pregunta extends Model
{
    //
    use SoftDeletes;
    protected $table="preguntas";

    public function __toString(){
    	return $this->titulo;
    }

    public function proyecto(){
    	return $this->belongsTo(Proyecto::class,'id_proyecto','id');
    }

    public function respuestas(){
        return $this->hasMany(Respuesta::class,'id_pregunta','id');
    }

    public function scopeCargarListado($query,$params=array()){
        if (isset($params['id_proyecto']) && $params['id_proyecto']!="") {
            $query->where('id_proyecto',$params['id_proyecto']);
        }

    	if (isset($params['buscar']) && $params['buscar']!="") {
    		$query->where(function($subquery) use($params){
    			$subquery->where('titulo','like','%'.$params['buscar'].'%')
    				->orWhere('descripcion','like','%'.$params['buscar'].'%');
    		});
    	}

    	if (isset($params['ordenar']) && $params['ordenar']!="") {
    		$parte=explode("-",$params['ordenar']);
    		$query->orderBy($parte[0],$parte[1]);
    	}

    	return $query;
    }

    //APP - no borrar comentario
    public function scopeGetPreguntasRespondidas($query,$params=array()){
       $query_respuestas=\DB::raw('(select * from respuestas where id_miembro='.$params['id_miembro'].") as respuestas");

        $query->select('preguntas.*');
        $query->leftjoin($query_respuestas,'respuestas.id_pregunta','=','preguntas.id');

        $query->where('id_proyecto',$params['id_proyecto']);
        $query->where('respuestas.id_miembro',$params['id_miembro']);

        if (isset($params['ordenar']) && $params['ordenar']!="") {
            $parte=explode("-",$params['ordenar']);
            $query->orderBy($parte[0],$parte[1]);
        }else{
            $query->orderBy('numero');
        }

        return $query;
    }

    //APP - no borrar comentario
    public function scopeGetPreguntasPendientes($query,$params=array()){
        $query_respuestas=\DB::raw('(select * from respuestas where id_miembro='.$params['id_miembro'].") as respuestas");

        $query->select('preguntas.*');
        $query->leftjoin($query_respuestas,'respuestas.id_pregunta','=','preguntas.id');

        $query->where('id_proyecto',$params['id_proyecto']);
        $query->whereNull('respuestas.id');

        if (isset($params['ordenar']) && $params['ordenar']!="") {
            $parte=explode("-",$params['ordenar']);
            $query->orderBy($parte[0],$parte[1]);
        }

        else{
            $query->orderBy('numero');
        }


        //TEST
        /*
        SELECT preguntas.* FROM `preguntas` 
        LEFT JOIN (SELECT * FROM respuestas WHERE id_miembro=1) AS respuestas ON respuestas.id_pregunta=preguntas.id
        WHERE `id_proyecto` = 13 AND respuestas.id IS null
        */
        return $query;
    }


}
