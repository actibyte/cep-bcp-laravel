<?php 
namespace App\Admin;
/**
* 
*/

use Carbon\Carbon;
class Util
{
    public static $DIAS=array('domingo','lunes','martes','miercoles','jueves','viernes','sabado');

    public static $MESES=array('enero','febrero','marzo','abril','mayo','junio','julio',
                'agosto','setiembre','octubre','noviembre','diciembre');

	public static function camposOrdenados($campos,$ruta,$params_get,$params_url){
        $campos_orden=collect($campos);

        $separador="-";
        $orden_seteado=explode($separador,$params_get['ordenar']);
        foreach ($campos_orden as $id => $campo) {
            $orden='asc';
            if (isset($orden_seteado[0]) && isset($orden_seteado[1]) && $orden_seteado[0]==$campo) {
                $orden_asignado=$orden_seteado[1];
                if ($orden_asignado=='asc') { $orden='desc';}
                elseif($orden_asignado=="desc"){ $orden=null;}
                else{   $orden='asc'; unset($params_get['ordenar']);}
            }

            $params_get_interno=$params_get;
            $params_get_interno['ordenar']=$campo.$separador.$orden;
            $campo_actual=array('url'=>route($ruta,array_merge($params_url,$params_get_interno)),'orden'=>$orden);

            // dd($campo_actual);
            $campos_orden[$campo]=\View::make('admin.comun.campo-orden')->with('campo',$campo_actual);
            unset($campos_orden[$id]);
        }

        return $campos_orden;
    }

    public static function diaToString(Carbon $fecha){
        if ($fecha==null) {
            return "";
        }
        $dias=self::$DIAS;

        return $dias[$fecha->dayOfWeek];

    }

    public static function mesToString(Carbon $fecha){
        $meses=self::$MESES;

        return $meses[$fecha->month-1];

    }
}