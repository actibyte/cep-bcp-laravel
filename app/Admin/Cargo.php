<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cargo extends Model
{
    //
    use SoftDeletes;
    protected $table="cargos";
    
    public function __toString(){
    	return $this->nombre;
    }
}
