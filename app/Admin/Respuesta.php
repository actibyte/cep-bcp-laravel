<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Respuesta extends Model
{
    //
    use SoftDeletes;
    protected $table="respuestas";

    public function adjuntos(){
    	return $this->hasMany(Adjunto::class,'id_respuesta','id');
    }

    public function miembro(){
    	return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function pregunta(){
        return $this->belongsTo(Pregunta::class,'id_pregunta','id');
    }

    public function scopeCargarListado($query,$params=array()){
        

        if (isset($params['id_miembro']) && $params['id_miembro']!="") {
            $query->where('id_miembro',$params['id_miembro']);
        }
        // echo $query->toSql();
    	return $query;
    }

    public function scopeGetRespuestas($query,$params=array()){
        $query->where('id_pregunta',$params['id_pregunta']);
        $query->where('id_miembro',$params['id_miembro']);
        $query->orderBy('updated_at');
        return $query;
    }

   public function scopeGetMisRespuestasDeProyecto($query,$params=array()){
        $query->select("respuestas.*");
        $query->leftJoin('preguntas','preguntas.id','=','respuestas.id_pregunta');

        $query->where('id_proyecto',$params['id_proyecto']);
        $query->where('id_miembro',$params['id_miembro']);
        $query->orderBy('updated_at');
        return $query;
    }
}
