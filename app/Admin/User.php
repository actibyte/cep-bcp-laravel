<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;


use Carbon\Carbon;

use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Equipo;
use App\Admin\Miembro;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    public function routeNotificationForMail()
    {
        return $this->correo_personal;
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="users";
    
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';



    public function scopeCargarListado($query,$params=array()){

        if (isset($params['buscar']) && $params['buscar']!="") {
            $query->where(function($subquery) use($params){
                $subquery->where('nombre','like','%'.$params['buscar'].'%')
                    ->orWhere('apellido','like','%'.$params['buscar'].'%')
                    ->orWhere('codigo',$params['buscar'])
                    ->orWhere('correo_personal','like','%'.$params['buscar'].'%')
                    ->orWhere('telefono_movil','like','%'.$params['buscar'].'%')
                    ->orWhere('codigo','like','%'.$params['buscar'].'%');

            });
        }

        if (isset($params['ordenar']) && $params['ordenar']!="") {
            $parte=explode("-",$params['ordenar']);
            $query->orderBy($parte[0],$parte[1]);
        }
        // dd($query->toSql());
        return $query;
    }

    public function __toString(){
        return $this->apellido.", ".$this->nombre;
    }

   
    public function sucursal(){
        return $this->belongsTo(Sucursal::class,"id_sucursal","id");
    }

    public function tienePermiso($id_modulo){
      return true;//$this->nivel_acceso==self::NIVEL_GERENTE_GENERAL;
    }

    public function claveActualizada(){
        if ($this->clave_actualizada) {
            return "Pendiente de modificada";
        }else{
            return "contraseña modificada";
        }
    }

    public function scopeCargarDisponiblesParaEquipo($query,$params){
        /* USUARIOS QUE NO ESTAN ASISNADOS AL EQUIPO del mismo proyecto*/

        $query->select ('users.*');

        $query->leftJoin(\DB::raw("(select users.id as id_asignados FROM users
LEFT JOIN miembros ON miembros.id_usuario=users.id
LEFT JOIN equipos ON equipos.id=miembros.id_equipo
WHERE equipos.id_proyecto=".$params['id_proyecto']." group by users.id) as asignados"),"users.id","=","asignados.id_asignados");

       $query->whereNull('id_asignados');

       //echo $query->toSql();//
        $query->orderBy('apellido','asc');
        $query->orderBy('nombre','asc');
        return $query;
    }

    public function scopeCargarDisponiblesParaEquipoBack($query,$params){
        /* USUARIOS QUE NO ESTAN ASISNADOS AL EQUIPO*/
        $consulta="select * from miembros where id_equipo=".$params['id_equipo'];
      

        $query->select('users.*');
        $query->leftjoin(\DB::raw("(".$consulta.") as miembros "),'miembros.id_usuario','=','users.id');

        $query->whereNull('miembros.id_usuario');
        // $query->where('miembros.id_equipo','!=',$params['id_equipo']);
        // $query->where('miembros.id_equipo',$params['id_equipo']);

        $query->orderBy('apellido','asc');
        // dd($query->toSql());

        return $query;
    }

    //EN LA APP
    public function estaHabilitadoParaUsarAplicacion(){
        $datos_para_app=Auth::guard('web')->user()->getDatosApp();
        $errores=0;
        foreach ($datos_para_app as $id => $data) {
            if ($data===null) {
                $errores++;
            }
        }

        //dd($errores);
        if ($errores>0) {
            $this->mensajeErrorLogin="No tiene proyectos asignados o ya han finalizado";
        }

        return $errores==0;
    }

    private $mensajeErrorLogin=null;

    public function getMensajeErrorLogin(){
        return $this->mensajeErrorLogin;
    }

    public function getDatosApp(){
        $usuario=$this;
        $proyecto=$this->getProyecto();
        $equipo=$this->getEquipo($proyecto);
        $sucursal=$this->getSucursal($equipo);
        $miembro=$this->getMiembro($equipo);
        $preguntas_pendientes=$this->getPreguntasPendientes($proyecto,$miembro);
        $preguntas_respondidas=$this->getPreguntasRespondidas($proyecto,$miembro);




        $numero_preguntas_pendientes=count($preguntas_pendientes);
        $numero_preguntas_respondidas=count($preguntas_respondidas);
        return compact('usuario','proyecto','sucursal','equipo','miembro',
            'preguntas_respondidas','preguntas_pendientes',
            'numero_preguntas_pendientes','numero_preguntas_respondidas');
    }

    public function getDatosAppDescarga(){

        $usuario=$this;
        $proyecto=$this->getProyecto();
        $equipo=$this->getEquipo($proyecto);
        $sucursal=$this->getSucursal($equipo);
        $miembro=$this->getMiembro($equipo);
        $preguntas_pendientes=$this->getPreguntasPendientes($proyecto,$miembro);
        $preguntas_respondidas=$this->getPreguntasRespondidas($proyecto,$miembro);

        $preguntas=$proyecto->preguntas;
        $respuestas=Respuesta::getMisRespuestasDeProyecto(array('id_miembro'=>$miembro->id,'id_proyecto'=>$proyecto->id))->get();
        $apuntes=$miembro->apuntes;
        $adjuntos=$miembro->adjuntos;


        return compact('usuario','proyecto','sucursal','equipo','miembro',
            'preguntas_respondidas','preguntas_pendientes',
            'numero_preguntas_pendientes','numero_preguntas_respondidas',
            'preguntas','respuestas','apuntes','adjuntos');
    }

    public function getProyecto(){
        $params=array('id_usuario'=>$this->id,'fecha'=>Carbon::today()->format('Y-m-d'));
        $proyecto=Proyecto::cargarMisProyectos($params)->first();


        return $proyecto;
    }

    public function getEquipo($proyecto){
        if ($proyecto==null) {
            return null;
        }
        //ubica el equipo asociado al usuario y proyecto
        $params=array('id_usuario'=>$this->id,'id_proyecto'=>$proyecto->id);
        $equipo=Equipo::getEquipoAsignado($params)->first();
        
        return $equipo;
    }

    public function getSucursal($equipo){
        if ($equipo==null) {
            return null;
        }
        $sucursal=$equipo->sucursal;
        return $sucursal;
    }

    public function getMiembro($equipo){
        if ($equipo==null) {
            return null;
        }
        //ubica el miembro del equipo asociado al usuario y proyecto
        $params=array('id_usuario'=>$this->id,'id_equipo'=>$equipo->id);
        $miembro=Miembro::getMiembroAsignado($params)->first();
        return $miembro;
    }

    public function getPreguntasPendientes($proyecto,$miembro){
        if ($proyecto==null || $miembro==null) {
            return null;
        }

        $params=array('id_miembro'=>$miembro->id,'id_proyecto'=>$proyecto->id);
        $preguntas_pendientes=Pregunta::getPreguntasPendientes($params)->get();
        return $preguntas_pendientes;
    }

    public function getPreguntasRespondidas($proyecto,$miembro){
        if ($proyecto==null || $miembro==null) {
            return null;
        }
        $params=array('id_miembro'=>$miembro->id,'id_proyecto'=>$proyecto->id);
        $preguntas_respondidas=Pregunta::getPreguntasRespondidas($params)->get();

        return $preguntas_respondidas;
    }
}
