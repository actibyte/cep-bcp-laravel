<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoSucursal extends Model
{
    //
    use SoftDeletes;
    protected $table="tipos_sucursal";
    public $timestamps = false;

    public function __toString(){
    	return $this->nombre;
    }
}
