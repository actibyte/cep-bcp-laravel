<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adjunto extends Model
{
    //
    use SoftDeletes;
    protected $table="adjuntos";

    const TIPO_AUDIO=10;
    const TIPO_IMAGEN=20;
    const TIPO_VIDEO=30;

    public function respuesta(){
    	return $this->belongsTo(Respuesta::class,'id_respuesta','id');
    }

    public function miembro(){
        return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function scopeCargarListado($query,$params=array()){
    	if (isset($params['id_miembro']) && $params['id_miembro']!="") {
            $query->where('id_miembro',$params['id_miembro']);
        }
    	
    	return $query;
    }

    public static function getTipos(){
        $tipos_disponibles=collect();
        $tipos_disponibles->push((object)array('id'=>self::TIPO_AUDIO,'nombre'=>"Audio"));
        $tipos_disponibles->push((object)array('id'=>self::TIPO_IMAGEN,'nombre'=>"Imagen"));
        $tipos_disponibles->push((object)array('id'=>self::TIPO_VIDEO,'nombre'=>"Video"));
        return $tipos_disponibles;
    }

    public function tipoToString(){
        $tipo_actual=$this->tipo;
        $tipos_disponibles=self::getEstados();
        $indice=$tipos_disponibles->search(function ($tipo) use ($tipo_actual){
            return $tipo->id==$tipo_actual;
        });
        
        if($indice!==false){
            return $tipos_disponibles->get($indice)->nombre;
        }

        return "no especificado";
    }
}
