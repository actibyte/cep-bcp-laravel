<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Chat extends Model
{
    //
    use SoftDeletes;
    protected $table="chats";

    public function proyecto(){
    	return $this->belongsTo(Proyecto::class,'id_proyecto','id');
    }

    public function miembro(){
    	return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function __toString(){
    	return $this->mensaje;
    }

    public function scopeCargarListado($query,$params=array()){
        if (isset($params['buscar']) && $params['buscar']!="") {
            $query->where('miembro','%'.$params['buscar'].'%');
            $query->where('mensaje','%'.$params['buscar'].'%');
        }

        if (isset($params['id_miembro']) && $params['id_miembro']!="") {
            $query->where('id_miembro',$params['id_miembro']);
        }

        if (isset($params['id_proyecto']) && $params['id_proyecto']!="") {
            $query->where('id_proyecto',$params['id_proyecto']);
        }

        return $query;
    }
}
