<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Proyecto extends Model
{
    //
    use SoftDeletes;
    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';

    protected $table="proyectos";
    protected $dates=['fecha_inicio','fecha_termino'];

    public function __toString(){
    	return $this->titulo;
    }

    // public function encuestadoresPorSucursal($id_sucursal){
    // 	return $this->belongsToMany(Usuario::class,'asignaciones','id_usuario','id_proyecto')
    //         ->where('id_sucursal',$id_sucursal);
    // }

    // public function asignaciones(){
    //     return $this->hasMany(Asignacion::class,'id_proyecto','id');
    // }

    // public function sucursales(){
    //     return $this->belongsToMany(Sucursal::class,'asignaciones','id_proyecto','id_sucursal')->where('id_usuario','=',0);

    // }

    public function equipos(){
        return $this->hasMany(Equipo::class,'id_proyecto','id');

    }

    public function chats(){
        return $this->hasMany(Chat::class,'id_proyecto','id');
    }

    // public function usuarios(){
    //     return $this->belongsToMany(Usuario::class,'asignaciones','id_proyecto','id_usuario');//->where('id_usuario','=',0);

    // }

    public function preguntas(){
    	return $this->hasMany(Pregunta::class,'id_proyecto','id');
    }

    public function scopeCargarListado($query,$params=array()){
    	if (isset($params['buscar']) && $params['buscar']!="") {
    		$query->where(function($subquery) use($params){
    			$subquery->where('titulo','like','%'.$params['buscar'].'%');
    				// ->orWhere('fecha_termino','=',$params['buscar']);
    		});
    	}

    	if (isset($params['ordenar']) && $params['ordenar']!="") {
    		$parte=explode("-",$params['ordenar']);
    		$query->orderBy($parte[0],$parte[1]);
    	}

    	return $query;
    }

    public function scopeCargarReportes($query,$params=array()){
        if (isset($params['buscar']) && $params['buscar']!="") {
            $query->where(function($subquery) use($params){
                $subquery->where('titulo','like','%'.$params['buscar'].'%');
                    // ->orWhere('fecha_limite','=',$params['buscar']);
            });
        }

        if (isset($params['ordenar']) && $params['ordenar']!="") {
            $parte=explode("-",$params['ordenar']);
            $query->orderBy($parte[0],$parte[1]);
        }

        return $query;
    }


    public function scopeCargarMisProyectos($query,$params){
        //APP
        $query->select(['proyectos.*'])
            ->leftJoin('equipos','equipos.id_proyecto','=','proyectos.id')
            ->leftJoin('miembros','miembros.id_equipo','=','equipos.id')
            ->leftJoin('users','users.id','=','miembros.id_usuario');
            
        $query->where('users.id',$params['id_usuario']);

        if (isset($params['desde']) && isset($params['hasta'])) {
            //NO SETEADO RANGO DE FECHAS
            $query->where(function($subquery) use($params) {
                $subquery->whereBetween('fecha_inicio', [$params['desde'], $params['hasta']]);
                $subquery->orWhereBetween('fecha_termino', [$params['desde'], $params['hasta']]);
            });
        }

        if(isset($params['fecha'])){
            $query->where(function($subquery) use($params) {
                $subquery->whereRaw("DATE('".$params['fecha']."') between DATE(fecha_inicio) and DATE(fecha_termino)" );
            });
        }
        

        $query->distinct("proyectos.id");
        $query->orderBy('id','desc');

        return $query;
    }

    

    public function getEstadosDisponibles(){
        $estados_disponibles=collect();
        $estados_disponibles->push((object)array('id'=>self::ESTADO_ACTIVO,'nombre'=>'Activo'));
        $estados_disponibles->push((object)array('id'=>self::ESTADO_INACTIVO,'nombre'=>'Inactivo'));
        return $estados_disponibles;
    }
}
