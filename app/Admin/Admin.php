<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;


class Admin extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table="admins";
    
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    const ESTADO_ACTIVO='AC';
    const ESTADO_INACTIVO='IN';



    public function scopeCargarListado($query,$params=array()){

        if (isset($params['buscar']) && $params['buscar']!="") {
            $query->where(function($subquery) use($params){
                $subquery->where('nombre','like','%'.$params['buscar'].'%')
                    ->orWhere('apellido','like','%'.$params['buscar'].'%')
                    ->orWhere('codigo',$params['buscar'])
                    ->orWhere('correo_personal','like','%'.$params['buscar'].'%')
                    ->orWhere('telefono_movil','like','%'.$params['buscar'].'%')
                    ->orWhere('usuario','like','%'.$params['buscar'].'%');

            });
        }

        if (isset($params['ordenar']) && $params['ordenar']!="") {
            $parte=explode("-",$params['ordenar']);
            $query->orderBy($parte[0],$parte[1]);
        }
        // dd($query->toSql());
        return $query;
    }

    public function __toString(){
        return $this->apellido.", ".$this->nombre;
    }

   
    public function sucursal(){
        return $this->belongsTo(Sucursal::class,"id_sucursal","id");
    }

    public function tienePermiso($id_modulo){
      return true;//$this->nivel_acceso==self::NIVEL_GERENTE_GENERAL;
    }

    public function claveActualizada(){
        if ($this->clave_actualizada) {
            return "Pendiente de modificada";
        }else{
            return "contraseña modificada";
        }
    }
}
