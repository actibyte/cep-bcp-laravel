<?php

namespace App\Admin\Foro;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Proyecto\Miembro;
use App\Admin\Proyecto\Equipo;

class Tema extends Model
{
    //
    protected $table = "foro_temas";
    public function respuestas(){
    	return $this->hasMany(Respuesta::class,'id_tema','id');
    }

    public function miembro(){
    	return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function equipo(){
    	return $this->belongsTo(Equipo::class,'id_equipo','id');
    }

     public function scopeCargarListado($query,$params=array()){
     	if (isset($params['id_equipo']) && $params['id_equipo']!="") {
     		$query->where('id_equipo',$params['id_equipo']);
     	}

    	return $query;
    }

    public function scopeCargarTemas($query,$params=array()){
        $query->where('id_equipo',$params['id_equipo']);

        $query->orderBy('created_at','desc');

        return $query;
    }
}
