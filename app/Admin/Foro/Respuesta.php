<?php

namespace App\Admin\Foro;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Proyecto\Miembro;

class Respuesta extends Model
{
    //
    protected $table = "foro_respuestas";

    public function tema(){
    	return $this->belongsTo(Tema::class,'id_tema','id');
    }

    public function miembro(){
    	return $this->belongsTo(Miembro::class,'id_miembro','id');
    }

    public function scopeCargarListado($query,$params=array()){
    	if(isset($params['id_tema']) && $params['id_tema']!=""){
    		$query->where('id_tema',$params['id_tema']);
    	}

    	$query->orderBy('created_at','asc');

    	return $query;
    }


    public function scopeCargarRespuestas($query,$params=array()){
        $query->where('id_tema',$params['id_tema']);

        $query->orderBy('created_at','asc');
        return $query;
    }
}
