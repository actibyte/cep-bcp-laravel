<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Admin\User as Usuario;
use Illuminate\Database\Eloquent\SoftDeletes;

class Miembro extends Model
{
    //
    use SoftDeletes;
    protected $table="miembros";

    public function equipo(){
    	return $this->belongsTo(Equipo::class,'id_equipo','id');
    }

    public function usuario(){
    	return $this->belongsTo(Usuario::class,'id_usuario','id');
    }

    public function cargo(){
    	return $this->belongsTo(Cargo::class,'id_cargo','id');
    }

    public function apuntes(){
        return $this->hasMany(Apunte::class,'id_miembro','id');
    }

    public function respuestas(){
        $id_proyecto=$this->equipo->proyecto->id;
        // dd($id_proyecto);
        $query=$this->hasMany(Respuesta::class,'id_miembro','id');
            // ->leftJoin('preguntas','preguntas.id','=','respuestas.id_pregunta')
            // ->where('id_proyecto',$id_proyecto);

            // dd($query->toSql());
        return $query;
    }

    public function adjuntos(){
        $id_proyecto=$this->equipo->proyecto->id;
        // dd($id_proyecto);
        $query=$this->hasMany(Adjunto::class,'id_miembro','id');
            // ->leftJoin('adjuntos','adjuntos.id','=','respuestas.id_pregunta')
            // ->where('id_proyecto',$id_proyecto);

            // dd($query->toSql());
        return $query;
    }

    public function chat(){
        return $this->hasMany(Chat::class,'id_miembro','id');
    }

    public function scopeCargarListado($query,$params=array()){
    	$query->select('miembros.*');
    	$query->leftJoin('users','users.id','=','miembros.id_usuario');
    	$query->leftJoin('cargos','miembros.id_cargo','=','cargos.id');

    	$query->where('id_equipo',$params['id_equipo']);

    	if (isset($params['ordenar']) && $params['ordenar']!="") {
    		$parte=explode("-",$params['ordenar']);

    		if ($parte[0]=="apellido") {
    			$query->orderBy("users.apellido",$parte[1]);
    		}elseif ($parte[0]=="nombre") {
    			$query->orderBy("users.nombre",$parte[1]);
    		}elseif ($parte[0]=="cargo") {
    			$query->orderBy("cargos.nombre",$parte[1]);
    		}else{
    			$query->orderBy($parte[0],$parte[1]);
    		}
    	}

    	return $query;
    }

    public function scopeGetMiembroAsignado($query,$params){
        $query->select(['miembros.*'])
            ->leftJoin('equipos','equipos.id','=','miembros.id_equipo')
            ->leftJoin('users','users.id','=','miembros.id_usuario');

        $query->where('users.id',$params['id_usuario']);
        $query->where('equipos.id',$params['id_equipo']);
        return $query;
    }
}
