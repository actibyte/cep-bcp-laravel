<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

use App\Admin\Foro\Tema;
use Illuminate\Database\Eloquent\SoftDeletes;

class Equipo extends Model
{
    //
    use SoftDeletes;
    protected $table="equipos";

    public function miembros(){
    	return $this->hasMany(Miembro::class,'id_equipo','id');
    }

    public function sucursal(){
    	return $this->belongsto(Sucursal::class,'id_sucursal','id');
    }

    public function proyecto(){
    	return $this->belongsto(Proyecto::class,'id_proyecto','id');
    }

    // public function temas(){
    //     return $this->hasMany(Tema::class,'id_equipo','id');
    // }

    public function scopeCargarListado($query,$params=array()){
        $query->select('equipos.*');
    	$query->leftJoin('sucursales','equipos.id_sucursal','=','sucursales.id');

    	

        if (isset($params['id_proyecto']) && $params['id_proyecto']!="") {
            $query->where('id_proyecto',$params['id_proyecto']);
        }

        if (isset($params['id_sucursal']) && $params['id_sucursal']!="") {
            $query->where('id_sucursal',$params['id_sucursal']);
        }

    	if (isset($params['ordenar']) && $params['ordenar']!="") {
    		$parte=explode("-",$params['ordenar']);

    		if ($parte[0]=="sucursal") {
    			$query->orderBy("sucursales.nombre",$parte[1]);
    		}else{
    			$query->orderBy($parte[0],$parte[1]);
    		}
    	}

    	return $query;
    }

   

    public function scopeCargarMisEquipos($query,$params){
        $query->select('equipos.*')
            ->leftJoin('proyectos','equipos.id_proyecto','=','proyectos.id')
            ->leftJoin('miembros','miembros.id_equipo','=','equipos.id')
            ->leftJoin('users','users.id','=','miembros.id_usuario');

        $query->where('users.id',$params['id_usuario']);

        $query->where(function($subquery) use($params) {
            // $subquery->whereBetween('proyectos.fecha_inicio', [$params['desde'], $params['hasta']]);
            // $subquery->orWhereBetween('proyectos.fecha_termino', [$params['desde'], $params['hasta']]);

        });
            
        return $query;
    }

    public function scopeGetEquipoAsignado($query,$params){
        $query->select(['equipos.*'])
            ->leftJoin('proyectos','equipos.id_proyecto','=','proyectos.id')
            ->leftJoin('miembros','miembros.id_equipo','=','equipos.id')
            ->leftJoin('users','users.id','=','miembros.id_usuario');

        $query->where('users.id',$params['id_usuario']);
        $query->where('proyectos.id',$params['id_proyecto']);
        return $query;
    }
}
