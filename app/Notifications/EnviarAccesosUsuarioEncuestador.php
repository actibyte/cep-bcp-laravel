<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


class EnviarAccesosUsuarioEncuestador extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $nombre_usuario;
    private $password;
    private $url_descarga_android;
    private $url_descarga_ios;

    public function __construct($nombre_usuario,$password)
    {
        //
        $this->nombre_usuario=$nombre_usuario;
        $this->password=$password;
        $this->url_descarga_android=asset('app.apk');
        $this->url_descarga_ios=asset('#');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)
                    ->subject('Entrega de accesos de usuario')
                    ->greeting('Hola')
                    ->line('Notificacion de registro en el sistema')
                    ->line('Se ha creado el usuario:')
                    ->line('usuario: "'.$this->nombre_usuario.'"')
                    ->line('clave:   "'.$this->password.'"')
//                    ->line('Debes descargar la apk e instalarla en un smartphone')
                    // ->line('<a href="'.$this->url_descarga_android.'">Para android</a>'))
                    // ->line('<a href="'.$this->url_descarga_ios.'">Para IOS</a>')
//                    ->action('Para Android ', $this->url_descarga_android)
                    // ->line('clave:   "'.$this->password.'"')
                    // ->action('Para IOS: ', $this->url_descarga_ios)
                    ->line('Si presenta algun inconveniente por favor contactenos.')
                    ->line('Gracias por usar nuestra aplicación');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
