<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;
use App\Admin\Apunte;

class ApuntesController extends Controller
{
    //
    private $usuario;
    public function __construct(){
		$this->middleware('auth:api');
        $this->usuario=Auth::guard('api')->user();
	}

    public function listado(Request $request){
        $data_usuario=$this->usuario->getDatosApp();
        extract($data_usuario);

        $apuntes=$miembro->apuntes;
        $status=true;
        
        $data=compact('status','apuntes');
        return response()->json($data);
    }

    public function almacenar(Request $request){
        $data_usuario=$this->usuario->getDatosApp();
        extract($data_usuario);

        $apunte=new Apunte();
        $apunte->id_miembro=$miembro->id;
        $apunte->comentario=$request->input('comentario');

        if ($apunte->save()) {
            $apuntes=$miembro->apuntes;
            $data=array('status'=>true,'mensaje'=>'Registrado con exito','apuntes'=>$apuntes,'apunte'=>$apunte);
        }else{
            $data=array('status'=>false,'mensaje'=>'No se pudo registrar');
        }

        return response()->json($data);
    }

    /*
    public function eliminar($id_apunte){
        $apunte=Apunte::findOrFail($id_apunte);

        if ($apunte->delete()) {
            $data=array('status'=>true,'mensaje'=>'Registrado con exito');
        }else{
            $data=array('status'=>false,'mensaje'=>'No se pudo registrar');
        }

        return response()->json($data);
    }*/

}

