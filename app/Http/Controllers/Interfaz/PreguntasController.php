<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Admin\Miembro;
use App\Admin\Equipo;
use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Respuesta;
use App\Admin\Sucursal;
use App\Admin\Util\Util;

use Carbon\Carbon;

class PreguntasController extends Controller
{
    //
    private $usuario;
	public function __construct(){
		$this->middleware('auth:api');

		$this->usuario=Auth::guard('api')->user();
	}

	public function listado(){
		$data_usuario=$this->usuario->getDatosApp();
		extract($data_usuario);
		// dd();


		$status=true;
		$data=compact('status','preguntas_pendientes','preguntas_respondidas');
		// dd($data);
        return response()->json($data);
	}

	public function detalle(Request $request){

		$id_pregunta=$request->get('id_pregunta');
		$datos_usuario=$this->usuario->getDatosApp();
		$id_miembro=$datos_usuario['miembro']->id;
		

		$pregunta=Pregunta::findOrFail($id_pregunta);
		$respuesta=Respuesta::getRespuestas(array('id_pregunta'=>$pregunta->id,'id_miembro'=>$id_miembro))->first();
		
		$status=true;
		$data=compact('status','pregunta','respuesta');

        return response()->json($data);
	}

	public function guardarRespuesta(Request $request){
		$datos_usuario=$this->usuario->getDatosApp();

		$respuesta=new Respuesta();
		$respuesta->id_pregunta=$request->get('id_pregunta');
		$respuesta->id_miembro=$datos_usuario['miembro']->id;
		$respuesta->comentario=$request->get('comentario');

		$status=false;
		if ($respuesta->save()) {
			$status=true;
		}

		$data=compact('status','respuesta');

        return response()->json($data);
	}
}
