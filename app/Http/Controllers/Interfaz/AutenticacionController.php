<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Session;

class AutenticacionController extends Controller
{
    //

    public function guard(){

        return Auth::guard('web');
    }

    public function login(Request $request){
        //sleep(10);

    	if ($this->guard()->attempt([
                    'codigo' => $request->input('codigo'), 
                    'password' => $request->input('password')
                    ])) {
            // Authentication passed.
            if(!$this->guard()->user()->estaHabilitadoParaUsarAplicacion()){
                $mensaje=$this->guard()->user()->getMensajeErrorLogin();
                $data=array('status'=>false,'mensaje'=>$mensaje);
            }else{
                $data=array('status'=>true,'api_token'=>$this->guard()->user()->api_token);    
            }
        }else{
        	$data=array('status'=>false,
            	'mensaje'=>'El usuario/clave son incorrectos');
        }

        return response()->json($data);
    }

    
}
