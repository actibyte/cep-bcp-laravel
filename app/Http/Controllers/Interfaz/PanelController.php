<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Equipo;
use App\Admin\Miembro;

class PanelController extends Controller
{
    private $usuario;

    public function __construct(){
		$this->middleware('auth:api');
		$this->usuario=Auth::guard('api')->user();
	}

	public function cargarInicio(){
		//ubica los proyectos dentro del rango de fechas
		$datos_usuario=$this->usuario->getDatosApp();

		$status=true;
		$data=compact('status');
		$data=array_merge($data,$datos_usuario);

        return response()->json($data);
	}

	public function cargarInformacion(){
		$datos_usuario=$this->usuario->getDatosApp();

		$proyecto=$datos_usuario['proyecto'];
		$proyecto->periodo=$proyecto->fecha_inicio->format("d-m-Y")." al ".$proyecto->fecha_termino->format("d-m-Y");
		
		$sucursal=$datos_usuario['sucursal'];

		$status=true;
		$data=compact('status','proyecto','sucursal');

        return response()->json($data);
	}
}
