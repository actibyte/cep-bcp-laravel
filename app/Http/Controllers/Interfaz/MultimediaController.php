<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Admin\Adjunto;



class MultimediaController extends Controller
{
    //


    private $usuario;
    private $disco;
    
    public function __construct(){
		$this->middleware('auth:api');

		$this->usuario=Auth::guard('api')->user();
		$this->disco=\Storage::disk('local');
	}

	public function listado(Request $request){
		extract($this->usuario->getDatosApp());

		$status=true;

		$adjuntos=$miembro->adjuntos;

		$imagenes= $this->getFiltroListado($adjuntos,Adjunto::TIPO_IMAGEN)." imagenes";

		$videos=$this->getFiltroListado($adjuntos,Adjunto::TIPO_VIDEO)." videos";

		$audios=$this->getFiltroListado($adjuntos,Adjunto::TIPO_AUDIO)." audios";


		$data=compact('status','imagenes','videos','audios');

		return response()->json($data);
	}

	private  function getFiltroListado($adjuntos,$tipo){
		return $adjuntos->filter(function($adjunto,$ida) use ($tipo) {
			//echo $tipo." -- ".$adjunto->tipo."<br>";
			return $adjunto->tipo==$tipo;
		})->count();
	}

	private function getRuta($tipo){
		extract($this->usuario->getDatosApp());

		$ruta="proyecto-".$proyecto->id."/multimedia/";
		$ruta.="sucursal-".$sucursal->codigo."-";
		$ruta.=$usuario->codigo."-".$usuario->apellido."-".$usuario->nombre."-";

		switch($tipo){
			case Adjunto::TIPO_AUDIO:
				$ruta.="audio-";
				$extension=".amr";
				break;
			case Adjunto::TIPO_VIDEO:
				$ruta.="video-";
				$extension=".mp4";
				break;
			case Adjunto::TIPO_IMAGEN:
				$ruta.="imagen-";
				$extension=".jpg";
				break;
		}

		$filtro=$miembro->adjuntos->filter(function($adjunto,$ida) use ($tipo){
			//echo $adjunto->tipo."=".$tipo."<br>";
			return $adjunto->tipo==$tipo;
		});

		//dd($filtro);

		$ruta.=$filtro->count().$extension;

		return $ruta;

	}

	public function subirAudio(Request $request){
		
		extract($this->usuario->getDatosApp());


		$archivo=$request->file('archivo');
		//dd($archivo);
		if (isset($archivo) && $archivo->isValid()) {
	 
	       	//obtenemos el nombre del archivo
	       	$nombre =  $archivo->getClientOriginalName();
	       	$nombre=$this->getRuta(Adjunto::TIPO_AUDIO);
	 
	       	//indicamos que queremos guardar un nuevo archivo en el disco local
	       	$this->disco->put($nombre,  \File::get($archivo));


			$adjunto=new Adjunto();
			$adjunto->id_miembro=$miembro->id;
			$adjunto->tipo=Adjunto::TIPO_AUDIO;
			$adjunto->ruta_archivo=$nombre;


	       	if ($adjunto->save()) {
	       		$status=true;
	       		$mensaje="guardado con exito";

	       		$adjuntos=$miembro->adjuntos;

				$imagenes= $this->getFiltroListado($adjuntos,Adjunto::TIPO_IMAGEN)." imagenes";

				$videos=$this->getFiltroListado($adjuntos,Adjunto::TIPO_VIDEO)." videos";

				$audios=$this->getFiltroListado($adjuntos,Adjunto::TIPO_AUDIO)." audios";


	       		$data=compact('status','mensaje','imagenes','audios','videos');
	       		
				return response()->json($data);	       	
	       	}
		}
		$status=false;
		$mensaje="No se pudo procesar";
		$data=compact('status','mensaje','archivo');
		return response()->json($data);
	}

	public function subirImagen(Request $request){
		extract($this->usuario->getDatosApp());

		

		$archivo=$request->file('archivo');
		//dd($archivo);
		if (isset($archivo) && $archivo->isValid()) {
	 
	       	//obtenemos el nombre del archivo
	       	//$nombre = $archivo->getClientOriginalName();
	       	$nombre=$this->getRuta(Adjunto::TIPO_IMAGEN);
	 
	       	//indicamos que queremos guardar un nuevo archivo en el disco local
	       	\Storage::disk('local')->put($nombre,  \File::get($archivo));
			$adjunto=new Adjunto();
			$adjunto->id_miembro=$miembro->id;
			$adjunto->tipo=Adjunto::TIPO_IMAGEN;
			$adjunto->ruta_archivo=$nombre;


	       	if ($adjunto->save()) {
	       		$status=true;
	       		$mensaje="guardado con exito";

	       		$adjuntos=$miembro->adjuntos;

				$imagenes= $this->getFiltroListado($adjuntos,Adjunto::TIPO_IMAGEN)." imagenes";

				$videos=$this->getFiltroListado($adjuntos,Adjunto::TIPO_VIDEO)." videos";

				$audios=$this->getFiltroListado($adjuntos,Adjunto::TIPO_AUDIO)." audios";


	       		$data=compact('status','mensaje','imagenes','audios','videos');
				return response()->json($data);	       	
	       	}
		}
		$status=false;
		$mensaje="No se pudo procesar";
		$data=compact('status','mensaje','archivo');
		return response()->json($data);
	}

	public function subirVideo(Request $request){
		
		extract($this->usuario->getDatosApp());

		

		$archivo=$request->file('archivo');
		//dd($archivo);
		if (isset($archivo) && $archivo->isValid()) {
	 
	       	//obtenemos el nombre del archivo
	       	//$nombre = $archivo->getClientOriginalName();
	       	$nombre=$this->getRuta(Adjunto::TIPO_VIDEO);
	 
	       	//indicamos que queremos guardar un nuevo archivo en el disco local
	       	\Storage::disk('local')->put($nombre,  \File::get($archivo));


			$adjunto=new Adjunto();
			$adjunto->id_miembro=$miembro->id;
			$adjunto->tipo=Adjunto::TIPO_VIDEO;
			$adjunto->ruta_archivo=$nombre;


	       	if ($adjunto->save()) {
	       		$status=true;
	       		$mensaje="guardado con exito";

	       		$adjuntos=$miembro->adjuntos;

				$imagenes= $this->getFiltroListado($adjuntos,Adjunto::TIPO_IMAGEN)." imagenes";

				$videos=$this->getFiltroListado($adjuntos,Adjunto::TIPO_VIDEO)." videos";

				$audios=$this->getFiltroListado($adjuntos,Adjunto::TIPO_AUDIO)." audios";


	       		$data=compact('status','mensaje','imagenes','audios','videos');

				return response()->json($data);	       	
	       	}
		}
		$status=false;
		$mensaje="No se pudo procesar";
		$data=compact('status','mensaje','archivo');
		return response()->json($data);
	}
}
