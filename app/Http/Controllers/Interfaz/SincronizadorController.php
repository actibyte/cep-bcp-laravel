<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\Sucursal;

use App\Admin\Respuesta;
use App\Admin\Adjunto;
use App\Admin\Apunte;

class SincronizadorController extends Controller
{
    //
    private $usuario;
    private $disco;

    public function __construct(){
		$this->middleware('auth:api');
		$this->usuario=Auth::guard('api')->user();

		$this->disco=\Storage::disk('local');
	}

	public function descargar(){
		//ubica los proyectos dentro del rango de fechas
		$datos_usuario=$this->usuario->getDatosAppDescarga();
		
		$status=true;
		$data=compact('status');
		$data=array_merge($data,$datos_usuario);

        return response()->json($data);
	}

	/**
	*  	@params file['archivo']
	*  	@params input['id_miembro']
	*  	@params input['tipo']
	*  	@params input['created_at']
	*  	@params input['updated_at']
	* 	return json['status','mensaje','respuesta'] // apunte insertada para manipular id de registro.
	*/
	public function subirAdjunto(Request $request){
		$status=false;

		$reglas=[
				'id_miembro'=>'required',
				'tipo'=>'required',
				'archivo' => 'required',
				'created_at'=>'required',
				'updated_at'=>'required'
		];

		$validator = \Validator::make($request->all(),$reglas);

        if ($validator->fails()) {
        	$mensaje="compruebe los datos ingresados";

            return response()->json(compact('status','mensaje'));
        }
		
		$mensaje="No se pudo procesar";


		$archivo=$request->file('archivo');
		// dd($archivo);
		if (isset($archivo) && $archivo->isValid()) {
	 
	       	//obtenemos el nombre del archivo
	       	$nombre =  $archivo->getClientOriginalName();
	       	$nombre=$this->getRutaMultimedia($request->input("id_miembro"),$request->input("tipo"));
	 		
	 		//dd($nombre);
	       	//indicamos que queremos guardar un nuevo archivo en el disco local
	       	$this->disco->put($nombre,  \File::get($archivo));


			$adjunto=new Adjunto();
			$adjunto->id_miembro=$request->input('id_miembro');
			$adjunto->tipo=$request->input('tipo');
			$adjunto->ruta_archivo=$nombre;
			$adjunto->created_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('created_at'));
			$adjunto->updated_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('updated_at'));

			
	       	if ($adjunto->save()) {
	       		$status=true;
	       		$mensaje="guardado con exito";
	       	}
		}
		
		$data=compact('status','mensaje','adjunto');
		return response()->json($data);

	}

	/**
	*  	@params input['id_miembro']
	*  	@params input['comentario']
	*  	@params input['created_at']
	*  	@params input['updated_at']
	* 	return json['status','mensaje','apunte'] // apunte insertada para manipular id de registro.
	*/
	public function subirApunte(Request $request){
		$status=false;

		$reglas=[
				'id_miembro'=>'required',
				'comentario'=>'required',
				'created_at'=>'required',
				'updated_at'=>'required'
		];

		$validator = \Validator::make($request->all(),$reglas);

        if ($validator->fails()) {
        	$mensaje="compruebe los datos ingresados";

            return response()->json(compact('status','mensaje'));
        }

		$mensaje="No se pudo registrar";

		$apunte=new Apunte();
		$apunte->id_miembro=$request->input('id_miembro');
		$apunte->comentario=$request->input('comentario');
		$apunte->created_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('created_at'));
		$apunte->updated_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('updated_at'));

		if ($apunte->save()) {
			$status=true;
			$mensaje="registrado con exito";
		}

		return response()->json(compact('status','mensaje','apunte'));
	}

	/**
	*  	@params input['id']
	*  	@params input['comentario']
	*  	@params input['updated_at']
	* 	return json['status','mensaje','apunte'] // apunte(edicion) insertada para manipular id de registro.
	*/
	public function subirApunteEdicion(Request $request){
		$status=false;

		$reglas=[
				'id'=>'required',
				'comentario'=>'required',
				'updated_at'=>'required'
		];

		$validator = \Validator::make($request->all(),$reglas);

        if ($validator->fails()) {
        	$mensaje="compruebe los datos ingresados";

            return response()->json(compact('status','mensaje'));
        }

		$mensaje="No se pudo editar";

		$apunte=Apunte::findOrFail($request->input('id'));
		$apunte->comentario=$request->input('comentario');
		$apunte->updated_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('updated_at'));

		if ($apunte->save()) {
			$status=true;
			$mensaje="editado con exito";
		}

		return response()->json(compact('status','mensaje','apunte'));
	}

	/**
	*  	@params input['id_miembro']
	*  	@params input['id_pregunta']
	*  	@params input['comentario']
	*  	@params input['created_at']
	*  	@params input['updated_at']
	* 	return json['status','mensaje','respuesta'] // respuesta insertada para manipular id de registro.
	*/
	public function subirRespuesta(Request $request){

		$status=false;
		$reglas=[
				'id_miembro'=>'required',
				'id_pregunta'=>'required',
				'comentario'=>'required',
				'created_at'=>'required',
				'updated_at'=>'required'
		];

		$validator = \Validator::make($request->all(),$reglas);

        if ($validator->fails()) {
        	$mensaje="compruebe los datos ingresados";

            return response()->json(compact('status','mensaje'));
        }


		$mensaje="No se pudo registrar";

		$respuesta=new Respuesta();
		$respuesta->id_miembro=$request->input('id_miembro');
		$respuesta->id_pregunta=$request->input('id_pregunta');
		$respuesta->comentario=$request->input('comentario');
		$respuesta->created_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('created_at'));
		$respuesta->updated_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('updated_at'));

		if ($respuesta->save()) {
			$status=true;
			$mensaje="registrado con exito";
		}

		return response()->json(compact('status','mensaje','respuesta'));
	}

	/**
	*  	@params input['id']
	*  	@params input['comentario']
	*  	@params input['updated_at']
	* 	return json['status','mensaje','respuesta'] // respuesta editada para manipular id de registro.
	*/
	public function subirRespuestaEdicion(Request $request){
		$status=false;
			
		//COMPROBACION DE CODIGO
		$reglas=[
				'id'=>'required',
				'comentario'=>'required',
				'updated_at'=>'required'
		];

		$validator = \Validator::make($request->all(),$reglas);

        if ($validator->fails()) {
        	$mensaje="compruebe los datos ingresados";

            return response()->json(compact('status','mensaje'));
        }

        $mensaje="No se pudo editar";

		$respuesta=Respuesta::findOrFail($request->input('id'));
		$respuesta->comentario=$request->input('comentario');
		$respuesta->updated_at=Carbon::createFromFormat("Y-m-d H:i:s",$request->input('updated_at'));

		if ($respuesta->save()) {
			$status=true;
			$mensaje="editado con exito";
		}

		return response()->json(compact('status','mensaje','respuesta'));
	}


	private function getRutaMultimedia($id_miembro,$tipo){

		$miembro=Miembro::findOrFail($id_miembro);

		$proyecto=$miembro->equipo->proyecto;
		$sucursal=$miembro->equipo->sucursal;
		$usuario=$miembro->usuario;


		$ruta="proyecto-".$proyecto->id."/multimedia/";
		$ruta.="sucursal-".$sucursal->codigo."-";
		$ruta.=$usuario->codigo."-".$usuario->apellido."-".$usuario->nombre."-";

		switch($tipo){
			case Adjunto::TIPO_AUDIO:
				$ruta.="audio-";
				$extension=".mp3";
				break;
			case Adjunto::TIPO_VIDEO:
				$ruta.="video-";
				$extension=".mp4";
				break;
			case Adjunto::TIPO_IMAGEN:
				$ruta.="imagen-";
				$extension=".jpg";
				break;
		}

		$filtro=$miembro->adjuntos->filter(function($adjunto,$ida) use ($tipo){
			//echo $adjunto->tipo."=".$tipo."<br>";
			return $adjunto->tipo==$tipo;
		});

		//dd($filtro);

		$ruta.=$filtro->count().$extension;

		return $ruta;

	}
}
