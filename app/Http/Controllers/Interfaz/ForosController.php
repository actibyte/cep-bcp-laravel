<?php

namespace App\Http\Controllers\Interfaz;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use App\Admin\Chat;

use App\Admin\Util;
use Carbon\Carbon;

class ForosController extends Controller
{
    //
    private $usuario;
	public function __construct(){
		$this->middleware('auth:api');

		$this->usuario=Auth::guard('api')->user();
	}

	public function listado(){
		$datos_usuario=$this->usuario->getDatosApp();
		extract($datos_usuario);

		$mensajes=$this->getMensajes($proyecto);

		$status=true;
		$data=compact('status','mensajes');

        return response()->json($data);
	}

	private function formatearMensaje($mensaje){

	    //
        if($mensaje->miembro!=null){
            $usuario=(String)$mensaje->miembro->usuario;
        }else{
            $usuario='usuario removido o eliminado';
        }

	    if($mensaje->miembro!=null
            && $mensaje->miembro->equipo!=null
            && $mensaje->miembro->equipo->sucursal!=null){
            $sucursal=(String) $mensaje->miembro->equipo->sucursal;
        }else{
	        $sucursal="desconocida";
        }


		return array(
					'mensaje'=>$mensaje->mensaje,
					'anio'=>$mensaje->created_at->format("Y"),
					'mes'=>substr(Util::mesToString($mensaje->created_at), 0,3),
					'dia'=>$mensaje->created_at->format("d"),
					'hora'=>$mensaje->created_at->format("h:i a"),
					'usuario'=>$usuario,
					'sucursal'=>$sucursal,
				);
	}

	private function getMensajes($proyecto){
		$listado_mensajes=$proyecto->chats;
		$mensajes=collect();
		foreach ($listado_mensajes as $id => $mensaje) {
			# code...
			$item=$this->formatearMensaje($mensaje);

			$mensajes->push($item);
		}
		return $mensajes;
	}

	public function almacenar(Request $request){
		$datos_usuario=$this->usuario->getDatosApp();
		extract($datos_usuario);

		$mensaje=new Chat();
		$mensaje->id_proyecto=$proyecto->id;
		$mensaje->id_miembro=$miembro->id;
		$mensaje->mensaje=$request->input("mensaje");
		$mensaje->estado='AC';
		// dd($mensaje);
		if ($mensaje->save()) {
			$status=true;
			$mensajes=$this->getMensajes($proyecto);
			$data=compact('status','Guardado con exito','mensaje','mensajes');

			$this->notificarDispositivos($proyecto,$miembro);
			$this->notificarDispositivosIos($proyecto,$miembro);

		}else{
			$status=true;
			$data=compact('status','No se pudo guardar');
		}

        return response()->json($data);
	}

	public function notificarDispositivos($proyecto,$miembro){
		$url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array (
                'Authorization: key=' . "AAAA3f_Mtzw:APA91bHLrXjp40p6xfmn6hODDR1_la-WQXlBLc0KzupmQb5paHPJmY5Zc58T_QncThGIYDTr3E-y5NpIu-pCiuwhBToqlWBJESNne89L4togb61I9DSlFtMfdbhHQb3jlBETV_70rdvn6kWqsT0_IUn0R23veznY-Q",
                'Content-Type: application/json'
        );


        $fields = array(
                /*'notification' =>array(
	                    'title'=>'CEP BCP',
	                    'text'=>'Nuevo mensaje'
	                ),*/
                'to'=>'/topics/proyecto-'.$proyecto->id,
                "data"=>array(
	                	"accion"=>"chat-actualizar",
	                	"id_miembro"=>$miembro->id
	                )
                //'to'=>'crZVvX8Mk5I:APA91bHse1mnmLfxVTTPEXDduAx7OzEamuU7ZfXPtCVaj5cTE51aq5rcdkTPPn26wxL9M0HGhO7GNA4s4CKFDMbpoL7b-K-nqe4nDvP68UE_niCrqG9J4syWN3j6QSSRURzPqy-kraeg'
        );
        $fields = json_encode ( $fields );


        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = curl_exec ( $ch );
        //echo $result;
        if(curl_errno($ch))
            //echo 'Curl error: '.curl_error($ch);
        curl_close ($ch);  
	}
	public function notificarDispositivosIos($proyecto,$miembro){
		$url = 'https://fcm.googleapis.com/fcm/send';
        $headers = array (
                'Authorization: key=' . "AAAA3f_Mtzw:APA91bHLrXjp40p6xfmn6hODDR1_la-WQXlBLc0KzupmQb5paHPJmY5Zc58T_QncThGIYDTr3E-y5NpIu-pCiuwhBToqlWBJESNne89L4togb61I9DSlFtMfdbhHQb3jlBETV_70rdvn6kWqsT0_IUn0R23veznY-Q",
                'Content-Type: application/json'
        );

        $fields = array(
                'notification' =>array(
	                    'title'=>'CEP BCP',
	                    'text'=>'Nuevo mensaje'
	                ),
                'to'=>'/topics/ios-proyecto-'.$proyecto->id,
                "data"=>array(
	                	"accion"=>"chat-actualizar",
	                	"id_miembro"=>$miembro->id
	                )
                //'to'=>'crZVvX8Mk5I:APA91bHse1mnmLfxVTTPEXDduAx7OzEamuU7ZfXPtCVaj5cTE51aq5rcdkTPPn26wxL9M0HGhO7GNA4s4CKFDMbpoL7b-K-nqe4nDvP68UE_niCrqG9J4syWN3j6QSSRURzPqy-kraeg'
        );
        $fields = json_encode ( $fields );

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );

        $result = curl_exec ( $ch );
       // echo $result;

        if(curl_errno($ch))
          //  echo 'Curl error: '.curl_error($ch);

        curl_close ($ch);  
	}
}
