<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Chumper\Zipper\Zipper;

use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Respuesta;
use App\Admin\Apunte;
use App\Admin\Adjunto;
use App\Admin\Chat;

use App\Admin\Foro\Tema;
use App\Admin\Foro\Respuesta as Comentario;

use App\Admin\Util;

class ExportarReporteController extends Controller
{
    //
    private $proyecto;
    private $comprimido;
    private $almacenamiento;
    private $nombre_comprimido;
    private $dir_temporal;

	public function __construct(){
        $this->middleware('auth:admin');
    }

	private function inicializar($id_proyecto){
		$this->proyecto=Proyecto::findOrFail($id_proyecto);

		$this->preguntas=$this->proyecto->preguntas;

		// $this->colaboradores=collect();
		// $this->proyecto->equipos->each(function($equipo){
			//cuidado al unir o fusionar listado de colaboradores
			//$this->colaboradores=$this->colaboradores->union($equipo);
		// });




		//inicializador de disco
		$this->almacenamiento=\Storage::disk('local');
		//inicializador de comprimido
		$this->nombre_comprimido=storage_path("app")."/exportar/proyecto-".time().".zip";
    	$this->comprimido = new Zipper();
    	$this->comprimido->make($this->nombre_comprimido);
    	
    	$this->dir_temporal="proyecto-".$this->proyecto->id;
    	$this->almacenamiento->makeDirectory($this->dir_temporal);

	}

    public function exportar(Request $request,$id_proyecto){
    	//inicializador de zip
    	$this->inicializar($id_proyecto);

    	$ruta_excel=$this->crearExcel();

    	$this->exportarMultimedia();

    	$this->comprimirCarpetaTemporal($ruta_excel);
    	
    	return response()->download($this->nombre_comprimido);
    }

    private function comprimirCarpetaTemporal($ruta_excel){
    	$this->comprimido->add(storage_path("app")."/".$this->dir_temporal);
    	$this->comprimido->close();

       // dd($ruta_excel.".xls");

        $excel_temp=$this->dir_temporal."/".$this->archivo_excel.".xls";
        
        $this->almacenamiento->delete($excel_temp);
    	//$this->almacenamiento->deleteDirectory($this->dir_temporal);
    }

    private $archivo_excel;

    public function crearExcel(){
    	// echo $this->dir_temporal;
    	$this->archivo_excel="reporte_proyecto-".$this->proyecto->id."-".time();
		$dir_excel=storage_path("app")."/".$this->dir_temporal;



    	$object=\Excel::create($this->archivo_excel, function($excel)  {
  			//HOJA RESUMEN
			$excel->sheet('Resumen', function($sheet)  {
				$this->cargarResumen($sheet);
			});

			//HOJA DE RESPUESTAS
            $excel->sheet('Respuestas', function($sheet) {
            	$this->cargarRespuestas($sheet);
            });

            //HOJA DE APUNTES
            $excel->sheet('Apuntes', function($sheet) {
            	$this->cargarApuntes($sheet);
            });

			//HOJA DE CHATS
          	$excel->sheet('Chat', function($sheet) {
          		$this->cargarChats($sheet);
          	});
        })->store('xls',$dir_excel);
		

		return $dir_excel."/".$this->archivo_excel;
    }

    private function obtenerIndiceCelda($indice_requerido){

    	//bordes de data
    	$celdas=array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z");
    	$maximo=100;
    	$columnas=array();
    	$prefijo="";
    	for ($i=0; $i < $maximo; $i++) { 
    		$veces=floor($i/count($celdas));
    		if ($veces>0 && isset($celdas[$veces])) {
    			$prefijo=$celdas[$veces-1];
    		}

    		$indice=$i-(count($celdas)*$veces);
    		$columnas[]=$prefijo.$celdas[$indice];
    	}


    	return $columnas[$indice_requerido-1];
    }

    private function cargarResumen($sheet){

        $colaboradores=collect();
        foreach ($this->proyecto->equipos as $ide => $equipo) {
            foreach ($equipo->miembros as $idm => $miembro) {
                $colaboradores->each(function($colaborador){

                });
                $colaboradores->push($miembro->usuario);
                
            }
        }

    	$sheet->rows(array(
    		array('Proyecto',$this->proyecto->titulo),
    		array('Fecha de inicio', $this->proyecto->fecha_inicio->format('Y-m-d')),
    		array('Fecha de finalización', $this->proyecto->fecha_termino->format('Y-m-d')),
    		array('colaboradores', $colaboradores->count()),
    		array('Preguntas', $this->preguntas->count()),
    		array('Fecha exportación', \Carbon\Carbon::now()->format('d/m/Y')),
    		array('Hora exportación', \Carbon\Carbon::now()->format('h:i:s a')),
    	));

    	//estilo de resumen
    	//estilo - labels
    	$sheet->cell('A1:A7', function($cell) { 
    		$cell->setBackground('#EEEEEE');
    		$cell->setFontWeight('bold');
    		$cell->setAlignment('left');
    		$cell->setValignment('center');
    	});
    	
    	//estilo - valores
    	$sheet->cell('B1:B7',function($cell){
    		$cell->setAlignment('right');
    		$cell->setValignment('center');
    	});

    	//estilo - bordes de resumen
    	$sheet->setBorder('A1:B7', 'thin');
    }

    private function cargarRespuestas($sheet){
        $preguntas=$this->proyecto->preguntas();

        //cargando fila de cabeceras
        $cabecera_tabla1=collect(['','','','']);
        $cabecera_tabla2=collect(['codigo','nombre','sucursal','tipo']);

        $preguntas->each(function($pregunta) use($cabecera_tabla1,$cabecera_tabla2){
            $cabecera_tabla1->push($pregunta->numero);
            $cabecera_tabla2->push($pregunta->titulo);
        });

        //cargar filas de miembro y las respuestas que registro
        $fila_excel=collect();
        $this->proyecto->equipos->each(function($equipo) use ($preguntas,$fila_excel) {
            $equipo->miembros->each(function($miembro) use($equipo,$preguntas,$fila_excel){

                if ($miembro->usuario!=null){
                    $datos_miembro=collect([
                        (String)$miembro->usuario->codigo,
                        (String)$miembro->usuario,
                        (String)$equipo->sucursal,
                        (String)$equipo->sucursal->tipo,
                    ]);
                }else{
                    $datos_miembro=collect([
                        (String)"---",
                        (String)"usuario eliminado",
                        (String)$equipo->sucursal,
                        (String)$equipo->sucursal->tipo,
                    ]);
                }


                //agregar primera respuesta registrada y la agrega a la fila
                $preguntas->each(function($pregunta) use($equipo,$miembro,$datos_miembro){
                    $respuestas_misma_pregunta=$miembro->respuestas->filter(function($respuesta) use($pregunta){
                        if ($respuesta->id_pregunta==$pregunta->id) 
                            return $respuesta;
                    });

                    if ($respuestas_misma_pregunta->count()>0) {
                        $data_respuesta=$respuestas_misma_pregunta->first()->comentario;
                    }else{
                        $data_respuesta="---";
                    }

                    $datos_miembro->push($data_respuesta);
                });

                $fila_excel->push($datos_miembro);
            });
        });
    
        
		//agregar cabecera y apuntes
    	$sheet->appendRow($cabecera_tabla1->toArray());
    	$sheet->appendRow($cabecera_tabla2->toArray());
        $sheet->rows($fila_excel->toArray());

		//estilos - bordes de data
    	$letra=$this->obtenerIndiceCelda($cabecera_tabla1->count());
    	
    	// estilo data cabecera
        $sheet->cell('A1:'.$letra.'1', function($cell) { 
            $cell->setBackground('#DDDDDD');
            $cell->setFontWeight('bold');
            $cell->setAlignment('center');
            $cell->setValignment('center');
            $cell->setBorder('thin', 'thin', 'thin', 'thin');
        });

    	$sheet->cells('A2:'.$letra.'2', function($cells) { 
    		$cells->setBackground('#EEEEEE');
    		$cells->setFontWeight('bold');
    		$cells->setAlignment('center');
    		$cells->setValignment('center');
    		$cells->setBorder('thin', 'thin', 'thin', 'thin');
            // $cell->setWidth(120);
    	});

        // $sheet->setAutoSize(false);
        

    	// estilo data contenido
    	$indice=$fila_excel->count()+2;
    	$sheet->setBorder('A1:'.$letra.$indice, 'thin');

        $sheet->setAutoSize(array(
            'A','B', 'G','H'
        ));
    }

    private function cargarApuntes($sheet){
    	//agregar cabecera de tabla
		
		$filas_excel=collect();
		$this->proyecto->equipos->each(function($equipo)  use($sheet,$filas_excel) {
			$equipo->miembros->each(function($miembro)  use($sheet,$equipo,$filas_excel) {
				//agregar datos de miembro
                if ($miembro->usuario!=null){
                    $datos_miembro=collect([
                        (String)$miembro->usuario->codigo,
                        (String)$miembro->usuario,
                        (String)$equipo->sucursal,
                        (String)$equipo->sucursal->tipo,
                    ]);
                }else{
                    $datos_miembro=collect([
                        (String)"---",
                        (String)"usuario eliminado",
                        (String)$equipo->sucursal,
                        (String)$equipo->sucursal->tipo,
                    ]);
                }
				//agregar datos de apuntes realizados
				$miembro->apuntes->each(function($apunte) use($datos_miembro){
					$datos_miembro->push($apunte->comentario);
				});
				
				//preparar fila de apunte
				$filas_excel->push($datos_miembro);
			});
		});
       
		//calcular el numero de apuntes maximos realizados
		$apuntes_maximos = $filas_excel->map(function ($fila) {
			return count($fila)-4;
		})->max();

        //rellenar cajas vacias
        $filas_excel->each(function($fila) use ($apuntes_maximos){
            $inicio=$fila->count()-4;
            for ($i=$inicio; $i < $apuntes_maximos; $i++) { 
                $fila->push("---");
            }
        });

        //crear cabecera segun apuntes realizados
        $cabecera_tabla=collect(['codigo','nombre','sucursal','tipo']);

        for ($i=0; $i < $apuntes_maximos; $i++) { 
            $cabecera_tabla->push('apunte '+($i+1));
        }

        //ESTILOS
       
		//agregar cabecera y apuntes
    	$sheet->appendRow($cabecera_tabla->toArray());
    	$sheet->rows($filas_excel->toArray());

		//estilos - bordes de data
    	$letra=$this->obtenerIndiceCelda($cabecera_tabla->count());
    	
    	// estilo data cabecera
    	$sheet->cell('A1:'.$letra.'1', function($cell) { 
    		$cell->setBackground('#EEEEEE');
    		$cell->setFontWeight('bold');
    		$cell->setAlignment('center');
    		$cell->setValignment('center');
    		$cell->setBorder('thin', 'thin', 'thin', 'thin');
    	});

    	// estilo data contenido
    	$indice=$filas_excel->count()+1;
    	$sheet->setBorder('A1:'.$letra.$indice, 'thin');

        // $cabecera_tabla->each(function($celda,$id_celda) use($sheet){
        //     if ($id_celda>4) {
        //         $celda=$this->obtenerIndiceCelda($id_celda);
        //         // dd($celda);
        //         // echo $celda;
        //         $sheet->setWidth($celda,120);
        //     }
        // });
        $sheet->setAutoSize(true);
    }

    private function cargarChats($sheet){
    	//agregar cabecera
    	$cabecera_tabla=collect(['Miembro','Mensaje']);

		//agregar mensajes
        $filas_mensaje=collect();
		$this->proyecto->chats->each(function($chat) use($sheet,$filas_mensaje){
		    if($chat->miembro!=null && $chat->miembro->usuario!=null){
		        $usuario=$chat->miembro->usuario;
                $codigo=$chat->miembro->usuario->codigo;
            }else{
		        $usuario='usuario eliminado o removido';
		        $codigo='---';
            }


            $filas_mensaje->push(collect([(String)$codigo,(String)$chat->mensaje]));
            $filas_mensaje->push(collect([(String)$usuario]));
            // $filas_mensaje->push(collect([(String)$chat->miembro->equipo->sucursal]));
            $filas_mensaje->push(collect([(String)$chat->created_at->format('d/m/Y')]));
            $filas_mensaje->push(collect([(String)$chat->created_at->format('h:i:s a')]));
		});

        $sheet->appendRow($cabecera_tabla->toArray());
        $sheet->rows($filas_mensaje->toArray());
		
		
		//ESTILOS DE HOJA
    	// estilo data cabecera
    	$sheet->cell('A1:B1', function($cell) { 
    		$cell->setBackground('#EEEEEE');
    		$cell->setFontWeight('bold');
    		$cell->setAlignment('center');
    		$cell->setValignment('center');
    		$cell->setBorder('thin', 'thin', 'thin', 'thin');
    	});

        // $sheet->mergeCells("A1:B2");
        $nro_filas_al_inicio=1;//por cabecera;
        $nro_filas_combinadas_por_mensaje=4; // por datos de mensaje (usuario, codigo, fecha, hora,)
        $filas_mensaje->each(function($fila,$posicion) use($sheet,$nro_filas_al_inicio,$nro_filas_combinadas_por_mensaje){
            if ($fila->count()==2) {
                $indice_inicial=$posicion+$nro_filas_al_inicio+1;
                $indice_final=$indice_inicial+$nro_filas_combinadas_por_mensaje-1;
                // echo "fusionar: "."B".$indice_inicial.":B".$indice_final;

                $sheet->mergeCells("B".$indice_inicial.":B".$indice_final);    
                $sheet->cells('B'.$indice_inicial,function($cell){
                    $cell->setValignment('center');
                    //$cell->getAlignment()->setWrapText(true);
                });
            }
        });



    	// estilo data contenido
    	$indice=$filas_mensaje->count()+$nro_filas_al_inicio;
    	$sheet->setBorder('A0:B'.$indice, 'thin');

        $sheet->setAutoSize(true);
        $sheet->setWidth("B", 50);
    }

    public function exportarMultimedia(){
    	//$ruta_multimedia=$this->dir_temporal."/multimedia";
    	
    	// dd($ruta_multimedia);
    	/*$this->proyecto->equipos->each(function($equipo) use($ruta_multimedia) {
    		$equipo->miembros->each(function($miembro) use($ruta_multimedia) {
    			$miembro->adjuntos->each(function($adjunto) use($ruta_multimedia,$miembro) {
    				$tipos=collect(['imagen','video','audio']);

    				// dd();

    				$nombre="sucursal".(String)$miembro->equipo->sucursal->id."--";
    				$nombre.="user".(String)$miembro->usuario->id."--";
    				$nombre.=$tipos->random()."-";
    				$nombre.=(String)$adjunto->id.".txt";

    				// $this->almacenamiento->m
    				$ruta_archivo=$ruta_multimedia."/".$nombre;
					$this->almacenamiento->put($ruta_archivo,rand());
    			});
    		});
    	});*/
    }

}
