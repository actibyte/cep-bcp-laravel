<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use App\Admin\Proyecto;
use App\Admin\Equipo;
use App\Admin\Sucursal;
use App\Admin\User as Usuario;
use App\Admin\Util;

class EquiposController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}

    public function listado(Request $request,$id_proyecto){
		//preparar filtro y ordenado
		$url_volver=route('proyecto.listado');

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['sucursal'],
            'equipo.listado',$params_get,$params_url);

		//LISTADO
		$listado_equipos=Equipo::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_equipos as $id => $equipo) {
			$equipo->url_miembros=route('miembro.listado',[$equipo->id_proyecto,$equipo->id]);
			$equipo->url_eliminar=route('equipo.eliminar',[$equipo->id_proyecto,$equipo->id]);
		}

		$sucursales_disponibles=Sucursal::cargarDisponiblesParaProyecto($params_url)->get();

		$data=compact('listado_equipos','sucursales_disponibles','campos_orden','url_volver');
		return view('admin.proyecto.equipo.listado', $data);
	}

	public function almacenar(Request $request,$id_proyecto){
		$reglas_validacion=[
				'id_sucursal'=>'required',
			];

		$this->validate($request,$reglas_validacion);
		
		$equipo=new Equipo();
		$equipo->id_proyecto=$id_proyecto;
		$equipo->id_sucursal=$request->input('id_sucursal');
		if($equipo->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Agregado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('equipo.listado',[$id_proyecto])->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_proyecto,$id_equipo){
		$equipo=Equipo::findOrFail($id_equipo);
		if($equipo->miembros->count()>0){
			$mensaje=array('tipo'=>'warning','mensaje'=>'Para eliminar, el equipo no debe contener miembros.');
		}elseif($equipo->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito.');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar.');
		}

		return redirect()->route('equipo.listado',[$id_proyecto])->with('notificacion',$mensaje);
	}
}
