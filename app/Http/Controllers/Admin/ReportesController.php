<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Chumper\Zipper\Zipper;

use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\Proyecto;
use App\Admin\Pregunta;
use App\Admin\Respuesta;
use App\Admin\Apunte;
use App\Admin\Adjunto;
use App\Admin\Chat;

use App\Admin\Foro\Tema;
use App\Admin\Foro\Respuesta as Comentario;

use App\Admin\Util;


class ReportesController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}
	
    public function proyectos(Request $request){
		$url_volver=route('proyecto.listado');

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('titulo','fecha_inicio','fecha_termino','estado');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['sucursal'],
            'reporte.proyectos',$params_get,$params_url);

		// //breacrums / ruteo
		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>'','nombre'=>'proyectos'));

		//LISTADO
		$listado_proyectos=Proyecto::cargarReportes(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_proyectos as $id => $proyecto) {
			$proyecto->url_chats=route('reporte.chats',[$proyecto->id]);
			$proyecto->url_equipos=route('reporte.equipos',[$proyecto->id]);
			$proyecto->url_exportar=route('reporte.exportar',[$proyecto->id]);
		}


		$data=compact('listado_proyectos','campos_orden','url_nuevo','ruteo');
		return view('admin.reporte.proyectos', $data);
    }

    public function equipos(Request $request, $id_proyecto){
    	//preparar filtro y ordenado
		$url_volver=route('reporte.proyectos');
		
		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['sucursal'],
            'reporte.equipos',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);

		//breacrums / ruteo
		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>'','nombre'=>'sucursales'));
		//LISTADO
		$listado_equipos=Equipo::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_equipos as $id => $equipo) {
			$equipo->url_miembros=route('reporte.miembros',[$equipo->id_proyecto,$equipo->id]);
		}

		$data=compact('proyecto','listado_equipos','campos_orden','url_volver','ruteo');
		return view('admin.reporte.equipos', $data);
    }

    public function miembros(Request $request, $id_proyecto, $id_equipo){
    	//preparar filtro y ordenado
		$url_volver=route('reporte.equipos',[$id_proyecto]);
		
		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto','id_equipo');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['apellido','nombre'],
            'reporte.miembros',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$equipo=Equipo::findOrFail($id_equipo);

		//breacrums / ruteo
		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>route('reporte.miembros',[$id_proyecto,$id_equipo]),'nombre'=>'Sucursal:'.$equipo->sucursal));
		$ruteo->push((object)array('url'=>'','nombre'=>'miembros'));

		//LISTADO
		$listado_miembros=Miembro::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_url);

		foreach ($listado_miembros as $id => $miembro) {
			$miembro->url_adjuntos=route('reporte.miembro.adjuntos',[$id_proyecto,$id_equipo,$miembro->id]);
			$miembro->url_apuntes=route('reporte.miembro.apuntes',[$id_proyecto,$id_equipo,$miembro->id]);
			$miembro->url_respuestas=route('reporte.miembro.respuestas',[$id_proyecto,$id_equipo,$miembro->id]);
		}

		$data=compact('proyecto','equipo','listado_miembros','campos_orden','url_volver','ruteo');
		return view('admin.reporte.miembros', $data);
    }

    

    public function respuestas(Request $request, $id_proyecto, $id_equipo, $id_miembro){
    	//preparar filtro y ordenado
		$url_volver=route('reporte.miembros',[$id_proyecto, $id_equipo]);

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto','id_equipo','id_miembro');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['titulo'],
            'reporte.equipos',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$equipo=Equipo::findOrFail($id_equipo);
		$miembro=Miembro::findOrFail($id_miembro);

		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>route('reporte.miembros',[$id_proyecto,$id_equipo]),'nombre'=>'Sucursal:'.$equipo->sucursal));
		$ruteo->push((object)array('url'=>route('reporte.miembro.respuestas',[$id_proyecto,$id_equipo,$id_miembro]),'nombre'=>'Miembro:'.$miembro->usuario));
		$ruteo->push((object)array('url'=>'','nombre'=>'miembros'));
		//LISTADO

		$listado_respuestas=Respuesta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);


		$data=compact('proyecto','equipo','miembro','listado_respuestas','campos_orden','url_volver','ruteo');
		return view('admin.reporte.respuestas', $data);
    }

    public function apuntes(Request $request,$id_proyecto,$id_equipo,$id_miembro){
    	//preparar filtro y ordenado
		$url_volver=route('reporte.miembros',[$id_proyecto, $id_equipo]);

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto','id_equipo','id_miembro');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['titulo'],
            'reporte.miembro.apuntes',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$equipo=Equipo::findOrFail($id_equipo);
		$miembro=Miembro::findOrFail($id_miembro);

		//breacrums / ruteo
		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>route('reporte.miembros',[$id_proyecto,$id_equipo]),'nombre'=>'Sucursal:'.$equipo->sucursal));
		$ruteo->push((object)array('url'=>route('reporte.miembro.apuntes',[$id_proyecto,$id_equipo,$id_miembro]),'nombre'=>'Miembro:'.$miembro->usuario));
		$ruteo->push((object)array('url'=>'','nombre'=>'apuntes'));
		//LISTADO

		$listado_apuntes=Apunte::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_url);
		
		$data=compact('proyecto','equipo','listado_apuntes','campos_orden','url_volver','ruteo');
		return view('admin.reporte.apuntes', $data);
    }

     public function adjuntos(Request $request,$id_proyecto,$id_equipo,$id_miembro){
    	//preparar filtro y ordenado
		$url_volver=route('reporte.miembros',[$id_proyecto, $id_equipo]);

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto','id_equipo','id_miembro');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['titulo'],
            'reporte.miembro.adjuntos',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$equipo=Equipo::findOrFail($id_equipo);
		$miembro=Miembro::findOrFail($id_miembro);

		//breacrums / ruteo
		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>route('reporte.miembros',[$id_proyecto,$id_equipo]),'nombre'=>'Sucursal:'.$equipo->sucursal));
		$ruteo->push((object)array('url'=>route('reporte.miembro.apuntes',[$id_proyecto,$id_equipo,$id_miembro]),'nombre'=>'Miembro:'.$miembro->usuario));
		$ruteo->push((object)array('url'=>'','nombre'=>'Adjuntos'));
		//LISTADO
		$listado_adjuntos=Adjunto::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		

		$data=compact('proyecto','equipo','listado_adjuntos','campos_orden','url_volver','ruteo');
		return view('admin.reporte.adjuntos', $data);
    }
  
    public function chats(Request $request,$id_proyecto){
	  	//preparar filtro y ordenado
		$url_volver=route('reporte.proyectos');

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['titulo'],'reporte.chats',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$listado_chats=Chat::cargarListado(array_merge($params_get,$params_url))
							->orderBy('created_at','desc')
							->paginate(10)->appends($params_url);

		$ruteo=collect();
		$ruteo->push((object)array('url'=>route('reporte.proyectos'),'nombre'=>'Reportes'));
		$ruteo->push((object)array('url'=>route('reporte.equipos',[$id_proyecto]),'nombre'=>'Proyecto:'.$proyecto));
		$ruteo->push((object)array('url'=>'','nombre'=>'Chat'));

		$data=compact('proyecto','listado_chats','campos_orden','url_volver','ruteo');
		return view('admin.reporte.chats', $data);
    }
}
