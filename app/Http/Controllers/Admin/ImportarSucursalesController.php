<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ImportarSucursalesController extends Controller
{
    //
    public $error_lectura;
    public function __construct(){
		$this->middleware('auth:admin');
	}

	public function importarExcel(Request $request){
		$rules = array(
	        'excel' => 'required',
	    );

	    $this->validate($request,$rules);

		$listado_sucursales=$this->generarListadoDesdeExcel($request->file('excel'));
		return $this->guardarUsuarios($listado_sucursales);

	}

	

	public function generarListadoDesdeExcel($ruta_excel){
		$listado_sucursales=collect();
		\Excel::selectSheets('SUCURSALES')->load($ruta_excel, function ($reader) use($listado_sucursales) {
            foreach ($reader->toArray() as $id=>$row) {
                $data=(object)$row;
            	if($id==0 && !$this->comprobarCamposRequeridos($row)){
            		$this->error_lectura="Faltan uno o mas campos requeridos, verifique los campos y/o nombre de la hoja";
            		break;
            	}elseif(trim($data->tipo)!=null
                    && trim($data->codigo)!=null
                    && trim($data->nombre)!=null
                    && trim($data->direccion)!=null
                    && trim($data->ciudad)!=null
                    && trim($data->telefono)!=null){
                    $sucursal=\App\Admin\Sucursal::where('codigo',$data->codigo)->first();
                    if ($sucursal==null) {
                        $sucursal=new \App\Admin\Sucursal();
                        $sucursal->id_tipo=(String)$data->tipo;
                        $sucursal->codigo=(String)$data->codigo;
                        $sucursal->nombre=(String)$data->nombre;
                        $sucursal->direccion=(String)$data->direccion;
                        $sucursal->ciudad=(String)$data->ciudad;
                        $sucursal->telefono=(String)$data->telefono;

                        $sucursal->nuevo_registro=true;
                        $listado_sucursales->push($sucursal);
                    }else{
                        $listado_sucursales->push($sucursal);
                    }
                }




            }
        });


		return $listado_sucursales;
	}

	private function guardarUsuarios($listado_sucursales){
        $contar_nuevos=0;
        $contar_registrados=0;

        // dd($listado_sucursals);
        foreach ($listado_sucursales as $id => $sucursal) {
        	if ($sucursal->nuevo_registro) {
        		unset($sucursal->nuevo_registro);
        		$sucursal->save();
        		$contar_nuevos++;
        	}else{
        		$contar_registrados++;
        	}
        }

        if($this->error_lectura!=null){
        	$tipo="danger";
        	$mensaje=$this->error_lectura;
        }elseif ($contar_nuevos>0 && $listado_sucursales->count()==$contar_nuevos) {
        	$tipo="success";
        	$mensaje="Se han insertado ".$contar_nuevos." sucursales";
        }elseif($contar_nuevos>0){
        	$tipo="warning";
        	$mensaje="Se han insertado ".$contar_nuevos." sucursales, se detectaron ".$contar_registrados." sucursales ya registrados";;
        }elseif($contar_registrados>0){
        	$tipo="danger";
        	$mensaje="Se detectaron ".$contar_registrados." sucursales ya registrados";
        }elseif($listado_sucursales->count()){
        	$tipo="warning";
        	$mensaje="No se han detectado registros en el archivo";
        }else{
        	$tipo="danger";
        	$mensaje="No se han encontrado nuevos registros";
        }
       
    	$mensaje=array('tipo'=>$tipo,'mensaje'=>$mensaje);
		return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
	}

	private function comprobarCamposRequeridos($fila){
		$campos_requeridos=array('tipo','codigo','nombre','direccion','ciudad','telefono');
		$contar=0;
		foreach (array_keys($fila) as $id => $campo) {
			$contar+=in_array($campo,$campos_requeridos,true);
		}

		return $contar==count($campos_requeridos);

	}
}
