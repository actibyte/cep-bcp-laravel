<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Admin\Util;
use App\Admin\Sucursal;
use App\Admin\TipoSucursal;

class SucursalesController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}

	public function listado(Request $request){
		//preparar filtro y ordenado
		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['nombre','codigo','ciudad','direccion','telefono','tipo'],
            'sucursal.listado',$params_get,$params_url);
		//LISTADO
		$listado_sucursales=Sucursal::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_sucursales as $id => $sucursal) {
			$sucursal->url_editar=route('sucursal.editar',[$sucursal->id]);
			$sucursal->url_eliminar=route('sucursal.eliminar',[$sucursal->id]);
		}

		$url_volver=route('sucursal.listado');
		$url_nuevo=route('sucursal.crear');

		$data=compact('listado_sucursales','campos_orden','url_nuevo');
		return view('admin.sucursal.listado', $data);
	}

	public function crear(){
		$url_volver=route('sucursal.listado');

		$sucursal=new Sucursal();
		$sucursal->form_action=route('sucursal.almacenar');

		$tipos_disponibles=TipoSucursal::orderBy('nombre','asc')->get();

		$data=compact('sucursal',
			'url_volver','tipos_disponibles');
		// $data=array_merge($data, $params);
		return view('admin.sucursal.formulario', $data);
	}

	
	public function almacenar(Request $request){
		$reglas_validacion=[
				'id_tipo'=>'required',
				'codigo'=>'required',
				'nombre'=>'required|unique:sucursales',
				'direccion'=>'required',
				'telefono'=>'required',
				// 'ciudad'=>'required',
				// 'sucursal'=>'required|min:6|unique:sucursales',
			];

		$validator=Validator::make($request->all(),$reglas_validacion);

		if($validator->fails()){
			return redirect()->route('sucursal.crear')->withErrors($validator)->withInput();
		}

		$sucursal=new Sucursal();
		$sucursal->id_tipo=$request->input('id_tipo');
		$sucursal->codigo=$request->input('codigo');
		$sucursal->nombre=$request->input('nombre');
		$sucursal->telefono=$request->input('telefono');
		$sucursal->direccion=$request->input('direccion');
		$sucursal->ciudad=$request->input('ciudad');
		$sucursal->descripcion=$request->input('descripcion');

		if($sucursal->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Registrado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);

	}


	public function editar($id_sucursal){
		$url_volver=route('sucursal.listado');

		$sucursal=Sucursal::findOrFail($id_sucursal);
		$sucursal->form_action=route('sucursal.actualizar',[$id_sucursal]);

		$tipos_disponibles=TipoSucursal::orderBy('nombre','asc')->get();

		$data=compact('sucursal','url_volver','tipos_disponibles');
		return view('admin.sucursal.formulario', $data);
	}

	public function actualizar(Request $request,$id_sucursal){
		$reglas_validacion=[
				'id_tipo'=>'required',
				'codigo'=>'required',
				'nombre'=>'required|unique:sucursales,id,:id',
				'direccion'=>'required',
				'telefono'=>'required',
				// 'ciudad'=>'required',
				// 'sucursal'=>'required|min:6|unique:sucursales,sucursal,'.$id_sucursal,
			];

		$validator=Validator::make($request->all(),$reglas_validacion);

		if($validator->fails()){
			return redirect()->route('sucursal.editar',[$id_sucursal])->withErrors($validator)->withInput();
		}

		$sucursal=Sucursal::findOrFail($id_sucursal);
		$sucursal->id_tipo=$request->input('id_tipo');
		$sucursal->codigo=$request->input('codigo');
		$sucursal->nombre=$request->input('nombre');
		$sucursal->telefono=$request->input('telefono');
		$sucursal->direccion=$request->input('direccion');
		$sucursal->ciudad=$request->input('ciudad');
		$sucursal->descripcion=$request->input('descripcion');

		if($sucursal->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_sucursal){

		$sucursal=Sucursal::findOrFail($id_sucursal);

		if ($sucursal->equipos->count()>0) {
			$mensaje=array('tipo'=>'warning','mensaje'=>'Para eliminar no debe estar asociado a ningun proyecto');
		}elseif($sucursal->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('sucursal.listado')->with('notificacion',$mensaje);
	}

	public function importar(){
    	$url_volver=route('sucursal.listado');
    	$action_form=route('sucursal.importar.procesar');

    	$data=compact('url_volver','action_form');
    	return view('admin.sucursal.importar',$data);
    }
    
}
