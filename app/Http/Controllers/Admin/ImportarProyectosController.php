<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Admin\Sucursal;
//use App\Admin\Proyecto;
use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\User as Usuario;

class ImportarProyectosController extends Controller
{
    //
    public $error_lectura;
    private $id_proyecto;

    const OK=1;
    const OBSERVACION=2;
    const ERROR=3;

    private $tipo;

    public function __construct(){
		$this->middleware('auth:admin');
	}

	public function importarExcel(Request $request,$id_proyecto){
        $this->id_proyecto=$id_proyecto;

		$rules = array(
	        'excel' => 'required',
	    );

	    $this->validate($request,$rules);

		$listado_asignaciones=$this->generarListadoDesdeExcel($request->file('excel'));
		//return $this->guardarUsuarios($listado_asignaciones);

        $mensaje=$this->generarMensaje($listado_asignaciones);

        
        $mensaje=array('tipo'=>$this->tipo,'mensaje'=>$mensaje);
        return redirect()->route('proyecto.listado')->with('notificacion',$mensaje);
	}

	public function generarListadoDesdeExcel($ruta_excel){
		$listado_ejecucion_filas=collect();

		\Excel::selectSheets('ASIGNACIONES')->load($ruta_excel, function ($reader) use($listado_ejecucion_filas) {
            $contar_sucursal_no_encontrada=0;
            $contar_usuario_no_encontrado=0;



            foreach ($reader->toArray() as $id=>$row) {
                $data=(object)$row;
                if($id==0 && !$this->comprobarCamposRequeridos($row)){
                    $this->error_lectura="Faltan uno o mas campos requeridos, verifique los campos y/o nombre de la hoja";
                    break;
                }else if(trim($data->codigo_sucursal)!=null
                    && trim($data->codigo_usuario)!=null){
                    //SUCURSAL
                    $sucursal=Sucursal::where('codigo',$data->codigo_sucursal)->first();
                    $usuario=Usuario::where('codigo',$data->codigo_usuario)->first();

                    $status_registrar_equipo=false;
                    $status_registrar_miembro=false;

                    $status=null;

                    if ($sucursal!=null && $usuario!=null) {
                        //EQUIPO
                        $equipo=Equipo::where('id_sucursal',$sucursal->id)
                                            ->where('id_proyecto',$this->id_proyecto)->first();
                        if($equipo==null){
                            $equipo=new Equipo();
                            $equipo->id_sucursal=$sucursal->id;
                            $equipo->id_proyecto=$this->id_proyecto;

                            $status_registrar_equipo=$equipo->save();
                        }

                        $usuario_sin_equipo_asignado=false;
                        $usuarios_disponibles=Usuario::cargarDisponiblesParaEquipo(array('id_proyecto'=>$this->id_proyecto));
                        $filtrado=$usuarios_disponibles->where('id',$usuario->id);
                        if($filtrado->count()==1){
                            $usuario_sin_equipo_asignado=true;
                        }

                       // dd($filtrado->get());
                        //MIEMBRO
                        $miembro=Miembro::where('id_equipo',$equipo->id)
                                        ->where('id_usuario',$usuario->id)->first();
                        if ($miembro==null && $usuario_sin_equipo_asignado) {
                            $miembro=new Miembro();
                            $miembro->id_equipo=$equipo->id;
                            $miembro->id_usuario=$usuario->id;
                            $miembro->id_cargo=0;

                            $status_registrar_miembro=$miembro->save();
                            //NUEVO MIEMBRO ASIGNADO
                        }

                        if ($status_registrar_equipo==true && $status_registrar_miembro==true) {
                            //$listado_filas_con_error->push($id);
                            $status=self::OK;
                        }else if($status_registrar_equipo==true || $status_registrar_miembro==true){
                            $status=self::OBSERVACION;
                        }else{
                            //$listado_filas_con_error->push($id);
                            $status=self::ERROR;
                        }
                    }else{
                        $status=self::ERROR;
                    }

                    $listado_ejecucion_filas->push((object)array('id'=>$id+2,"status"=>$status));
                }

            }
        });
        

        return $listado_ejecucion_filas;
	}

    private function generarMensaje($listado_ejecucion_filas){
        $filas_ok = $listado_ejecucion_filas->filter(function ($valor, $indice) {
            return $valor->status==self::OK;
        });

        $filas_observacion = $listado_ejecucion_filas->filter(function ($valor, $indice) {
            return $valor->status==self::OBSERVACION;
        });

        $filas_error = $listado_ejecucion_filas->filter(function ($valor, $indice) {
            return $valor->status==self::ERROR;
        });


        $mensaje=array();


        
        if ($filas_ok->count()>0 || $filas_observacion->count()>0) {
            $numero_filas=$filas_ok->count()+$filas_observacion->count();
            $mensaje[]="se insertaron ".$numero_filas." filas "; 
            
        }

        if ($filas_error->count()>0) {
            # code...
            $array_ids = $filas_error->map(function ($item, $key) {
                return $item->id;
            });

            
            $mensaje[]="las filas '".$array_ids->implode(",")."' no fueron insertadas ";
        }

        //
        if ($listado_ejecucion_filas->count()==$filas_ok->count()) {
            $this->tipo="success";
        }else if($listado_ejecucion_filas->count()==$filas_error->count()){
            $this->tipo="danger";
        }else{
            $this->tipo="warning";
        }

        return ucfirst(trim(implode(",",$mensaje)));
    }

	

	private function comprobarCamposRequeridos($fila){
		$campos_requeridos=array('codigo_sucursal','codigo_usuario');
		$contar=0;
		foreach (array_keys($fila) as $id => $campo) {
			$contar+=in_array($campo,$campos_requeridos,true);
		}

		return $contar==count($campos_requeridos);

	}
}
