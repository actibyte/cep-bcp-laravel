<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Admin\Proyecto;
use App\Admin\Equipo;
use App\Admin\Miembro;
use App\Admin\Sucursal;
use App\Admin\User as Usuario;
use App\Admin\Util;

class MiembrosController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}

    public function listado(Request $request,$id_proyecto,$id_equipo){
		//preparar filtro y ordenado
		$url_volver=route('equipo.listado',[$id_proyecto]);

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto','id_equipo');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['nombre','apellido'],
            'miembro.listado',$params_get,$params_url);

		//LISTADO
		$listado_miembros=Miembro::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_miembros as $id => $miembro) {
			$miembro->url_eliminar=route('miembro.eliminar',[$id_proyecto,$miembro->id_equipo,$miembro->id]);
		}
		
		$usuarios_disponibles=Usuario::cargarDisponiblesParaEquipo($params_url)->get();

		$data=compact('listado_miembros','usuarios_disponibles','campos_orden','url_nuevo','url_volver');
		return view('admin.proyecto.equipo.miembro.listado', $data);
	}

	public function almacenar(Request $request,$id_proyecto,$id_equipo){
		$reglas_validacion=[
				'id_usuario'=>'required',
			];
		
		$this->validate($request,$reglas_validacion);

		$miembro=new Miembro();
		$miembro->id_equipo=$id_equipo;
		$miembro->id_usuario=$request->input('id_usuario');
		$miembro->id_cargo=2;
		
		if($miembro->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Agregado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo agregar');
		}

		return redirect()->route('miembro.listado',[$id_proyecto,$id_equipo])->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_proyecto,$id_equipo,$id_miembro){
		$miembro=Miembro::findOrFail($id_miembro);
		
		if($miembro->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo eliminar');
		}

		return redirect()->route('miembro.listado',[$id_proyecto,$id_equipo])->with('notificacion',$mensaje);
	}
}
