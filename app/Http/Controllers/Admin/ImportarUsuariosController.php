<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Admin\User as Usuario;
use App\Notifications\EnviarAccesosUsuarioEncuestador;

class ImportarUsuariosController extends Controller
{
    //
    public $error_lectura;

    public function __construct(){
		$this->middleware('auth:admin');
	}

	public function importarExcel(Request $request){
		//validar envio post > file
		$rules = array(
	        'excel' => 'required',
	    );

	    $this->validate($request,$rules);

		$listado_usuarios=$this->generarListadoDesdeExcel($request->file('excel'));
		return $this->guardarUsuarios($listado_usuarios);

	}

	public function generarListadoDesdeExcel($ruta_excel){
		$listado_usuarios=collect();
		$status=\Excel::selectSheets('USUARIOS')->load($ruta_excel, function ($reader) use($listado_usuarios) {
            // $reader->selectSheet("USUARIOS");
           

        	foreach ($reader->toArray() as $id=>$row) {
                $data=(object)$row;
                if($id==0 && !$this->comprobarCamposRequeridos($row)){
                    $this->error_lectura="No se puede leer el excel, verifique los campos y/o nombre de la hoja";
                    break;
                }else if(trim($data->codigo)!=null
                    && trim($data->nombre)!=null
                    && trim($data->apellido)!=null
                    && trim($data->correo)!=null) {


                    $usuario=Usuario::where('codigo',$data->codigo)->orWhere('correo_personal',$data->correo)->first();
                    if ($usuario==null) {
                        $usuario=new Usuario();
                        $usuario->codigo=$data->codigo;
                        $usuario->nombre=$data->nombre;
                        $usuario->apellido=$data->apellido;
                        $usuario->correo_personal=$data->correo;
                        $usuario->api_token=str_random(60);
                        $usuario->nuevo_registro=true;
                        $listado_usuarios->push($usuario);
                    }else{
                        $listado_usuarios->push($usuario);
                    }
                }
            }
        });

		return $listado_usuarios;
	}

	private function guardarUsuarios($listado_usuarios){
        $contar_nuevos=0;
        $contar_registrados=0;

        // dd($listado_usuarios);
        foreach ($listado_usuarios as $id => $usuario) {
        	if ($usuario->nuevo_registro) {
                $password=$usuario->codigo;//str_random(10);
                $usuario->password=bcrypt($password);
                $usuario->clave_actualizada=1;
                $usuario->estado="AC";
        		unset($usuario->nuevo_registro);
                $usuario->save();
                //NOTIFICAR SOLO CON REGISTRO EXITOSO
                // $usuario->notify(new EnviarAccesosUsuarioEncuestador($usuario->codigo,$password));
                $usuario->notify(new EnviarAccesosUsuarioEncuestador($usuario->codigo,$usuario->codigo));

        		$contar_nuevos++;
        	}else{
        		$contar_registrados++;
        	}
        }



        if($this->error_lectura!=null){
            $tipo="danger";
            $mensaje=$this->error_lectura;
        }elseif ($contar_nuevos>0 && $listado_usuarios->count()==$contar_nuevos) {
        	$tipo="success";
        	$mensaje="Se han insertado ".$contar_nuevos." usuarios";
        }elseif($contar_nuevos>0){
        	$tipo="warning";
        	$mensaje="Se han insertado ".$contar_nuevos." usuarios, se detectaron ".$contar_registrados." usuarios ya registrados";;
        }elseif($contar_registrados>0){
        	$tipo="danger";
        	$mensaje="Se detectaron ".$contar_registrados." usuarios ya registrados";
        }elseif($listado_usuarios->count()){
        	$tipo="warning";
        	$mensaje="No se han detectado registros en el archivo";
        }else{
        	$tipo="danger";
        	$mensaje="No se han encontrado nuevos registros";
        }
       
    	$mensaje=array('tipo'=>$tipo,'mensaje'=>$mensaje);
		return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
	}

	private function comprobarCamposRequeridos($fila){
		$campos_requeridos=array('nombre','apellido','correo','codigo');
		$contar=0;
		
		foreach (array_keys($fila) as $id => $campo) {
			$contar+=in_array($campo,$campos_requeridos,true);
		}
		return $contar==count($campos_requeridos);

	}
}
