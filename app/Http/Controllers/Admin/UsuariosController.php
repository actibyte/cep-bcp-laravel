<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;
use App\Admin\Util;
use App\Admin\User as Usuario;

use Notification;
use App\Notifications\EnviarAccesosUsuarioEncuestador;

class UsuariosController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}

	public function listado(Request $request){
		//preparar filtro y ordenado
		$url_nuevo=route('usuario.crear');

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['nombre','apellido','codigo','usuario'],
            'usuario.listado',$params_get,$params_url);

		//LISTADO
		$listado_usuarios=Usuario::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_usuarios as $id => $usuario) {
			$usuario->url_resetear=route('usuario.resetear',[$usuario->id]);
			$usuario->url_editar=route('usuario.editar',[$usuario->id]);
			$usuario->url_eliminar=route('usuario.eliminar',[$usuario->id]);
		}

		$data=compact('listado_usuarios','campos_orden','url_nuevo');
		return view('admin.usuario.listado', $data);
	}

	public function crear(){
		$url_volver=route('usuario.listado');

		$usuario=new Usuario();
		$usuario->form_action=route('usuario.almacenar');

		$estados_disponibles=collect();
		$estados_disponibles->push((object)array('id'=>Usuario::ESTADO_ACTIVO,'nombre'=>'Activo'));
		$estados_disponibles->push((object)array('id'=>Usuario::ESTADO_INACTIVO,'nombre'=>'Inactivo'));

		$data=compact('usuario','estados_disponibles','url_volver');
		return view('admin.usuario.formulario', $data);
	}
	
	public function almacenar(Request $request){
		$reglas_validacion=[
				'codigo' => 'required|unique:users',
				'nombre'=>'required',
				'apellido'=>'required',
				'correo_personal'=>'required|email|unique:users',
				// 'telefono'=>'required',
				'estado' => 'required',
			];

		$this->validate($request,$reglas_validacion);

		$ruta_foto="";

		//parametros para notificacion de correo de registro;
		$codigo=$request->input('codigo');
		$password=$codigo;//str_random(10);
		$url_descarga=asset('app.apk');


		//creacion de objeto usuario para registrar en bd
		$usuario=new Usuario();
		$usuario->codigo=$request->input('codigo');
		$usuario->nombre=$request->input('nombre');
		$usuario->apellido=$request->input('apellido');
		$usuario->correo_personal=$request->input('correo_personal');
		$usuario->telefono_movil=$request->input('telefono_movil');
		$usuario->id_sucursal=$request->input('id_sucursal');
		$usuario->ruta_foto=$ruta_foto;
		$usuario->password=bcrypt($password);
		$usuario->clave_actualizada=0;
		$usuario->api_token=bcrypt(str_random(60));
		$usuario->estado=$request->input('estado');

		if($usuario->save()){
			$usuario->notify(new EnviarAccesosUsuarioEncuestador($codigo,$password));

			$mensaje=array('tipo'=>'success','mensaje'=>'Registrado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}
		
		return redirect()->route('usuario.listado')->with('notificacion',$mensaje);

	}

	public function editar($id_usuario){
		$url_volver=route('usuario.listado');

		$usuario=Usuario::findOrFail($id_usuario);
		$usuario->form_action=route('usuario.actualizar',[$id_usuario]);

		$estados_disponibles=collect();
		$estados_disponibles->push((object)array('id'=>Usuario::ESTADO_ACTIVO,'nombre'=>'Activo'));
		$estados_disponibles->push((object)array('id'=>Usuario::ESTADO_INACTIVO,'nombre'=>'Inactivo'));

		$data=compact('usuario', 'estados_disponibles',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.usuario.formulario', $data);
	}

	public function actualizar(Request $request,$id_usuario){
		$reglas_validacion=[
				'codigo' => 'required|unique:users,id,:id',
				'nombre'=>'required',
				'apellido'=>'required',
				'correo_personal'=>'required|email|unique:users,id,:id',
				// 'telefono'=>'required',
				'estado' => 'required',
			];

		$this->validate($request,$reglas_validacion);

		$ruta_foto="";

		$usuario=Usuario::findOrFail($id_usuario);
		$usuario->codigo=$request->input('codigo');
		$usuario->nombre=$request->input('nombre');
		$usuario->apellido=$request->input('apellido');
		$usuario->correo_personal=$request->input('correo_personal');
		$usuario->telefono_movil=$request->input('telefono_movil');
		$usuario->id_sucursal=$request->input('id_sucursal');
		$usuario->ruta_foto=$ruta_foto;
		$usuario->estado=$request->input('estado');

		if($usuario->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_usuario){

		$usuario=Usuario::findOrFail($id_usuario);

		if($usuario->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('usuario.listado')->with('notificacion',$mensaje);
	}

	public function resetearClave(Request $request,$id_usuario){

		$password=str_random(10);

		$usuario=Usuario::findOrFail($id_usuario);
		$usuario->clave_actualizada=1;
        $usuario->password=bcrypt($password);
		$usuario->api_token=bcrypt(str_random(60));

		$params=array();
		$request->get('buscar')!=""?$params['buscar']=$request->get('buscar'):null;
		$request->get('ordenar')!=""?$params['ordenar']=$request->get('ordenar'):null;

		if ($usuario->save()) {
			$usuario->notify(new EnviarAccesosUsuarioEncuestador($usuario->codigo,$password));

			$mensaje=array('tipo'=>'success','mensaje'=>'Reseteado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('usuario.listado',$params)->with('notificacion',$mensaje);
	}


	public function cambiarClave(){
       	$usuario=Usuario::logueado();

        $usuario->action_form=route('usuario.procesar-cambio-clave');
        $data=compact('usuario');
        return view('admin.usuario.cambiar-clave',$data);
    }

    public function procesarCambioClave(Request $request){


        $reglas_validacion=[
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
       	
       	$this->validate($request,$reglas_validacion);

       

        $usuario=Usuario::logueado();
        $usuario->password=bcrypt($request->input('password'));
        $usuario->clave_actualizada=1;
       
        if ($usuario->save()) {
            $mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
            
        }
        return redirect("/")->with('notificacion',$mensaje);
    }

    public function importar(){
    	$url_volver=route('usuario.listado');
    	$action_form=route('usuario.importar.procesar');

    	$data=compact('url_volver','action_form');
    	return view('admin.usuario.importar',$data);
    }
}
