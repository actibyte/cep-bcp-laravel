<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Admin\Util;
use App\Admin\Proyecto;
use App\Admin\Pregunta;

class PreguntasController extends Controller
{
    //
    public function __construct(){
		$this->middleware('auth:admin');
	}


	public function listado(Request $request,$id_proyecto){
		
		$url_volver=route('proyecto.listado');
		$url_nuevo=route('pregunta.crear',[$id_proyecto]);

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['titulo','numero','descripcion'],
            'pregunta.listado',$params_get,$params_url);

		$proyecto=Proyecto::findOrFail($id_proyecto);

		//LISTADO
		$listado_preguntas=Pregunta::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_preguntas as $id => $pregunta) {
			// $pregunta->url_respuestas=route('respuesta.listado',[$pregunta->id_proyecto,$pregunta->id]);
			$pregunta->url_editar=route('pregunta.editar',[$pregunta->id_proyecto,$pregunta->id]);
			$pregunta->url_eliminar=route('pregunta.eliminar',[$pregunta->id_proyecto,$pregunta->id]);
		}

		$data=compact('proyecto','listado_preguntas','campos_orden','url_nuevo','url_volver');
		return view('admin.proyecto.pregunta.listado', $data);
	}

	public function crear($id_proyecto){
		$url_volver=route('pregunta.listado',[$id_proyecto]);

		$pregunta=new Pregunta();
		$pregunta->id_proyecto=$id_proyecto;
		$pregunta->form_action=route('pregunta.almacenar',[$id_proyecto]);

		$data=compact('pregunta',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.proyecto.pregunta.formulario', $data);
	}

	public function almacenar(Request $request,$id_proyecto){
		$reglas_validacion=[
				'numero' => 'required',
				'titulo' => 'required',
				// 'descripcion'=>'required',
			];

		$this->validate($request,$reglas_validacion);

		$pregunta=new Pregunta();
		$pregunta->id_proyecto=$id_proyecto;
		$pregunta->numero=$request->input('numero');
		$pregunta->titulo=$request->input('titulo');
		$pregunta->descripcion=$request->input('descripcion');
		$pregunta->estado='AC';
		if($pregunta->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Registrado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('pregunta.listado',[$id_proyecto])->with('notificacion',$mensaje);

	}

	public function editar($id_proyecto,$id_pregunta){
		$url_volver=route('pregunta.listado',[$id_proyecto]);

		$pregunta=Pregunta::findOrFail($id_pregunta);
		$pregunta->form_action=route('pregunta.actualizar',[$id_proyecto,$id_pregunta]);

		$data=compact('pregunta',
			'url_volver');
		// $data=array_merge($data, $params);
		return view('admin.proyecto.pregunta.formulario', $data);
	}

	public function actualizar(Request $request,$id_proyecto,$id_pregunta){
		$reglas_validacion=[
				'numero' => 'required',
				'titulo'=>'required',
				// 'descripcion'=>'required',
			];

		$this->validate($request,$reglas_validacion);

		$pregunta=Pregunta::findOrFail($id_pregunta);
		$pregunta->id_proyecto=$id_proyecto;
		$pregunta->numero=$request->input('numero');
		$pregunta->titulo=$request->input('titulo');
		$pregunta->descripcion=$request->input('descripcion');
		
		if($pregunta->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('pregunta.listado',[$id_proyecto])->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_proyecto,$id_pregunta){

		$pregunta=Pregunta::findOrFail($id_pregunta);

		if($pregunta->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return back()->with('notificacion',$mensaje);
	}
}
