<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Validator;

use App\Admin\Util;
use App\Admin\Proyecto;

class ProyectosController extends Controller
{
    //
	public function __construct(){
		$this->middleware('auth:admin');
	}

    public function listado(Request $request){
		//preparar filtro y ordenado
		$url_nuevo=route('proyecto.crear');

		$url_volver=route('proyecto.listado');
		$url_nuevo=route('proyecto.crear');

		$params_get=$request->only(['buscar','ordenar','page']);
        $params_url=compact('id_proyecto');
        //ORDENADO: crear enlaces para ordenado de lista
        $campos_orden=Util::camposOrdenados(['codigo','titulo','fecha_inicio','fecha_termino'],
            'proyecto.listado',$params_get,$params_url);

		//LISTADO
		$listado_proyectos=Proyecto::cargarListado(array_merge($params_get,$params_url))->paginate(10)->appends($params_get);

		foreach ($listado_proyectos as $id => $proyecto) {
			$proyecto->url_preguntas=route('pregunta.listado',[$proyecto->id]);
			$proyecto->url_equipos=route('equipo.listado',[$proyecto->id]);
			$proyecto->url_importar=route('proyecto.importar.formulario',[$proyecto->id]);
			$proyecto->url_editar=route('proyecto.editar',[$proyecto->id]);
			$proyecto->url_eliminar=route('proyecto.eliminar',[$proyecto->id]);
		}

		$data=compact('listado_proyectos','campos_orden','url_nuevo');
		return view('admin.proyecto.proyecto.listado', $data);
	}

	public function crear(){
		$url_volver=route('proyecto.listado');

		$proyecto=new Proyecto();
		$proyecto->form_action=route('proyecto.almacenar');

		$estados_disponibles=$proyecto->getEstadosDisponibles();

		$data=compact('proyecto','estados_disponibles','url_volver');
		return view('admin.proyecto.proyecto.formulario', $data);
	}

	
	public function almacenar(Request $request){
		$reglas_validacion=[
				// 'codigo'=>'required|unique:proyectos',
				'titulo'=>'required|unique:proyectos',
				'fecha_inicio'=>'required|date',
				'fecha_termino'=>'required|date',
				// 'descripcion' => 'required',
				'estado' => 'required',
			];

		$this->validate($request,$reglas_validacion);

		$proyecto=new Proyecto();
		$proyecto->codigo=$request->input('codigo');
		$proyecto->titulo=$request->input('titulo');
		$proyecto->fecha_inicio=$request->input('fecha_inicio');
		$proyecto->fecha_termino=$request->input('fecha_termino');
		$proyecto->descripcion=$request->input('descripcion');
		$proyecto->estado=$request->input('estado');

		if($proyecto->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Registrado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('proyecto.listado')->with('notificacion',$mensaje);

	}

	public function editar($id_proyecto){
		$url_volver=route('proyecto.listado');

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$proyecto->form_action=route('proyecto.actualizar',[$id_proyecto]);

		$estados_disponibles=$proyecto->getEstadosDisponibles();
		
		$data=compact('proyecto', 'estados_disponibles', 'url_volver');
		return view('admin.proyecto.proyecto.formulario', $data);
	}
	

	public function actualizar(Request $request,$id_proyecto){
		$reglas_validacion=[
				// 'codigo' => 'required|unique:proyectos,id,:id',
				'titulo'=>'required|unique:proyectos,id,:id',
				'fecha_inicio'=>'required|date',
				'fecha_termino'=>'required|date',
				// 'descripcion' => 'required',
				'estado' => 'required',
			];

		$this->validate($request,$reglas_validacion);
		

		$proyecto=Proyecto::findOrFail($id_proyecto);
		$proyecto->codigo=$request->input('codigo');
		$proyecto->titulo=$request->input('titulo');
		$proyecto->fecha_inicio=$request->input('fecha_inicio');
		$proyecto->fecha_termino=$request->input('fecha_termino');
		$proyecto->descripcion=$request->input('descripcion');
		$proyecto->estado=$request->input('estado');

		if($proyecto->save()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Actualizado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('proyecto.listado')->with('notificacion',$mensaje);
	}

	public function destruir(Request $request,$id_proyecto){

		$proyecto=Proyecto::findOrFail($id_proyecto);

		if($proyecto->delete()){
			$mensaje=array('tipo'=>'success','mensaje'=>'Eliminado con exito');
		}else{
			$mensaje=array('tipo'=>'warning','mensaje'=>'No se pudo editar');
		}

		return redirect()->route('proyecto.listado')->with('notificacion',$mensaje);
	}

	public function importar($id_proyecto){
    	$url_volver=route('proyecto.listado');
    	$action_form=route('proyecto.importar.procesar',$id_proyecto);

    	$proyecto=Proyecto::findOrFail($id_proyecto);
    	$data=compact('url_volver','action_form','proyecto');
    	return view('admin.proyecto.proyecto.importar',$data);
    }
}
