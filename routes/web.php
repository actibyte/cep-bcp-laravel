<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::auth();




// Route::get('/home', 'HomeController@index');

Route::get('/politicas-de-privacidad',function(){
    return view('politicas-de-uso');
});
Route::get('/','HomeController@index' );
Route::get('/admin','HomeController@index' );


Route::group(['prefix'=>'interfaz', 'namespace'=>'Interfaz'], function () {
    //AUTENTICACION
    Route::post('/login','AutenticacionController@login');  //Procesar el inicio de sesion
    // Route::post('/logout','AutenticacionController@logout');

    //SINCRONIZACION
    Route::get('descargar','SincronizadorController@descargar'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::post('cargar/adjunto','SincronizadorController@subirAdjunto'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::post('cargar/apunte','SincronizadorController@subirApunte'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::post('cargar/respuesta','SincronizadorController@subirRespuesta'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::post('cargar/apunte/editar','SincronizadorController@subirApunteEdicion'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::post('cargar/respuesta/editar','SincronizadorController@subirRespuestaEdicion'); // Nombre del usuario,  Numero de preguntas pendientes


    //APP
    Route::get('inicio','PanelController@cargarInicio'); // Nombre del usuario,  Numero de preguntas pendientes
    Route::get('informacion','PanelController@cargarInformacion'); // Informacion de sucursal y proyecto
    
    Route::get('preguntas','PreguntasController@listado'); // informacion de preguntas (pendientes y completadas)
    Route::get('pregunta/detalle','PreguntasController@detalle'); // informacion de pregunta seleccionada + respuesta si la tuviera
    Route::post('pregunta/guardar','PreguntasController@guardarRespuesta');

    Route::get('apuntes','ApuntesController@listado');    //informacion de apuntes registrados
    Route::post('apunte/guardar','ApuntesController@almacenar');

    Route::get('foro','ForosController@listado');    //informacion de foros : mensajes registrados
    Route::post('foro/mensaje/guardar','ForosController@almacenar');

    Route::get('multimedia/archivos','MultimediaController@listado');    //informacion de archivos subidos
    Route::post('multimedia/video/guardar','MultimediaController@subirVideo'); //Guardar video
    Route::post('multimedia/audio/guardar','MultimediaController@subirAudio'); //guardar audio
    Route::post('multimedia/imagen/guardar','MultimediaController@subirImagen');   //guardar imagen

    // //PROYECTOS
    // Route::get('/','ProyectosController@listadoAsignados');
    // Route::get('proyectos','ProyectosController@listadoAsignados');

    // //PROYECTOS>PREGUNTAS
    // Route::get('proyecto/{id_proyecto}/preguntas','ProyectosController@listadoPreguntas');
    // Route::get('pregunta/{pregunta}','ProyectosController@detallePregunta');

    // //PROYECTOS>RESPUESTAS
    // Route::get('pregunta/{id_pregunta}/respuestas','RespuestasController@listado');
    // Route::post('pregunta/{id_pregunta}/respuesta/crear','RespuestasController@almacenar');
    // Route::post('respuesta/{id_respuesta}/eliminar','RespuestasController@eliminar');

    // Route::get('respuesta/{id_respuesta}/detalle','RespuestasController@detalle');

    // // FORO
    // Route::get('equipo/{id_equipo}/temas','ForoTemasController@listado');
    // Route::post('equipo/{id_equipo}/tema/crear','ForoTemasController@almacenar');
    // Route::get('equipo/{id_equipo}/tema/{id_tema}/respuestas','ForoRespuestasController@listado');
    // Route::post('equipo/{id_equipo}/tema/{id_tema}/crear','ForoRespuestasController@almacenar');

    // //APUNTE
    // Route::get('miembro/{id_miembro}/apuntes','ApuntesController@listado');
    // Route::post('miembro/{id_miembro}/apunte/crear','ApuntesController@almacenar');
});


// Route::group(['middleware'=>['auth','validar.sesion'],'prefix'=>'admin'], function () {
//Route::auth();

Route::group(['namespace'=>'Admin\Auth'],function(){
    //Login Routes...
    Route::get('login',['as'=>'admin.login','uses'=>'LoginController@showLoginForm']);
    Route::post('login',['as'=>'admin.login.procesar','uses'=>'LoginController@login']);
    Route::post('logout',['as'=>'admin.logout','uses'=>'LoginController@logout']);

    // Registration Routes...
    Route::get('register', ['as'=>'admin.registrar','uses'=>'LoginController@showRegistrationForm']);
    Route::post('register', ['as'=>'admin.registrar.procesar','uses'=>'LoginController@register']);

    // Registration Routes...
    Route::get('password/reset', ['as'=>'admin.reset.clave','uses'=>'ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as'=>'admin.reset.clave.procesar','uses'=>'ForgotPasswordController@sendResetLinkEmail']);

    // Registration Routes...
    Route::get('password/reset/{token}', ['as'=>'admin.reset.cambiar','uses'=>'ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as'=>'admin.reset.cambiar.procesar','uses'=>'ResetPasswordController@reset']);
});

Route::group(['prefix'=>'admin','namespace'=>'Admin'], function () {
    Route::get('resetear',function(){

        $proyectos=App\Admin\Proyecto::all();

        foreach ($proyectos as $id => $proyecto) {
            $nombre_directorio="proyecto-".$proyecto->id;
            //echo "<br>".$nombre_directorio;
            if (\Storage::disk('local')->exists($nombre_directorio)) {
                # code...
                \Storage::disk('local')->deleteDirectory($nombre_directorio);
            }else{
               // echo "nada";
            }

        }


        \DB::table('users')->truncate();
        \DB::table('sucursales')->truncate();
        \DB::table('proyectos')->truncate();
        \DB::table('preguntas')->truncate();
        \DB::table('equipos')->truncate();
        \DB::table('miembros')->truncate();
        \DB::table('chats')->truncate();
        \DB::table('respuestas')->truncate();
        \DB::table('apuntes')->truncate();
        \DB::table('adjuntos')->truncate();

        return redirect('admin');
    });

	Route::group(['prefix'=>'perfil'],function(){
		Route::get('cambiar-clave',['as'=>'usuario.cambiar-clave','uses'=>'UsuariosController@cambiarClave']);
		Route::put('cambiar-clave',['as'=>'usuario.procesar-cambio-clave','uses'=>'UsuariosController@procesarCambioClave']);
		// Route::get('editar-perfil',['as'=>'usuario.editar-perfil','uses'=>'UsuarioController@edit']);
		// Route::put('editar-perfil',['as'=>'usuario.update-perfil','uses'=>'UsuarioController@update']);
	});

  
    Route::group(['prefix'=>'usuarios'], function () {
        Route::get('importar',['as'=>'usuario.importar.formulario','uses'=>'UsuariosController@importar']);
        Route::post('importar',['as'=>'usuario.importar.procesar','uses'=>'ImportarUsuariosController@importarExcel']);

        Route::get('listado',['as'=>'usuario.listado','uses'=>'UsuariosController@listado']);
        Route::get('crear',['as'=>'usuario.crear','uses'=>'UsuariosController@crear']);
        Route::get('{id_usuario}/editar',['as'=>'usuario.editar','uses'=>'UsuariosController@editar']);

        Route::post('crear',['as'=>'usuario.almacenar','uses'=>'UsuariosController@almacenar']);
        Route::put('{id_usuario}/editar',['as'=>'usuario.actualizar','uses'=>'UsuariosController@actualizar']);
        Route::delete('{id_usuario}/eliminar',['as'=>'usuario.eliminar','uses'=>'UsuariosController@destruir']);

        Route::post('{id_usuario}/resetear',['as'=>'usuario.resetear','uses'=>'UsuariosController@resetearClave']);
    });

    Route::group(['prefix'=>'sucursales'], function () {
        Route::get('importar',['as'=>'sucursal.importar.formulario','uses'=>'SucursalesController@importar']);
        Route::post('importar',['as'=>'sucursal.importar.procesar','uses'=>'ImportarSucursalesController@importarExcel']);

        Route::get('listado',['as'=>'sucursal.listado','uses'=>'SucursalesController@listado']);
        Route::get('crear',['as'=>'sucursal.crear','uses'=>'SucursalesController@crear']);
        Route::get('{id_sucursal}/editar',['as'=>'sucursal.editar','uses'=>'SucursalesController@editar']);

        Route::post('crear',['as'=>'sucursal.almacenar','uses'=>'SucursalesController@almacenar']);
        Route::put('{id_sucursal}/editar',['as'=>'sucursal.actualizar','uses'=>'SucursalesController@actualizar']);
        Route::delete('{id_sucursal}/eliminar',['as'=>'sucursal.eliminar','uses'=>'SucursalesController@destruir']);
    });

    Route::group(['prefix'=>'proyectos'], function () {
        Route::get('/',['as'=>'proyecto.listado','uses'=>'ProyectosController@listado']);
        Route::get('crear',['as'=>'proyecto.crear','uses'=>'ProyectosController@crear']);
        Route::get('{id_proyecto}/editar',['as'=>'proyecto.editar','uses'=>'ProyectosController@editar']);

        Route::get('{id_proyecto}/importar',['as'=>'proyecto.importar.formulario','uses'=>'ProyectosController@importar']);
        Route::post('{id_proyecto}/importar',['as'=>'proyecto.importar.procesar','uses'=>'ImportarProyectosController@importarExcel']);

        Route::post('crear',['as'=>'proyecto.almacenar','uses'=>'ProyectosController@almacenar']);
        Route::put('{id_proyecto}/editar',['as'=>'proyecto.actualizar','uses'=>'ProyectosController@actualizar']);
        Route::delete('{id_proyecto}/eliminar',['as'=>'proyecto.eliminar','uses'=>'ProyectosController@destruir']);
    });

    Route::group(['prefix'=>'proyecto/{id_proyecto}/preguntas'], function () {
        Route::get('/',['as'=>'pregunta.listado','uses'=>'PreguntasController@listado']);
        Route::get('crear',['as'=>'pregunta.crear','uses'=>'PreguntasController@crear']);
        Route::get('{id_pregunta}/editar',['as'=>'pregunta.editar','uses'=>'PreguntasController@editar']);

        Route::post('crear',['as'=>'pregunta.almacenar','uses'=>'PreguntasController@almacenar']);
        Route::put('{id_pregunta}/editar',['as'=>'pregunta.actualizar','uses'=>'PreguntasController@actualizar']);
        Route::delete('{id_pregunta}/eliminar',['as'=>'pregunta.eliminar','uses'=>'PreguntasController@destruir']);
    });

    Route::group(['prefix'=>'proyecto/{id_proyecto}/equipos'], function () {
        Route::get('/',['as'=>'equipo.listado','uses'=>'EquiposController@listado']);
        Route::post('/',['as'=>'equipo.almacenar','uses'=>'EquiposController@almacenar']);
        Route::delete('{id_equipo}/remover',['as'=>'equipo.eliminar','uses'=>'EquiposController@destruir']);
    });

    Route::group(['prefix'=>'proyecto/{id_proyecto}/equipo/{id_equipo}/miembros'], function () {
        Route::get('/',['as'=>'miembro.listado','uses'=>'MiembrosController@listado']);
        Route::post('/',['as'=>'miembro.almacenar','uses'=>'MiembrosController@almacenar']);
        Route::delete('{id_miembro}/remover',['as'=>'miembro.eliminar','uses'=>'MiembrosController@destruir']);
    });

   
    
    Route::group(['prefix'=>'reportes'], function () {
        Route::get('proyectos',['as'=>'reporte.proyectos','uses'=>'ReportesController@proyectos']);
        Route::get('proyecto/{id_proyecto}/exportar',['as'=>'reporte.exportar','uses'=>'ExportarReporteController@exportar']);

      

        //derivados segun reporte
        Route::get('proyecto/{id_proyecto}/equipos',
            ['as'=>'reporte.equipos','uses'=>'ReportesController@equipos']);

        Route::get('proyecto/{id_proyecto}/foro',
            ['as'=>'reporte.chats','uses'=>'ReportesController@chats']);

        Route::get('proyecto/{id_proyecto}/equipo/{id_equipo}/miembros',
            ['as'=>'reporte.miembros','uses'=>'ReportesController@miembros']);

        Route::group(['prefix'=>'proyecto/{id_proyecto}/equipo/{id_equipo}/miembro/{id_miembro}'],function(){
            Route::get('respuestas',
                ['as'=>'reporte.miembro.respuestas','uses'=>'ReportesController@respuestas']);

            //apuntes
            Route::get('apuntes',
                ['as'=>'reporte.miembro.apuntes','uses'=>'ReportesController@apuntes']);

            Route::get('adjuntos',
                ['as'=>'reporte.miembro.adjuntos','uses'=>'ReportesController@adjuntos']);
        });
    });


    
});


